<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\BatchClaimApproval1Controller;
use App\Http\Controllers\BatchClaimApproval2Controller;
use App\Http\Controllers\BatchClaimController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\ClaimCheckerController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ClaimMakerController;
use App\Http\Controllers\HistoryBatchClaimController;
use App\Http\Controllers\HistoryClaimController;
use App\Http\Controllers\LandingContactController;
use App\Http\Controllers\LandingFAQController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ParameterGlController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['whitelist.hosts'])->group(function () {
    // Rute-rute yang perlu diakses hanya oleh host yang diizinkan
    // Main Page Route
    Route::get('/', [DashboardController::class, 'index']);
    Route::get('/faq', [LandingFAQController::class, 'index'])->name('faq');
    Route::get('/contact', [LandingContactController::class, 'index'])->name('contact');

    Route::middleware('throttle:login')->post('login', 'Auth\LoginController@login')->name('login');

    Route::post('api/get-rekening', [ApiController::class, 'getRekening'])->name('api.get-account');
    Route::get(
        'api/get-session',
        [ApiController::class, 'geSession']
    )->name('api.get-session');
    Route::get('api/test', [ApiController::class, 'test']);

    Auth::routes(['verify' => true]);
    // AUTH By Farhan
    Route::get('permissions/datatable', [PermissionController::class, 'datatable'])->name('permissions.datatable');
    Route::get('users/update-status/{id}', [UserController::class, 'updateStatus'])->name('users.updateStatus');

    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);

    Route::get('/users-change-pass-index/{id}', [UserController::class, 'ChangePassword'])->name('ChangePassword');
    Route::patch('/users-change-pass/{id}', [
        UserController::class, 'updatePassword'
    ])->name('users-change-pass');
    Route::resource('users', UserController::class);

    Route::get('news/datatable', [NewsController::class, 'DataTable'])->name('news.datatable');
    Route::resource('news', NewsController::class);

    Route::get('parameter-gl/datatable', [ParameterGlController::class, 'DataTable'])->name('parameter-gl.datatable');
    Route::resource('parameter-gl', ParameterGlController::class);

    Route::get('branch/datatable', [BranchController::class, 'DataTable'])->name('branch.datatable');
    Route::resource('branch', BranchController::class);

    Route::get('claim/datatableFeeTransfer', [ClaimMakerController::class, 'DataTableFeeTransfer'])->name('claim.datatableFeeTransfer');
    Route::get('claim/datatableFeeEvent', [ClaimMakerController::class, 'DataTableFeeEvent'])->name('claim.datatableFeeEvent');
    Route::resource('claim', ClaimMakerController::class);

    Route::get('/search-nasabah', [ClaimMakerController::class, 'searchNasabah'])->name('claim.searchNasabah');
    Route::get('/branches', [ClaimMakerController::class, 'getBranches']);


    Route::get('claim-checker/datatableFeeTransfer', [ClaimCheckerController::class, 'DataTableFeeTransfer'])->name('claim-checker.datatableFeeTransfer');
    Route::get('claim-checker/datatableFeeEvent', [ClaimCheckerController::class, 'DataTableFeeEvent'])->name('claim-checker.datatableFeeEvent');
    Route::post('claim-checker/approve', [ClaimCheckerController::class, 'approve'])->name('claim-checker.approve');
    Route::resource('claim-checker', ClaimCheckerController::class);


    Route::get('history-claim/datatableFeeTransfer', [HistoryClaimController::class, 'DataTableFeeTransfer'])->name('history-claim.datatableFeeTransfer');
    Route::get('history-claim/datatableFeeEvent', [HistoryClaimController::class, 'DataTableFeeEvent'])->name('history-claim.datatableFeeEvent');
    Route::resource('history-claim', HistoryClaimController::class);

    Route::get('history-batch-claim/datatable', [HistoryBatchClaimController::class, 'DataTable'])->name('history-batch-claim.datatable');
    Route::resource('history-batch-claim', HistoryBatchClaimController::class);

    Route::post('batch-claim/create-batch', [BatchClaimController::class, 'createBatch'])->name('batch-claim.create-batch');
    Route::get('batch-claim/datatableFeeTransfer', [BatchClaimController::class, 'DataTableFeeTransfer'])->name('batch-claim.datatableFeeTransfer');
    Route::get('batch-claim/datatableFeeEvent', [BatchClaimController::class, 'DataTableFeeEvent'])->name('batch-claim.datatableFeeEvent');
    Route::post('batch-claim/approve', [BatchClaimController::class, 'approve'])->name('batch-claim.approve');
    Route::resource('batch-claim', BatchClaimController::class);

    Route::get('batch-claim-approval1/datatable', [BatchClaimApproval1Controller::class, 'DataTable'])->name('batch-claim-approval1.datatable');
    Route::post('batch-claim-approval1/approve', [BatchClaimApproval1Controller::class, 'approve'])->name('batch-claim-approval1.approve');
    Route::resource('batch-claim-approval1', BatchClaimApproval1Controller::class);

    Route::get('batch-claim-approval2/datatable', [BatchClaimApproval2Controller::class, 'DataTable'])->name('batch-claim-approval2.datatable');
    Route::post('batch-claim-approval2/approve', [BatchClaimApproval2Controller::class, 'approve'])->name('batch-claim-approval2.approve');
    Route::post('batch-claim-approval2/postingGl', [BatchClaimApproval2Controller::class, 'postingGl'])->name('batch-claim-approval2.postingGl');
    Route::resource('batch-claim-approval2', BatchClaimApproval2Controller::class);
});
