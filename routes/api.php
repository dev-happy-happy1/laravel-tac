<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApiManagementController;
use App\Http\Controllers\NewsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// define a route for the API endpoint
Route::get('/image/{filename}', function ($filename) {
    // get the path to the image file
    // dd('ok');
    $path = storage_path("app/public/image/$filename");
    // dd($path);
    // check if the file exists
    if (!file_exists($path)) {
        abort(404);
    }

    // read the contents of the file
    $file = file_get_contents($path);

    // return the contents as an image response
    return response($file, 200)->header('Content-Type', 'image/jpeg');
});

Route::get('/news-pdf/{filename}', function ($filename) {
    $filePath = storage_path('app/public/attach/' . $filename);

    if (!file_exists($filePath)) {
        abort(404);
    }

    return response()->download($filePath);
});

Route::get('/attach-cv/{filename}', function ($filename) {
    $filePath = public_path('public/attach_cv/' . $filename);

    if (!file_exists($filePath)) {
        abort(404);
    }

    return response()->download($filePath);
})->name('download-cv');

Route::post('/upload-job', [ApiController::class, 'uploadJob']);
Route::post('/upload-avatar', [ApiController::class, 'uploadAvatar']);

Route::get('/avatar/{nip}',  [ApiController::class, 'getAvatar']);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//TAC
Route::get('/get-kurs/{currency}',  [ApiController::class, 'getKurs']);

Route::get('/get-cif',  [ApiController::class, 'dataCif'])->name('dataCif');

Route::get('/get-account',  [ApiController::class, 'dataAccount'])->name('dataAccount');

Route::get('/get-account-cabang',  [ApiController::class, 'dataAccountCabang'])->name('dataAccountCabang');


Route::get('/access-token',  [ApiManagementController::class, 'GetAccessToken'])->name('GetAccessToken');
Route::get('/posting-misc',  [ApiManagementController::class, 'PostingMisc'])->name('PostingMisc');
