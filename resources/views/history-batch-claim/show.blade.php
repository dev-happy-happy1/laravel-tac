@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@php
    $formattedDate = \Carbon\Carbon::parse($batch_claim->created_at)
        ->locale('id')
        ->translatedFormat('d F Y');
@endphp

@section('content')
    <div>
        <div>
            <div class="card card-custom">
                <div class="card-header border-bottom p-1">
                    <div class="head-label">
                        <h4 class="mb-0">
                            View Detail Batch Claim
                        </h4>
                    </div>
                    {{ BackButton($route) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="mb-1">
                            <strong>Status Claim:</strong>
                            <span style="display: block; padding-top: 5px;">
                                {!! statusClaim($batch_claim) !!}
                            </span>
                        </div>

                        @if (!empty($batch_claim->note))
                            <div class="mb-1"
                                style="border: 2px solid red; padding: 10px; background-color: #ffe5e5; border-radius: 5px;">
                                <strong>Alasan Reject</strong>
                                <span style="display: block; padding-top: 5px;">
                                    {{ $batch_claim->note }}
                                </span>
                            </div>
                        @endif

                    </div>
                </div>
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="mb-1">
                            <strong>Tanggal Create Batch:</strong>
                            {{ $formattedDate }}
                        </div>
                        <div class="mb-1">
                            <strong>Batch:</strong>
                            {{ $batch_claim->name }}
                        </div>
                        <div class="mb-1">
                            <strong>Created By:</strong>
                            {{ $user->name }}
                        </div>
                        {{-- Tabel Summary GL --}}
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th width='30px'>No</th>
                                    <th>Cabang</th>
                                    <th>Nama Cabang</th>
                                    <th>Currency</th>
                                    <th>GL TSFI</th>
                                    <th>Total Nominal Fee</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($batch_claim->batch_detail as $key => $detail)
                                    <tr class="text-center">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $detail->branch_code }}</td>
                                        <td>{{ $detail->branch_name }}</td>
                                        <td>{{ $detail->currency_fee }}</td>
                                        <td>
                                            @if (isset($master_gl[$detail->currency_fee]))
                                                {{ $master_gl[$detail->currency_fee]->gl_description }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>{{ number_format($detail->amount_fee, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{-- Tabel Dokumen --}}
                        <div class="mb-1 mt-2">
                            <strong>Permohonan Reimburse Claim Berdasarkan Dokumen Sebagai Berikut:</strong>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th width='30px'>No</th>
                                    <th>No Request Claim</th>
                                    <th>MD Approval</th>
                                    <th>Bukti Transaksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $displayedCodes = [];
                                @endphp
                                @foreach ($batch_claim->batch_detail as $detail)
                                    @php
                                        $claim = $detail->claim;
                                        $mdApprovalDocs = $claim
                                            ? $claim->documents->where('type', 'MD Approval')
                                            : collect();
                                        $buktiTransaksiDocs = $claim
                                            ? $claim->documents->where('type', 'Bukti Transaksi')
                                            : collect();
                                    @endphp

                                    @if ($claim && !in_array($claim->code, $displayedCodes))
                                        @php
                                            $displayedCodes[] = $claim->code;
                                        @endphp
                                        <tr>
                                            <td>{{ count($displayedCodes) }}</td>
                                            <td>{{ $claim->code ?? 'N/A' }}</td>
                                            <td>
                                                @if ($mdApprovalDocs->isNotEmpty())
                                                    <ol>
                                                        @foreach ($mdApprovalDocs as $doc)
                                                            <li>
                                                                <a href="{{ Storage::url('public/file/' . $doc->filename) }}"
                                                                    target="_blank">
                                                                    {{ $doc->filename }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if ($buktiTransaksiDocs->isNotEmpty())
                                                    <ol>
                                                        @foreach ($buktiTransaksiDocs as $doc)
                                                            <li>
                                                                <a href="{{ Storage::url('public/file/' . $doc->filename) }}"
                                                                    target="_blank">
                                                                    {{ $doc->filename }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
