@extends('layouts/fullLayoutMaster')

@section('title', 'CT Connect - Contact')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
  <style>
    /* .navbar{
      background-color: #E1DFE4;
    } */
    body{
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }
    .header{
      font-size: 37px;
      font-weight: 700;
      line-height: 24px;
      letter-spacing: 0.02em;
      text-align: center;
    }
  </style>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="container pb-4 pt-1">
      <h1 class="header pt-5 pb-1"><strong>How Can We Help?</strong></h1>
      <p class="text-center"> email us: ctconnect@ctcorpora.com</p>
    </div>
  </div>
</div>
<div class="bg-white">
  <div class="container pt-5">
    <div class="row pt-2">
      <div class="col-xs-8 col-sm-8 col-md-8 offset-2">
          <h4>
             Hi! What's Wrong? <span class="text-danger">*</span>
          </h4>
          {{ Form::inputSelect(null, 'title', [
            'The output in Mobile App doesn’t match the data in Back Office' => 'The output in Mobile App doesn’t match the data in Back Office',
            'Upload failed (excel/photo/etc)' => 'Upload failed (excel/photo/etc)',
            'Can’t sign in' => 'Can’t sign in',
            'Others'=> 'Others'
          ], null, ['placeholder' => 'Choose one thing we can help', 'required']) }}
      </div>
      <div class="col-xs-8 col-sm-8 col-md-8 offset-2 pt-1">
          <h4>
             Which feature is not working? <span class="text-danger">*</span>
          </h4>
          {{ Form::inputSelect(null, 'title', [
            "Dashboard" => "Dashboard",
            "Company Settings" => "Company Settings",
            "Structure Management" => "Structure Management",
            "Employee Directory" => "Employee Directory",
            "News Management" => "News Management",
            "Promos Management" => "Promos Management",
            "Job Posting" => "Job Posting",
            "Job Applicant" => "Job Applicant",
            "Policy Document" => "Policy Document",
            "HR Contact & User" => "HR Contact & User"
          ], null, ['placeholder' => 'Select the feature', 'required']) }}
      </div>
      <div class="col-xs-8 col-sm-8 col-md-8 offset-2 pt-1">
          <h4>
              Please describe your issue so we can help you more quickly <span class="text-danger">*</span>
          </h4>
          <p class="text-muted">
            What is happening?
            <br>
            What do you expect to happen?
            <br>
            What was the last thing you did before the issue happened?
          </p>
          <textarea id="myeditorinstance" name='description' class="ckeditor">{{ old('description') }}</textarea>
      </div>
       <div class="col-xs-8 col-sm-8 col-md-8 offset-2 pt-1">
          <h4>
             Responses may take a little longer <span class="text-danger">*</span>
          </h4>
          {{ Form::inputSelect(null, 'title', [
            'Really urgent' => 'Really urgent',
            'Urgent' => 'Urgent',
            'Kinda urgent' => 'Kinda urgent',
            'Not urgent' => 'Not urgent'
          ], null, ['placeholder' => 'How urgent is this issue?', 'required']) }}
      </div>
       <div class="col-xs-8 col-sm-8 col-md-8 offset-2 pt-1">
          <h4>
             Your contact details: <span class="text-danger">*</span>
          </h4>
          <div class="row">
            <div class="col-md-4">
              {{ Form::inputText(null, 'title', null, null, ['placeholder' => 'Your name', 'required']) }}
            </div>
            <div class="col-md-8">
              {{ Form::inputText(null, 'title', null, null, ['placeholder' => 'and your email', 'required']) }}
            </div>
          </div>
      </div>
      <div class="col-xs-8 col-sm-8 col-md-8 offset-2 pt-1 pb-5">
        <a href="{{ route('contact') }}" class="btn btn-primary">Submit</a>
      </div>
    </div>
  </div>
</div>
@endsection

@push('vendor-script')
<script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endpush

@push('page-script')
<script src="{{asset(mix('js/scripts/pages/page-auth-login.js'))}}"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
      var allEditors = document.querySelectorAll('.ckeditor');
        for (var i = 0; i < allEditors.length; ++i) {
            ClassicEditor.create(allEditors[i], {
                  toolbar: {
                    items: [
                        'heading',
                        'bold',
                        'italic',
                        'link',
                        '|',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'undo',
                        'redo'
                    ]
                },
                height: '500px', // Adjust the height value as needed
            })
            .catch( error => {
                console.error( error );
            } );
        }
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>
@endpush
