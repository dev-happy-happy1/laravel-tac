{!! Form::open(['route' => 'parameter-gl.store', 'method' => 'POST', 'id' => 'MyForm']) !!}

{{ Form::inputText('No GL: ', 'gl_no', null, null, ['placeholder' => 'No GL', 'required']) }}

{{ Form::inputText('GL Ref: ', 'gl_ref', null, null, ['placeholder' => 'GL Ref', 'required']) }}

{{ Form::inputText('Description: ', 'gl_description', null, null, ['placeholder' => 'Description', 'required']) }}

{{ Form::inputText('Currency: ', 'gl_currency', null, null, ['placeholder' => 'Currency', 'required']) }}



<button onclick="CheckValidation();" type="submit" id="btn-submit" class="btn font-weight-bold btn-block btn-primary">
    Submit
</button>

{!! Form::close() !!}
