{!! Form::open(['route' => 'branch.store', 'method' => 'POST', 'id' => 'MyForm']) !!}

{{ Form::inputText('Code Cabang: ', 'code', null, null, ['placeholder' => 'Code Cabang', 'required']) }}

{{ Form::inputText('Nama Cabang: ', 'name', null, null, ['placeholder' => 'Nama Cabang', 'required']) }}

{{ Form::inputText('Description: ', 'description', null, null, ['placeholder' => 'Description', 'required']) }}



<button onclick="CheckValidation();" type="submit" id="btn-submit" class="btn font-weight-bold btn-block btn-primary">
    Submit
</button>

{!! Form::close() !!}
