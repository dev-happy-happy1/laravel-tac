{{-- Extends layout --}}
@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@section('vendor-style')
{{-- vendor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <br>
                <h3 align='center'><b>LAPORAN AT COST</b></h3>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::inputText('Value Date:', 'date', null, 'flatpicker', ['required','readonly', 'placeholder'=> 'YYYY-MM-DD to YYYY-MM-DD']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::inputSelect('At Cost Currency', 'currency', ['Any'=>'Any','AUD'=>'AUD', 'GBP'=>'GBP', 'HKD' => 'HKD']) }}
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-warning btn-block btnSearch" style="margin-top: 19.5px">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            {{-- Header --}}
            <!-- <div class="card-header border-bottom p-1">
                <div class="head-label">
                    <h4 class="mb-0">Laporan</h4>
                </div>
            </div> -->
            {{-- Body --}}
            <div class="card-body pt-2 table-responsive">
                <table class="table table-bordered yajra-datatable">
                    <thead>
                        <tr class='text-center'>
                            <th>Value Date</th>
                            <th>Status Matching</th>
                            <th>Status Posting</th>
                            <th>CCY TRX</th>
                            <th>CCY DEBIT</th>
                            <th>Charges At Cost</th>
                            <th>No Referensi</th>
                            <th>Customer Name</th>
                            <th>Customer Account</th>
                            <th>Transfer Amount</th>
                            <th>Kurs 1</th>
                            <th>Kurs 2</th>
                            <th>Nominal Equivalen</th>
                            <th>Maintenance By</th>
                            <th>Approve By</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('vendor-script')
{{-- vendor files --}}
<script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endpush

@push('page-script')
<script type="text/javascript">
    $(document).ready(function() {

        $('.flatpicker').flatpickr({
            mode: 'range'
        });

        $('.btnSearch').on('click', function() {
            var date = $('#date').val();
            var currency = $('#currency').val();

            // Set dynamic parameters for the data table
            $('.yajra-datatable').data('dt_params', {
                date: date,
                currency: currency
            });
            // Redraw data table, causes data to be reloaded
            $('.yajra-datatable').DataTable().draw();
        });

        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: "{{ route($route.'.index') }}",
                dataType: 'json',
                cache: false,
                type: 'GET',
                data: function(d) {
                    // Retrieve dynamic parameters
                    var dt_params = $('.yajra-datatable').data('dt_params');
                    // Add dynamic parameters to the data object sent to the server
                    if (dt_params) {
                        $.extend(d, dt_params);
                    }
                }
            },
            columns: [{
                    data: 'original_dwh.h_value_date',
                    name: 'original_dwh.h_value_date'
                },
                {
                    data: 'matching_status',
                    name: 'matching_status'
                },
                {
                    data: 'posting_status',
                    name: 'posting_status'
                },
                {
                    data: 'original_dwh.h_currency_transaction',
                    name: 'original_dwh.h_currency_transaction'
                },
                {
                    data: 'original_dwh.h_currency',
                    name: 'original_dwh.h_currency'
                },
                {
                    data: 'atcost_amount',
                    name: 'atcost_amount'
                },
                {
                    data: 'original_dwh.h_noreff',
                    name: 'original_dwh.h_noreff'
                },
                {
                    data: 'original_dwh.h_customer_name',
                    name: 'original_dwh.h_customer_name'
                },
                {
                    data: 'original_dwh.h_customer_account',
                    name: 'original_dwh.h_customer_account'
                },
                {
                    data: 'original_dwh.h_transfer_amount',
                    name: 'original_dwh.h_transfer_amount'
                },
                {
                    data: 'submit_kurs_sell',
                    name: 'submit_kurs_sell'
                },
                {
                    data: 'submit_kurs_buy',
                    name: 'submit_kurs_buy'
                },
                {
                    data: 'atcost_equivalen',
                    name: 'atcost_equivalen'
                },
                {
                    data: 'maintenances',
                    name: 'maintenances'
                },
                {
                    data: 'approves',
                    name: 'approves'
                },
            ],
            "columnDefs": [{
                className: "text-center",
                "targets": [0, 6]
            }],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10', '25', '50 ', 'All']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-right"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle mr-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 mr-50'
                }) + 'Export',
                buttons: [{
                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 mr-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 mr-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 mr-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                    },
                    // {
                    //     extend: 'pdf',
                    //     text: feather.icons['clipboard'].toSvg({
                    //         class: 'font-small-4 mr-50'
                    //     }) + 'Pdf',
                    //     className: 'dropdown-item',
                    // },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 mr-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }, ],
        }).on('draw', function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    });
</script>
@endpush