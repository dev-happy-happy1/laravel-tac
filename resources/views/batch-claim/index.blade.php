@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')

    <style>
        .autocomplete-suggestions {
            border: 1px solid rgba(231, 231, 231, 0.664);
            background: #FFF;
            overflow: auto;
            box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 5%);
        }

        .autocomplete-suggestion {
            padding: 2px 5px;
            white-space: nowrap;
            overflow: hidden;
        }

        .autocomplete-selected {
            background: #F0F0F0;
        }

        .autocomplete-suggestions strong {
            font-weight: normal;
            color: #3399FF;
        }

        .autocomplete-group {
            padding: 2px 5px;
        }

        .autocomplete-group strong {
            display: block;
            border-bottom: 1px solid #000;
        }
    </style>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="card card-custom">
                <div class="card-body table-responsive">
                    <div class="card-header border-bottom p-1">
                        <div class="head-label">
                            <h4 class="mb-0">List Approval Claim Request</h4>
                        </div>
                    </div>
                    <h5 class="pt-1">Claim Fee Incoming/Outgoing Transfer</h5>
                    <table id='table-transfer' class="table table-bordered yajra-datatable">
                        <thead>
                            <tr class="text-center">
                                <th>Select</th>
                                <th width='30px'>No</th>
                                <th>Tgl Request</th>
                                <th>Code</th>
                                <th>CIF Name</th>
                                <th>CIF</th>
                                <th>Cabang</th>
                                <th>Maker</th>
                                <th>Bagian</th>
                                <th>Jenis Fee</th>
                                <th>No Rekening</th>
                                <th>Currency</th>
                                <th>Nominal Fee</th>
                                <th>Status</th>
                                <th width='100px'>Action</th>
                            </tr>
                        </thead>
                    </table>

                    <h5 class="pt-1">Claim Fee Event</h5>
                    <table id='table-event' class="table table-bordered yajra-datatable">
                        <thead>
                            <tr class="text-center">
                                <th>Select</th>
                                <th width='30px'>No</th>
                                <th>Tgl Request</th>
                                <th>Code</th>
                                <th>Nama Karyawan</th>
                                <th>NIP</th>
                                <th>Cabang</th>
                                <th>Maker</th>
                                <th>Bagian</th>
                                <th>Jenis Fee</th>
                                <th>No Rekening</th>
                                <th>Currency</th>
                                <th>Nominal Fee</th>
                                <th>Status</th>
                                <th width='100px'>Action</th>
                            </tr>
                        </thead>
                    </table>
                    <button type="button" class="btn btn-primary mt-2" id="btn-create-batch">Create Batch</button>
                </div>
            </div>
        </div>
    </div>



@endsection

@push('vendor-script')
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endpush

@push('page-script')
    <script type="text/javascript">
        $(function() {
            'use strict';

            // Handle DataTable for table-transfer
            var dt_transfer_table = $('#table-transfer');
            var transfer_url = "{{ route('batch-claim.datatableFeeTransfer') }}";

            if (dt_transfer_table.length) {
                var dt_transfer = dt_transfer_table.DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: transfer_url,
                        type: 'GET'
                    },
                    columns: [{
                            data: 'id',
                            name: 'id',
                        },
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'created_at',
                            name: 'created_at',
                            render: function(data, type, row) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = String(date.getDate()).padStart(2, '0');
                                    var month = String(date.getMonth() + 1).padStart(2,
                                        '0'); // Months are zero-based
                                    var year = date.getFullYear();
                                    return day + '/' + month + '/' + year;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'code',
                            name: 'code'
                        },
                        {
                            data: 'cif_name',
                            name: 'cif_name'
                        },
                        {
                            data: 'cif',
                            name: 'cif'
                        },
                        {
                            data: 'branches',
                            name: 'branches',
                            render: function(data, type, row) {
                                return data.code + ' - ' + data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.unit;
                            }
                        },
                        {
                            data: 'fee',
                            name: 'fee'
                        },
                        {
                            data: 'account',
                            name: 'account'
                        },
                        {
                            data: 'currency',
                            name: 'currency'
                        },
                        {
                            data: 'amount',
                            name: 'amount'
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: true,
                            searchable: true
                        }
                    ],
                    columnDefs: [{
                        className: "text-center",
                        targets: [0, 13]
                    }],
                    scrollY: "500px",
                    scrollCollapse: true,
                    paging: false,
                    dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                }).on('draw', function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            }

            // Handle DataTable for table-event
            var dt_event_table = $('#table-event');
            var event_url = "{{ route('batch-claim.datatableFeeEvent') }}";

            if (dt_event_table.length) {
                var dt_event = dt_event_table.DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: event_url,
                        type: 'GET'
                    },
                    columns: [{
                            data: 'id',
                            name: 'id',
                        },
                        {
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'created_at',
                            name: 'created_at',
                            render: function(data, type, row) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = String(date.getDate()).padStart(2, '0');
                                    var month = String(date.getMonth() + 1).padStart(2,
                                        '0'); // Months are zero-based
                                    var year = date.getFullYear();
                                    return day + '/' + month + '/' + year;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'code',
                            name: 'code'
                        },
                        {
                            data: 'event_emp_name',
                            name: 'event_emp_name'
                        },
                        {
                            data: 'event_emp_nip',
                            name: 'event_emp_nip'
                        },
                        {
                            data: 'branches',
                            name: 'branches',
                            render: function(data, type, row) {
                                return data.code + ' - ' + data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.unit;
                            }
                        },
                        {
                            data: 'fee',
                            name: 'fee'
                        },
                        {
                            data: 'account',
                            name: 'account'
                        },
                        {
                            data: 'currency',
                            name: 'currency'
                        },
                        {
                            data: 'amount',
                            name: 'amount'
                        },
                        {
                            data: 'status',
                            name: 'status',

                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: true,
                        }
                    ],
                    columnDefs: [{
                        className: "text-center",
                        targets: [0, 13]
                    }],
                    scrollY: "500px",
                    scrollCollapse: true,
                    paging: false,
                    dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                }).on('draw', function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            }

            // Handle click on "Create Batch" button
            $('#btn-create-batch').on('click', function() {
                var selectedIds = [];
                // Check for selected checkboxes in table-transfer
                dt_transfer.$('input[type="checkbox"].row-checkbox:checked').each(function() {
                    selectedIds.push($(this).val());
                });
                // Check for selected checkboxes in table-event
                dt_event.$('input[type="checkbox"].row-checkbox:checked').each(function() {
                    selectedIds.push($(this).val());
                });
                // console.log(selectedIds);

                swal.fire({
                    title: 'Apakah Anda Yakin Untuk Create Batch CLaim?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    reverseButtons: false,
                }).then((result) => {
                    if (result.isConfirmed) {
                        showLoading(); // Show loading indicator before sending request

                        $.ajax({
                            url: "{{ route('batch-claim.create-batch') }}",
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                claimId: selectedIds,
                                _token: '{{ csrf_token() }}'
                            },
                            success: function(response) {
                                // Handle the response from the server
                                // console.log(response);
                                swal.fire({
                                    title: "Success!",
                                    text: "Create batch successfully!",
                                    icon: "success"
                                });
                                dt_transfer.ajax.reload();
                                dt_event.ajax.reload();
                            },
                            error: function(xhr, status, error) {
                                // Handle the error
                                // console.error(xhr.responseText);
                                swal.fire({
                                    title: "Error!",
                                    text: "An error occurred: ",
                                    icon: "error"
                                });
                            }
                        });
                    }
                });

            });

        });

        // Function to show the loading SweetAlert
        function showLoading() {
            swal.fire({
                icon: "info",
                title: "Loading...",
                text: "Please wait...",
                showConfirmButton: false,
                allowOutsideClick: false, // Prevent closing by clicking outside
            });
        }
    </script>
@endpush
