{{-- Extends layout --}}
@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-xs-8 col-sm-8 col-md-8 offset-lg-2">
        <div class="card">
            {{-- Header --}}
            <div class="card-header border-bottom p-1">
                <div class="head-label">
                    <h4 class="mb-0">Show News</h4>
                </div>
                {{ BackButton($route) }}
            </div>
            {{-- Body --}}
            <div class="card-body pt-2">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <table class="table table-striped table-borderless">
                            <tr>
                                <td>Company</td>
                                <td>:</td>
                                <td>{{ $record->company->label }}</td>
                            </tr>
                            <tr>
                                <td>Slug</td>
                                <td>:</td>
                                <td>{{ $record->slug }}</td>
                            </tr>
                            <tr>
                                <td>URL</td>
                                <td>:</td>
                                <td><a href="{{ $record->url }}">Redirect URL</a></td>
                            </tr>
                            <tr>
                                <td>Author</td>
                                <td>:</td>
                                <td>{{ $author->name }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                {{-- <ul>
                    <li>Slug: {{ $record->slug }}</li>
                    <li>URL: {{ $record->url }}</li>
                    <li>URL: <a href="{{ $record->url }}" target="_BLANK">Redirect URL</a></li>
                    <li>Author: {{ $author->name }} - {{ $creator->label }}</li>
                    <li>Created At: {{ $record->createdAt }}</li>
                </ul> --}}
                <hr>
                <h2>{{ $record->title }}</h2>
                <p class="text-default">{{ date('d F Y, H:i:s') }}</p>
                <img src="{{ 
                Storage::url('public/image/' . $record->image); }}" alt=""  width="auto" class="card-img-top img-fluid">
                <p>{!! $record->content !!}</p>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')
{{-- <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script> --}}
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
{{-- <script>
        ClassicEditor
        .create( document.querySelector( '.ckeditor' ) )
        .catch( error => {
            console.error( error );
        } );

        $('.data-submit').on('click', function(params) {
            var editorContent = CKEDITOR.instances.editor.getData();
            console.log(editorContent);

            if (editorContent == '') {
            alert('Please enter some content in the editor.');
            return false;
            }
        })
        //  function validateForm() {
            
        // }
    </script> --}}
@endsection