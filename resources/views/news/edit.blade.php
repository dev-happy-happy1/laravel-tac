{{-- Extends layout --}}
@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-pickadate.css')) }}">
@endsection

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 offset-lg-2">
            <div class="card">
                {{-- Header --}}
                <div class="card-header border-bottom p-1">
                    <div class="head-label">
                        <h4 class="mb-0">Edit News</h4>
                    </div>
                    {{ BackButton($route) }}
                </div>
                {{-- Body --}}
                <div class="card-body pt-2">
                {!! Form::model($record, ['method' => 'PATCH','route' => [$route.'.update', $record->id], 'id' => 'MyForm', 'enctype'=>'multipart/form-data']) !!}
                    {{-- {!! Form::open(['route' => $route . '.store', 'method' => 'POST', 'id' => 'MyForm', 'enctype'=>'multipart/form-data', 'onsubmit'=>"return validateForm();"]) !!} --}}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div style="max-width: 100%;"  class="image-container text-center">
                                <img src="{{ Storage::url('public/image/' . $record->image); }}" id="show-image" style="max-width: 100%; margin-bottom: 10px" class="show-image rounded">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::inputText('Title:* ', 'title', null, null, ['placeholder' => 'title', 'required']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            {{ Form::inputText('URL: ', 'url', null, null, ['placeholder' => 'news url, exp: http://ctconnect.ctcorpora.com']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            {{ Form::inputText('Published Date:* ', 'published_at', null, 'form-control flatpickr flatpickr-input', ['placeholder' => 'YYYY-MM-DD', 'required', 'readonly']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            {{ Form::inputFile('Banner Image: (jpg, png, jpeg only)', 'image', null, 'fileImage', ['placeholder' => 'file',"accept"=>".jpg, .jpeg, .png"], '', 'kosongkan file, jika tidak berubah') }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            {{ Form::inputFile('Document Attach: (pdf only)', 'attach', null, 'fileAttach', ['placeholder' => 'file',"accept"=>".pdf"], '', 'kosongkan file, jika tidak berubah') }}
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 pb-2">
                            <strong>
                                Content: <span class="text-danger">*</span>
                            </strong>
                           <textarea id="myeditorinstance" name='content' class="ckeditor" required>{{ $record->content }}</textarea>
                        </div>
                    </div>
                    <button class="btn btn-primary data-submit mr-1">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
     {{-- <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script> --}}
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        var rangePickr = $('.flatpickr'); 

        if (rangePickr.length) {
            rangePickr.flatpickr();
        }

        var allEditors = document.querySelectorAll('.ckeditor');
        for (var i = 0; i < allEditors.length; ++i) {
            ClassicEditor.create(allEditors[i], {
                  toolbar: {
                    items: [
                        'heading',
                        'bold',
                        'italic',
                        'link',
                        '|',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'undo',
                        'redo'
                    ]
                },
                height: '500px', // Adjust the height value as needed
            })
            .catch( error => {
                console.error( error );
            } );
        }
        // $('body').on('change', '.file', function () {
        //     file_name   = this.files[0].name;
        //     size        = this.files[0].size / 1024;
        //     limit       = 1024 * 1;
        //     validExtensions = ["png", "jpg", "jpeg"];

        //     extension   = file_name.substr( (file_name.lastIndexOf('.') + 1) );

        //     change_name = file_name.split('.').shift() + '' + parseInt(Math.random() * 10000) + '.' + extension;
        //     // set         = $(this).val().replace(file_name, change_name);
        //     // $(this).val(set);

        //     this.files[0].name = change_name;
        //     valid = true;
        //     if(validExtensions.indexOf(extension) == -1){
        //         swal.fire('Oops', 'file harus berektensi: jpg, pdf, jpeg', 'info');
        //         $(this).val('');
        //         valid = false;
        //     }

        //     if(size > limit){
        //         swal.fire('Oops', 'file harus berukuran kurang dari 1MB', 'info');
        //         $(this).val('');
        //         valid = false;
        //     }

        //     if(valid){
        //         files_cek++;
        //     }
        // });

        $('body').on('change', '.fileAttach', function () {
            file_name   = this.files[0].name;
            size        = this.files[0].size / 1024;
            limit       = 1024 * 2;
            validExtensions = ["pdf"];

            extension   = file_name.substr( (file_name.lastIndexOf('.') + 1) );

            change_name = file_name.split('.').shift() + '' + parseInt(Math.random() * 10000) + '.' + extension;
            // set         = $(this).val().replace(file_name, change_name);
            // $(this).val(set);

            this.files[0].name = change_name;
            valid = true;
            if(validExtensions.indexOf(extension) == -1){
                swal.fire('Oops', 'file harus berektensi: pdf', 'info');
                $(this).val('');
                valid = false;
            }

            if(size > limit){
                swal.fire('Oops', 'file harus berukuran kurang dari 2MB', 'info');
                $(this).val('');
                valid = false;
            }

            if(valid){
                files_cek++;
            }
        });

        $('body').on('change', '.fileImage', function () {
            file_name   = this.files[0].name;
            size        = this.files[0].size / 1024;
            limit       = 1024 * 1;
            validExtensions = ["png", "jpg", "jpeg"];

            extension   = file_name.substr( (file_name.lastIndexOf('.') + 1) );

            change_name = file_name.split('.').shift() + '' + parseInt(Math.random() * 10000) + '.' + extension;
            // set         = $(this).val().replace(file_name, change_name);
            // $(this).val(set);

            this.files[0].name = change_name;
            valid = true;
            if(validExtensions.indexOf(extension) == -1){
                swal.fire('Oops', 'file harus berektensi: jpg, png, jpeg', 'info');
                $(this).val('');
                valid = false;
            }

            if(size > limit){
                swal.fire('Oops', 'file harus berukuran kurang dari 1MB', 'info');
                $(this).val('');
                valid = false;
            }

            if(valid){
                const fileInput = document.getElementById('image');
                const preview = document.getElementById('show-image');
                console.log(fileInput);
                const file = fileInput.files[0];
                const reader = new FileReader();

                reader.onloadend = function () {
                    preview.src = reader.result;
                    preview.style.display = 'block';
                };

                if (file) {
                    reader.readAsDataURL(file);
                } else {
                    preview.src = '';
                    preview.style.display = 'none';
                }
            }
        });
    </script>
@endsection
