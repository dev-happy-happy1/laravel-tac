@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
    <style>
        /* .navbar{
                  background-color: #E1DFE4;
                } */
    </style>
@endsection

@section('content')
    <div class="auth-wrapper auth-v2">
        <div class="auth-inner row m-0">
            <!-- Brand logo-->
            <a class="brand-logo" href="{{ url('/') }}">
                {{-- <img src="{{ asset('images/icons/mega (2).png') }}" alt="" style="max-height: 3.125rem"> --}}
                {{-- <h2 class="brand-text ml-1 mt-10">Remintance At COST</h2> --}}
            </a>
            <!-- /Brand logo-->
            <!-- Left Text-->
            <div class="d-none d-lg-flex col-lg-8 align-items-center">
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                    <img class="img-fluid" src="{{ asset('images/farhan/login.png') }}" alt="Login V2"
                        style="max-height: 40.25rem" />
                    {{-- <img class="img-fluid" src="{{asset('images/pages/login-v2.svg')}}" alt="Login V2" /> --}}
                </div>
            </div>
            <!-- /Left Text-->
            <!-- Login-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-3">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                    <small>Welcome to, </small>
                    <h1 class="card-title font-weight-bold mb-1"><b>TREASURY APPLICATION CENTER</b>! &#x1F44B;</h1>
                    <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>
                    <form class="auth-login-form mt-2" method="POST" action="{{ route('login') }}">
                        @csrf
                        <input type="hidden" name="email" value="{{ $username }}" readonly />
                        <input type="password" class="hidden" name="password" value="{{ $password }}" readonly />

                        <div class="form-group">
                            <label for="login-otp" class="form-label">OTP Code</label>
                            <input type="text" class="form-control @error('otp') is-invalid @enderror" id="login-otp"
                                name="otp" placeholder="enter your otp code" aria-describedby="login-otp" tabindex="1"
                                autofocus value="{{ old('otp') }}" autocomplete="off" maxlength="6" required />
                            @error('otp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button class="btn btn-success btn-block" name="login" value="login" tabindex="4">Validate
                            OTP</button>
                    </form>
                </div>
            </div>
            <!-- /Login-->
        </div>
    </div>
@endsection

@push('vendor-script')
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endpush

@push('page-script')
    <script src="{{ asset(mix('js/scripts/pages/page-auth-login.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>

    @if (session('toaster'))
        <script>
            var type = "{{ session('toaster')['alert-type'] }}";
            toastr[type]("{{ session('toaster')['message'] }}", "{{ session('toaster')['title'] }}", {
                showMethod: 'fadeIn',
                hideMethod: 'fadeOut',
                timeOut: 8000,
            });
        </script>
    @endif
@endpush
