@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@php
    $formattedDate = \Carbon\Carbon::parse($batch_claim->created_at)
        ->locale('id')
        ->translatedFormat('d F Y');
@endphp

@section('content')
    <div>
        <div>
            <div class="card card-custom">
                <div class="card-header border-bottom p-1">
                    <div class="head-label">
                        <h4 class="mb-0">
                            View Detail Batch Claim
                        </h4>
                    </div>
                    {{ BackButton($route) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="mb-1">
                            <strong>Tanggal Create Batch:</strong>
                            {{ $formattedDate }}
                        </div>
                        <div class="mb-1">
                            <strong>Batch:</strong>
                            {{ $batch_claim->name }}
                        </div>
                        <div class="mb-1">
                            <strong>Created By:</strong>
                            {{ $user->name }}
                        </div>
                        {{-- Tabel Summary GL --}}
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th width='30px'>No</th>
                                    <th>Cabang</th>
                                    <th>Nama Cabang</th>
                                    <th>Currency</th>
                                    <th>GL TSFI</th>
                                    <th>Total Nominal Fee</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($batch_claim->batch_detail as $key => $detail)
                                    <tr class="text-center">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $detail->branch_code }}</td>
                                        <td>{{ $detail->branch_name }}</td>
                                        <td>{{ $detail->currency_fee }}</td>
                                        <td>
                                            @if (isset($master_gl[$detail->currency_fee]))
                                                {{ $master_gl[$detail->currency_fee]->gl_description }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td>{{ number_format($detail->amount_fee, 2, ',', '.') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>



                        {{-- Tabel Dokumen --}}
                        <div class="mb-1 mt-2">
                            <strong>Permohonan Reimburse Claim Berdasarkan Dokumen Sebagai Berikut:</strong>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr class="text-center">
                                    <th width='30px'>No</th>
                                    <th>No Request Claim</th>
                                    <th>MD Approval</th>
                                    <th>Bukti Transaksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $displayedCodes = [];
                                @endphp
                                @foreach ($batch_claim->batch_detail as $detail)
                                    @php
                                        $claim = $detail->claim;
                                        $mdApprovalDocs = $claim
                                            ? $claim->documents->where('type', 'MD Approval')
                                            : collect();
                                        $buktiTransaksiDocs = $claim
                                            ? $claim->documents->where('type', 'Bukti Transaksi')
                                            : collect();
                                    @endphp

                                    @if ($claim && !in_array($claim->code, $displayedCodes))
                                        @php
                                            $displayedCodes[] = $claim->code;
                                        @endphp
                                        <tr>
                                            <td>{{ count($displayedCodes) }}</td>
                                            <td>{{ $claim->code ?? 'N/A' }}</td>
                                            <td>
                                                @if ($mdApprovalDocs->isNotEmpty())
                                                    <ol>
                                                        @foreach ($mdApprovalDocs as $doc)
                                                            <li>
                                                                <a href="{{ Storage::url('public/file/' . $doc->filename) }}"
                                                                    target="_blank">
                                                                    {{ $doc->filename }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                            <td>
                                                @if ($buktiTransaksiDocs->isNotEmpty())
                                                    <ol>
                                                        @foreach ($buktiTransaksiDocs as $doc)
                                                            <li>
                                                                <a href="{{ Storage::url('public/file/' . $doc->filename) }}"
                                                                    target="_blank">
                                                                    {{ $doc->filename }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ol>
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>


                        <div class="d-flex justify-content-center mt-2">
                            <button type="button" class="btn btn-success mx-2" id="approval1-batch-claim">Approve</button>
                            <button type="button" class="btn btn-danger mx-2" id="rejected1-batch-claim">Reject</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('page-script')
    <script>
        $(document).ready(function() {
            $('#approval1-batch-claim').click(function() {
                var batchId = '{{ $batch_claim->id }}';
                var status = '3.1'; // Ubah status sesuai kebutuhan

                swal.fire({
                    title: "Are you sure?",
                    text: "You will approve this batch claim!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, approve it!",
                    cancelButtonText: "No, cancel!",
                    reverseButtons: false
                }).then(function(result) {
                    if (result.isConfirmed) {
                        showLoading();
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('batch-claim-approval1.approve') }}',
                            data: {
                                id: batchId,
                                status: status,
                                _token: '{{ csrf_token() }}'
                            },
                            success: function(data) {
                                swal.fire({
                                    title: "Success!",
                                    text: "Approve Batch Claim successfully!",
                                    icon: "success"
                                });
                                window.location.href =
                                    '{{ route('batch-claim-approval1.index') }}'; // Redirect ke halaman index
                            },
                            error: function(xhr, status, error) {
                                swal.fire({
                                    title: "Error!",
                                    text: "An error occurred: ",
                                    icon: "error"
                                });
                            }
                        });
                    }
                });
            });

            $('#rejected1-batch-claim').click(function() {
                Swal.fire({
                    title: 'Apakah Anda Yakin Untuk Reject Claim Request Ini?',
                    icon: 'warning',
                    input: 'textarea',
                    inputLabel: 'Note',
                    inputPlaceholder: 'Masukkan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    reverseButtons: false,
                    preConfirm: (note) => {
                        if (!note) {
                            Swal.showValidationMessage('Note is required')
                        }
                        return note
                    }
                }).then((result) => {
                    if (result.isConfirmed) {
                        var batchId =
                            '{{ $batch_claim->id }}'; // Pastikan $claim didefinisikan dan memiliki id
                        var status = '3.2'; // Ubah status sesuai kebutuhan
                        var note = result.value; // Ambil nilai dari input note

                        showLoading();

                        $.ajax({
                            type: 'POST',
                            url: '{{ route('batch-claim-approval1.approve') }}',
                            data: {
                                id: batchId,
                                status: status,
                                note: note, // Tambahkan note ke data yang dikirim
                                _token: '{{ csrf_token() }}'
                            },
                            success: function(data) {
                                Swal.fire({
                                    title: "Success!",
                                    text: "Batch Claim rejected successfully!",
                                    icon: "success"
                                }).then(() => {
                                    window.location.href =
                                        '{{ route('batch-claim-approval1.index') }}'; // Redirect ke halaman index
                                });
                            },
                            error: function(xhr, status, error) {
                                Swal.fire({
                                    title: "Error!",
                                    text: "An error occurred: " +
                                        error,
                                    icon: "error"
                                });
                            }
                        });
                    }
                });
            });
        });

        function showLoading() {
            swal.fire({
                icon: "info",
                title: "Loading...",
                text: "Please wait...",
                showConfirmButton: false,
                allowOutsideClick: false, // Prevent closing by clicking outside
            });
        }
    </script>
@endpush
