@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard TAC')


@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/calendars/fullcalendar.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-invoice-list.css')) }}">

    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-calendar.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection


<style>
    #chartdiv {
        width: 380px;
        height: 155px;
    }
</style>

@section('content')
    <!-- Dashboard HR Starts -->
    <section id="dashboard-ecommerce">
        <div class="row match-height">
            <!-- Medal Card -->
            {{-- <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="card card-congratulations">
                    <div class="card-body text-center">
                        <img src="{{ asset('images/elements/decore-right.png') }}" class="congratulations-img-right"
                            alt="card-img-right" />
                        <div class="text-center pt-3">
                            <h1 class="text-white">Hello, {{ auth()->user()->name }}!</h1>
                            <p class="card-text m-auto w-75">
                                {{ welcome_word() }}
                            </p>
                        </div>
                    </div>
                </div>
            </div> --}}
            @unlessrole('super-admin|HR-Admin|HR-User')
                <div class="col-6">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Hello, {{ auth()->user()->name }}! 🎉</h3>
                            <p class="card-text">{{ welcome_word() }}</p>
                            <br>
                            <br>
                            <img src="{{ asset('images/illustration/badge.svg') }}" class="congratulation-medal"
                                alt="Medal Pic" />
                        </div>
                    </div>
                </div>
            @else
                <div class="col-2">
                    <div class="card">
                        <div class="card-header flex-column align-items-start pb-2">
                            <div class="avatar bg-light-info p-50 m-0">
                                <div class="avatar-content">
                                    {{-- <i data-feather="users" class="font-medium-5"></i> --}}
                                    <i data-feather='clock' class="font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="font-weight-bolder mt-1">{{ date('l') }}</h2>
                            <p class="card-text">{{ date('d F Y') }}</p>
                        </div>
                        {{-- <div class="gained-chart"></div> --}}
                    </div>
                </div>

                <div class="col-2">
                    <div class="card">
                        <div class="card-header flex-column align-items-start pb-2">
                            <div class="avatar bg-light-primary p-50 m-0">
                                <div class="avatar-content">
                                    <i data-feather="users" class="font-medium-5"></i>
                                </div>
                            </div>
                            <h2 class="font-weight-bolder mt-1">{{ number_format($total) }}</h2>
                            <p class="card-text">Total Employee</p>
                        </div>
                        {{-- <div class="gained-chart"></div> --}}
                    </div>
                </div>
                <div class="col-2">
                    <div class="card">
                        <div class="card-header flex-column align-items-start pb-2">
                            <div class="avatar bg-light-info p-50 m-0">
                                <div class="avatar-content">
                                    <x-bi-gender-male class="font-medium-5" />
                                </div>
                            </div>
                            <h2 class="font-weight-bolder mt-1">{{ number_format($male) }}</h2>
                            <p class="card-text">Total <b>Male</b> Employee</p>
                        </div>
                        {{-- <div class="gained-chart"></div> --}}
                    </div>
                </div>
                <div class="col-2">
                    <div class="card">
                        <div class="card-header flex-column align-items-start pb-2">
                            <div class="avatar bg-light-warning p-50 m-0">
                                <div class="avatar-content">
                                    <x-bi-gender-female class="font-medium-5" />
                                </div>
                            </div>
                            <h2 class="font-weight-bolder mt-1">{{ number_format($female) }}</h2>
                            <p class="card-text">Total <b>Female</b> Employee</p>
                        </div>
                        {{-- <div class="gained-chart"></div> --}}
                    </div>
                </div>

                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <div id="chartdiv"></div>
                        </div>
                    </div>
                </div>


                <div class="col-6">
                    <div class="card">
                        <div
                            class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                            <div class="header-left">
                                <h4 class="card-title">Employees by Group</h4>
                            </div>
                        </div>

                        <div class="card-body">
                            <canvas class="departement chartjs" data-height="200"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    <div class="card">
                        <div
                            class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                            <div class="header-left">
                                <h4 class="card-title">Employees Hire & Left</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <canvas class="hire chartjs" data-height="200"></canvas>
                        </div>
                    </div>
                </div>


                {{-- <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Employment Status</h4>
                        </div>
                        <div class="card-body">
                            <canvas class="doughnut-chart-ex chartjs" data-height="200"></canvas>
                            <div class="d-flex justify-content-between mt-3 mb-1">
                                <div class="d-flex align-items-center">
                                    <i data-feather='circle' class="font-medium-2 text-primary"></i>
                                    <span class="font-weight-bold ml-75 mr-25">Permanent</span>
                                    <span>- 1300</span>
                                </div>
                                <div>
                                    <span>80%</span>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between mb-1">
                            <div class="d-flex align-items-center">
                                    <i data-feather='circle' class="font-medium-2 text-warning"></i>
                                    <span class="font-weight-bold ml-75 mr-25">Contract</span>
                                    <span>- 100</span>
                                </div>
                                <div>
                                    <span>10%</span>
                                </div>    
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="d-flex align-items-center">
                                    <i data-feather='circle' class="font-medium-2 text-success"></i>
                                    <span class="font-weight-bold ml-75 mr-25">Outsourching</span>
                                    <span>- 100</span>
                                </div>
                                <div>
                                    <span>10%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}

                {{-- <div class="col-4">
                    <div class="card">
                        <div
                            class="card-header d-flex justify-content-between align-items-sm-center align-items-start flex-sm-row flex-column">
                            <div class="header-left">
                                <h4 class="card-title">Length Of Service</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <canvas class="bar-chart-exs chartjs" data-height="300"></canvas>
                        </div>
                    </div>
                </div> --}}
            @endunlessrole

            {{-- <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Job Level</h4>
                    </div>
                    <div class="card-body">
                        <div class="progress height-12-per" style="height: 10% !important">
                            <div class="progress-bar bg-danger h" role="progressbar" style="width: 40%" aria-valuenow="40"
                                aria-valuemin="0" aria-valuemax="100">
                            </div>
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 30%"
                                aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">
                            </div>
                            <div class="progress-bar bg-primary" role="progressbar" style="width: 10%"
                                aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                            </div>
                            <div class="progress-bar bg-success" role="progressbar" style="width: 15%"
                                aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">
                            </div>
                            <div class="progress-bar bg-info" role="progressbar" style="width: 5%" aria-valuenow="5"
                                aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                       <div class="d-flex justify-content-between mt-3">
                            <div class="d-flex align-items-center">
                                <i data-feather='circle' class="font-medium-2 text-danger"></i>
                                <span class="font-weight-bold ml-75 mr-25">Senior</span>
                            </div>
                            <div>
                                <span>768</span>
                            </div>
                        </div>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <i data-feather='circle' class="font-medium-2 text-warning"></i>
                                <span class="font-weight-bold ml-75 mr-25">Manager</span>
                            </div>
                            <div>
                                <span>432</span>
                            </div>
                        </div>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <i data-feather='circle' class="font-medium-2 text-primary"></i>
                                <span class="font-weight-bold ml-75 mr-25">Junior</span>
                            </div>
                            <div>
                                <span>112</span>
                            </div>
                        </div>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <i data-feather='circle' class="font-medium-2 text-success"></i>
                                <span class="font-weight-bold ml-75 mr-25">Supervisor</span>
                            </div>
                            <div>
                                <span>145</span>
                            </div>
                        </div>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <i data-feather='circle' class="font-medium-2 text-info"></i>
                                <span class="font-weight-bold ml-75 mr-25">Internship</span>
                            </div>
                            <div>
                                <span>43</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}

            <!--/ Medal Card -->
        </div>
    </section>


    <section style="display: none">
        <div class="app-calendar overflow-hidden border">
            <div class="row g-0">
                <!-- Sidebar -->
                <div class="col app-calendar-sidebar flex-grow-0 overflow-hidden d-flex flex-column"
                    id="app-calendar-sidebar">
                    <div class="sidebar-wrapper">
                        <div class="card-body pb-0">
                            <h5 class="section-label mb-1">
                                <span class="align-middle">Filter</span>
                            </h5>
                            <div class="form-check mb-1">
                                <input type="checkbox" class="form-check-input select-all" id="select-all" checked />
                                <label class="form-check-label" for="select-all">View All</label>
                            </div>
                            <div class="calendar-events-filter">
                                <div class="form-check form-check-danger mb-1">
                                    <input type="checkbox" class="form-check-input input-filter" id="personal"
                                        data-value="personal" checked />
                                    <label class="form-check-label" for="personal">Personal</label>
                                </div>
                                <div class="form-check form-check-primary mb-1">
                                    <input type="checkbox" class="form-check-input input-filter" id="business"
                                        data-value="business" checked />
                                    <label class="form-check-label" for="business">Business</label>
                                </div>
                                <div class="form-check form-check-warning mb-1">
                                    <input type="checkbox" class="form-check-input input-filter" id="family"
                                        data-value="family" checked />
                                    <label class="form-check-label" for="family">Family</label>
                                </div>
                                <div class="form-check form-check-success mb-1">
                                    <input type="checkbox" class="form-check-input input-filter" id="holiday"
                                        data-value="holiday" checked />
                                    <label class="form-check-label" for="holiday">Holiday</label>
                                </div>
                                <div class="form-check form-check-info">
                                    <input type="checkbox" class="form-check-input input-filter" id="etc"
                                        data-value="etc" checked />
                                    <label class="form-check-label" for="etc">ETC</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-auto">
                        <img src="{{ asset('images/pages/calendar-illustration.png') }}" alt="Calendar illustration"
                            class="img-fluid" />
                    </div>
                </div>
                <!-- /Sidebar -->

                <!-- Calendar -->
                <div class="col position-relative">
                    <div class="card shadow-none border-0 mb-0 rounded-0">
                        <div class="card-body pb-0">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
                <!-- /Calendar -->
                <div class="body-content-overlay"></div>
            </div>
        </div>
        <!-- Calendar Add/Update/Delete event modal-->
        <div class="modal modal-slide-in event-sidebar fade" id="add-new-sidebar">
            <div class="modal-dialog sidebar-lg">
                <div class="modal-content p-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                    <div class="modal-header mb-1">
                        <h5 class="modal-title">Add Event</h5>
                    </div>
                    <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                        <form class="event-form needs-validation" data-ajax="false" novalidate>
                            <div class="mb-1">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" class="form-control" id="title" name="title"
                                    placeholder="Event Title" required />
                            </div>
                            <div class="mb-1">
                                <label for="select-label" class="form-label">Label</label>
                                <select class="select2 select-label form-select w-100" id="select-label"
                                    name="select-label">
                                    <option data-label="primary" value="Business" selected>Business</option>
                                    <option data-label="danger" value="Personal">Personal</option>
                                    <option data-label="warning" value="Family">Family</option>
                                    <option data-label="success" value="Holiday">Holiday</option>
                                    <option data-label="info" value="ETC">ETC</option>
                                </select>
                            </div>
                            <div class="mb-1 position-relative">
                                <label for="start-date" class="form-label">Start Date</label>
                                <input type="text" class="form-control" id="start-date" name="start-date"
                                    placeholder="Start Date" />
                            </div>
                            <div class="mb-1 position-relative">
                                <label for="end-date" class="form-label">End Date</label>
                                <input type="text" class="form-control" id="end-date" name="end-date"
                                    placeholder="End Date" />
                            </div>
                            <div class="mb-1">
                                <div class="form-check form-switch">
                                    <input type="checkbox" class="form-check-input allDay-switch" id="customSwitch3" />
                                    <label class="form-check-label" for="customSwitch3">All Day</label>
                                </div>
                            </div>
                            <div class="mb-1">
                                <label for="event-url" class="form-label">Event URL</label>
                                <input type="url" class="form-control" id="event-url"
                                    placeholder="https://www.google.com" />
                            </div>
                            <div class="mb-1 select2-primary">
                                <label for="event-guests" class="form-label">Add Guests</label>
                                <select class="select2 select-add-guests form-select w-100" id="event-guests" multiple>
                                    <option data-avatar="1-small.png" value="Jane Foster">Jane Foster</option>
                                    <option data-avatar="3-small.png" value="Donna Frank">Donna Frank</option>
                                    <option data-avatar="5-small.png" value="Gabrielle Robertson">Gabrielle Robertson
                                    </option>
                                    <option data-avatar="7-small.png" value="Lori Spears">Lori Spears</option>
                                    <option data-avatar="9-small.png" value="Sandy Vega">Sandy Vega</option>
                                    <option data-avatar="11-small.png" value="Cheryl May">Cheryl May</option>
                                </select>
                            </div>
                            <div class="mb-1">
                                <label for="event-location" class="form-label">Location</label>
                                <input type="text" class="form-control" id="event-location"
                                    placeholder="Enter Location" />
                            </div>
                            <div class="mb-1">
                                <label class="form-label">Description</label>
                                <textarea name="event-description-editor" id="event-description-editor" class="form-control"></textarea>
                            </div>
                            <div class="mb-1 d-flex">
                                <button type="submit" class="btn btn-primary add-event-btn me-1">Add</button>
                                <button type="button" class="btn btn-outline-secondary btn-cancel"
                                    data-bs-dismiss="modal">Cancel</button>
                                <button type="submit"
                                    class="btn btn-primary update-event-btn d-none me-1">Update</button>
                                <button class="btn btn-outline-danger btn-delete-event d-none">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Calendar Add/Update/Delete event modal-->
    </section>
    <!-- Dashboard HR ends -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    {{-- <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script> --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/charts/chart.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

    <!-- Vendor js files -->
    <script src="{{ asset(mix('vendors/js/calendar/fullcalendar.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/app-calendar-events.js')) }}"></script>
    <script>
        var events = [{
                id: 1,
                url: '',
                title: 'Design Review',
                start: date,
                end: nextDay,
                allDay: false,
                extendedProps: {
                    calendar: 'Business'
                }
            },
            {
                id: 2,
                url: '',
                title: 'Meeting With Client',
                start: new Date(date.getFullYear(), date.getMonth() + 1, -11),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -10),
                allDay: true,
                extendedProps: {
                    calendar: 'Business'
                }
            },
            {
                id: 3,
                url: '',
                title: 'Family Trip',
                allDay: true,
                start: new Date(date.getFullYear(), date.getMonth() + 1, -9),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -7),
                extendedProps: {
                    calendar: 'Holiday'
                }
            },
            {
                id: 4,
                url: '',
                title: "Doctor's Appointment",
                start: new Date(date.getFullYear(), date.getMonth() + 1, -11),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -10),
                allDay: true,
                extendedProps: {
                    calendar: 'Personal'
                }
            },
            {
                id: 5,
                url: '',
                title: 'Dart Game?',
                start: new Date(date.getFullYear(), date.getMonth() + 1, -13),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -12),
                allDay: true,
                extendedProps: {
                    calendar: 'ETC'
                }
            },
            {
                id: 6,
                url: '',
                title: 'Meditation',
                start: new Date(date.getFullYear(), date.getMonth() + 1, -13),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -12),
                allDay: true,
                extendedProps: {
                    calendar: 'Personal'
                }
            },
            {
                id: 7,
                url: '',
                title: 'Dinner',
                start: new Date(date.getFullYear(), date.getMonth() + 1, -13),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -12),
                allDay: true,
                extendedProps: {
                    calendar: 'Family'
                }
            },
            {
                id: 8,
                url: '',
                title: 'Product Review',
                start: new Date(date.getFullYear(), date.getMonth() + 1, -13),
                end: new Date(date.getFullYear(), date.getMonth() + 1, -12),
                allDay: true,
                extendedProps: {
                    calendar: 'Business'
                }
            },
            {
                id: 9,
                url: '',
                title: 'Monthly Meeting',
                start: nextMonth,
                end: nextMonth,
                allDay: true,
                extendedProps: {
                    calendar: 'Business'
                }
            },
            {
                id: 10,
                url: '',
                title: 'Monthly Checkup',
                start: prevMonth,
                end: prevMonth,
                allDay: true,
                extendedProps: {
                    calendar: 'Personal'
                }
            }
        ];
    </script>
    <script src="{{ asset(mix('js/scripts/pages/app-calendar.js')) }}"></script>
    <!-- Page js files -->
    {{-- <script src="{{ asset(mix('js/scripts/pages/dashboard-analytics.js')) }}"></script> --}}
    <script src="{{ asset(mix('js/scripts/pages/app-invoice-list.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/charts/chart-chartjs.js')) }}"></script>
    <script src="{{ asset('js/core.js') }}"></script>
    <script src="{{ asset('js/chart.js') }}"></script>
    <script src="{{ asset('js/animated.js') }}"></script>
    {{-- <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script> --}}
    <script>
        am4core.useTheme(am4themes_animated);

        var chartMin = 0;
        var chartMax = 0;

        var data = {
            score: 52.7,
            gradingData: [{
                    title: "Young",
                    advice: "Young",
                    color: "#28c76f",
                    lowScore: 18,
                    highScore: 24
                },
                {
                    title: "Adult",
                    advice: "Adult",
                    color: "#00cfe8",
                    lowScore: 25,
                    highScore: 39
                },
                {
                    title: "Middle-aged",
                    advice: "Middle-aged",
                    color: "#ff9f43",
                    lowScore: 40,
                    highScore: 59
                },
                {
                    title: "Elderly",
                    advice: "Elderly",
                    color: "#ea5455",
                    lowScore: 60,
                    highScore: 74
                }
            ]
        };

        /**
        Grading Lookup
        */
        function lookUpGrade(lookupScore, grades) {
            // Only change code below this line
            for (var i = 0; i < grades.length; i++) {
                if (
                    grades[i].lowScore < lookupScore &&
                    grades[i].highScore >= lookupScore
                ) {
                    return grades[i];
                }
            }
            return null;
        }

        // create chart
        var chart = am4core.create("chartdiv", am4charts.GaugeChart);
        chart.hiddenState.properties.opacity = 0;
        chart.fontSize = 11;
        chart.innerRadius = am4core.percent(80);
        chart.resizable = true;

        /**
        Grade and Target above the gauge
        */
        var topContainer = chart.chartContainer.createChild(am4core.Container);
        topContainer.layout = "absolute";
        topContainer.toBack();
        topContainer.width = am4core.percent(100);

        /**
         * Normal axis
         */
        var axis = chart.xAxes.push(new am4charts.ValueAxis());
        axis.min = chartMin;
        axis.max = chartMax;
        axis.strictMinMax = true;
        axis.renderer.radius = am4core.percent(80);
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 0;
        axis.renderer.ticks.template.disabled = false;
        axis.renderer.ticks.template.strokeOpacity = 0;
        axis.renderer.ticks.template.strokeWidth = 0.5;
        axis.renderer.ticks.template.length = 5;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.radius = am4core.percent(15);
        axis.renderer.labels.template.fontSize = "0.9em";
        axis.renderer.labels.template.fill = am4core.color("#757575");

        /**
         * Axis for ranges
         */
        var axis2 = chart.xAxes.push(new am4charts.ValueAxis());
        axis2.min = chartMin;
        axis2.max = chartMax;
        axis2.renderer.radius = am4core.percent(105); // figure out how to move labels instead
        axis2.strictMinMax = true;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = false;
        axis2.renderer.grid.template.opacity = 0;
        axis2.renderer.labels.template.bent = true;
        axis2.renderer.labels.template.fill = am4core.color("#000");
        axis2.renderer.labels.template.fontWeight = "bold";
        axis2.renderer.labels.template.fillOpacity = 0; //hide
        axis2.numberFormatter.numberFormat = "#a";

        /**
        Ranges
        */

        for (let grading of data.gradingData) {
            var range = axis2.axisRanges.create();
            range.axisFill.fill = am4core.color(grading.color);

            range.axisFill.fillOpacity = 1;

            range.axisFill.zIndex = -1;
            range.value = grading.lowScore > chartMin ? grading.lowScore : chartMin;
            range.endValue = grading.highScore < chartMax ? grading.highScore : chartMax;
            range.grid.strokeOpacity = 0;
            range.stroke = am4core.color(grading.color).lighten(-0.1);
            range.label.inside = true;
            range.label.text = grading.title.toUpperCase();
            range.label.inside = true;
            range.label.location = 0.5;
            range.label.inside = true;
            range.label.radius = am4core.percent(10);
            range.label.paddingBottom = -5; // ~half font size
            range.label.fontSize = "0.9em";
        }

        var matchingGrade = lookUpGrade(data.score, data.gradingData);

        /**
         * Metric Value
         */

        var labelMetricValue = chart.radarContainer.createChild(am4core.Label);
        labelMetricValue.isMeasured = false;
        labelMetricValue.fontSize = "2em";
        labelMetricValue.x = am4core.percent(50);
        labelMetricValue.paddingBottom = 15;
        labelMetricValue.horizontalCenter = "middle";
        labelMetricValue.verticalCenter = "bottom";
        //labelMetricValue.dataItem = data;
        labelMetricValue.text = data.score.toFixed(1) + " Yr";
        //labelMetricValue.text = "{score}";
        labelMetricValue.fill = am4core.color(matchingGrade.color);

        /**
         * Advice
         */

        //var label2 = chart.radarContainer.createChild(am4core.Label);
        var labelAdvice = chart.createChild(am4core.Label);
        labelAdvice.isMeasured = false;
        labelAdvice.fontSize = "1em";
        //labelAdvice.paddingTop = 150;
        labelAdvice.horizontalCenter = "middle";
        labelAdvice.verticalCenter = "bottom";
        //labelAdvice.text = matchingGrade.title.toUpperCase();
        // labelAdvice.text = "hello";
        labelAdvice.fill = am4core.color(matchingGrade.color);
        labelAdvice.dx = 176;
        labelAdvice.dy = 120;

        /**
         * Hand
         */
        var hand = chart.hands.push(new am4charts.ClockHand());
        hand.axis = axis2;
        hand.radius = am4core.percent(85);
        hand.innerRadius = am4core.percent(50);
        hand.startWidth = 10;
        hand.pixelHeight = 10;
        hand.pin.disabled = true;
        hand.value = data.score;
        hand.fill = am4core.color("#444");
        hand.stroke = am4core.color("#000");

        var handTarget = chart.hands.push(new am4charts.ClockHand());
        handTarget.axis = axis2;
        handTarget.radius = am4core.percent(100);
        handTarget.innerRadius = am4core.percent(105);
        handTarget.fill = axis2.renderer.line.stroke;
        handTarget.stroke = axis2.renderer.line.stroke;
        handTarget.pin.disabled = true;
        handTarget.pin.radius = 10;
        handTarget.startWidth = 10;
        handTarget.fill = am4core.color("#444");
        handTarget.stroke = am4core.color("#000");

        hand.events.on("positionchanged", function() {
            var t = axis2.positionToValue(hand.currentPosition).toFixed(0);
            var formattedValue = chart.numberFormatter.format(t, "#.#a");
            labelMetricValue.text = formattedValue;

            var value2 = axis.positionToValue(hand.currentPosition);
            var matchingGrade = lookUpGrade(axis.positionToValue(hand.currentPosition), data.gradingData);
            labelAdvice.text = matchingGrade.advice.toUpperCase();
            labelAdvice.fill = am4core.color(matchingGrade.color);
            labelAdvice.stroke = am4core.color(matchingGrade.color);
            labelMetricValue.fill = am4core.color(matchingGrade.color);
        })

        setInterval(function() {
            var value = chartMin + Math.random() * (chartMax - chartMin);
            var value = 0; // 2016 12500, 2019 150000, 2021 220000
            hand.showValue(value, 1000, am4core.ease.cubicOut);

            value = 0;
            handTarget.showValue(value, 1000, am4core.ease.cubicOut);
            axis2.axisRanges.values[0].axisFill.fillOpacity = 0.2;
            axis2.axisRanges.values[1].axisFill.fillOpacity = 0.2;
        }, 2000);
    </script>
    <script>
        // Color Variables
        var primaryColorShade = '#836AF9',
            yellowColor = '#ffe800',
            successColorShade = '#28dac6',
            warningColorShade = '#ffe802',
            warningLightColor = '#FDAC34',
            infoColorShade = '#299AFF',
            greyColor = '#4F5D70',
            blueColor = '#2c9aff',
            blueLightColor = '#84D0FF',
            greyLightColor = '#EDF1F4',
            tooltipShadow = 'rgba(0, 0, 0, 0.25)',
            lineChartPrimary = '#666ee8',
            lineChartDanger = '#ff4961',
            labelColor = '#6e6b7b',
            grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout
        var barChartEx = $('.bar-chart-exs');

        if (barChartEx.length) {
            var barChartExample = new Chart(barChartEx, {
                type: 'bar',
                options: {
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        // Updated default tooltip UI
                        shadowOffsetX: 1,
                        shadowOffsetY: 1,
                        shadowBlur: 8,
                        shadowColor: tooltipShadow,
                        backgroundColor: window.colors.solid.white,
                        titleFontColor: window.colors.solid.black,
                        bodyFontColor: window.colors.solid.black
                    },
                    scales: {
                        xAxes: [{
                            barThickness: 15,
                            display: true,
                            gridLines: {
                                display: true,
                                color: grid_line_color,
                                zeroLineColor: grid_line_color
                            },
                            scaleLabel: {
                                display: false
                            },
                            ticks: {
                                fontColor: labelColor
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: grid_line_color,
                                zeroLineColor: grid_line_color
                            },
                            ticks: {
                                stepSize: 100,
                                min: 0,
                                fontColor: labelColor
                            }
                        }]
                    }
                },
                data: {
                    labels: ['< 1 Yr', '1-3 Yr', '3-5 Yr', '5-10 Yr', '> 10 Yr'],
                    datasets: [{
                        data: [50, 235, 643, 543, 350],
                        backgroundColor: successColorShade,
                        borderColor: 'transparent'
                    }]
                }
            });
        }
    </script>
    <script>
        var primaryColorShade = '#836AF9',
            yellowColor = '#ffe800',
            successColorShade = '#28dac6',
            warningColorShade = '#ffe802',
            warningLightColor = '#FDAC34',
            infoColorShade = '#299AFF',
            greyColor = '#4F5D70',
            blueColor = '#2c9aff',
            blueLightColor = '#84D0FF',
            greyLightColor = '#EDF1F4',
            tooltipShadow = 'rgba(0, 0, 0, 0.25)',
            lineChartPrimary = '#666ee8',
            lineChartDanger = '#ff4961',
            labelColor = '#6e6b7b',
            grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout
        var departement = $('.departement');

        if (departement.length) {
            var departementample = new Chart(departement, {
                type: 'bar',
                options: {
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        // Updated default tooltip UI
                        shadowOffsetX: 1,
                        shadowOffsetY: 1,
                        shadowBlur: 8,
                        shadowColor: tooltipShadow,
                        backgroundColor: window.colors.solid.white,
                        titleFontColor: window.colors.solid.black,
                        bodyFontColor: window.colors.solid.black
                    },
                    scales: {
                        xAxes: [{
                            barThickness: 15,
                            display: true,
                            gridLines: {
                                display: true,
                                color: grid_line_color,
                                zeroLineColor: grid_line_color
                            },
                            scaleLabel: {
                                display: false
                            },
                            ticks: {
                                fontColor: labelColor
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: grid_line_color,
                                zeroLineColor: grid_line_color
                            },
                            ticks: {
                                stepSize: 100,
                                min: 0,
                                fontColor: labelColor
                            }
                        }]
                    }
                },
                data: <?= json_encode($chartData_employee) ?>
            });
        }
    </script>

    <script>
        var primaryColorShade = '#836AF9',
            yellowColor = '#ffe800',
            successColorShade = '#28dac6',
            warningColorShade = '#ffe802',
            warningLightColor = '#FDAC34',
            infoColorShade = '#299AFF',
            greyColor = '#4F5D70',
            blueColor = '#2c9aff',
            blueLightColor = '#84D0FF',
            greyLightColor = '#EDF1F4',
            tooltipShadow = 'rgba(0, 0, 0, 0.25)',
            lineChartPrimary = '#666ee8',
            lineChartDanger = '#ff4961',
            labelColor = '#6e6b7b',
            grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout
        var hire = $('.hire');

        if (hire.length) {
            var hireample = new Chart(hire, {
                type: 'bar',
                options: {
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    responsiveAnimationDuration: 500,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        // Updated default tooltip UI
                        shadowOffsetX: 1,
                        shadowOffsetY: 1,
                        shadowBlur: 8,
                        shadowColor: tooltipShadow,
                        backgroundColor: window.colors.solid.white,
                        titleFontColor: window.colors.solid.black,
                        bodyFontColor: window.colors.solid.black
                    },
                    scales: {
                        xAxes: [{
                            barThickness: 15,
                            display: true,
                            gridLines: {
                                display: true,
                                color: grid_line_color,
                                zeroLineColor: grid_line_color
                            },
                            scaleLabel: {
                                display: false
                            },
                            ticks: {
                                fontColor: labelColor
                            }
                        }],
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: grid_line_color,
                                zeroLineColor: grid_line_color
                            },
                            ticks: {
                                // stepSize: 100,
                                min: 0,
                                fontColor: labelColor
                            }
                        }]
                    }
                },
                data: <?= json_encode($chartData) ?>
            });
        }
    </script>
    <script>
        var $gainedChart = document.querySelector('.gained-chart');
        var gainedChartOptions;
        var gainedChart;
        var $averageAge = document.querySelector('.average-age');
        var averageAgeOptions;
        var averageAge;

        gainedChartOptions = {
            chart: {
                height: 100,
                type: 'area',
                toolbar: {
                    show: false
                },
                sparkline: {
                    enabled: true
                },
                grid: {
                    show: false,
                    padding: {
                        left: 0,
                        right: 0
                    }
                }
            },
            colors: [window.colors.solid.primary],
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth',
                width: 2.5
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shadeIntensity: 0.9,
                    opacityFrom: 0.7,
                    opacityTo: 0.5,
                    stops: [0, 80, 100]
                }
            },
            series: [{
                name: 'Subscribers',
                data: [28, 40, 36, 52, 38, 60, 55]
            }],
            xaxis: {
                labels: {
                    show: false
                },
                axisBorder: {
                    show: false
                }
            },
            yaxis: [{
                y: 0,
                offsetX: 0,
                offsetY: 0,
                padding: {
                    left: 0,
                    right: 0
                }
            }],
            tooltip: {
                x: {
                    show: false
                }
            }
        };
        gainedChart = new ApexCharts($gainedChart, gainedChartOptions);
        gainedChart.render();

        averageAgeOptions = {
            chart: {
                height: 100,
                type: 'area',
                toolbar: {
                    show: false
                },
                sparkline: {
                    enabled: true
                },
                grid: {
                    show: false,
                    padding: {
                        left: 0,
                        right: 0
                    }
                }
            },
            colors: [window.colors.solid.warning],
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth',
                width: 2.5
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shadeIntensity: 0.9,
                    opacityFrom: 0.7,
                    opacityTo: 0.5,
                    stops: [0, 80, 100]
                }
            },
            series: [{
                name: 'Subscribers',
                data: [28, 40, 36, 52, 38, 35, 25]
            }],
            xaxis: {
                labels: {
                    show: false
                },
                axisBorder: {
                    show: false
                }
            },
            yaxis: [{
                y: 0,
                offsetX: 0,
                offsetY: 0,
                padding: {
                    left: 0,
                    right: 0
                }
            }],
            tooltip: {
                x: {
                    show: false
                }
            }
        };
        averageAge = new ApexCharts($averageAge, averageAgeOptions);
        averageAge.render();
    </script>
@endsection
