{{-- Extends layout --}}
@extends('layouts/contentLayoutMaster')

@section('title', $page_title) 

{{-- Content --}} 
@section('content')
<div class="row">
    <div class="col-xs-8 col-sm-8 col-md-8 offset-lg-2">
        <div class="card">
            {{-- Header --}}
            <div class="card-header border-bottom p-1">
                <div class="head-label">
                    <h4 class="mb-0">Change Password</h4>
                </div>
            </div>
            {{-- Body --}}
            <div class="card-body pt-2">
                {!! Form::model($user, ['method' => 'PATCH','route' => ['users-change-pass', $user->id], 'id' => 'MyForm']) !!}

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            {{ Form::inputText('Name: ', 'name', null, null, ['placeholder' => 'Name', 'disabled']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            {{ Form::inputText('Username: ', 'username', null, null, ['placeholder' => 'Username', 'disabled']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            {{ Form::inputText('Email: ', 'email', null, null, ['placeholder' => 'Email', 'disabled', 'autocomplete' =>'off']) }}
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <hr>
                            {{ Form::inputPassword('Password: ', 'password', null, null, ['placeholder' => 'Password:', 'autocomplete' => 'new-password'])}}
                            {{ Form::inputPassword('Confirm Password: ', 'confirm-password', null, null, ['placeholder' => 'Confirm Password'])}}
                            <ul>
                                <li>Minimum of 8 characters in length</li>
                                <li>Passwords must contain a combination of alpha, numeric, and special characters, where the computing system permits (!@#$%^&*_+=?/~’;’,<>|\)</li>
                            </ul>
                            <hr>
                            <span class='text-warning'>Biarkan kosong, jika tidak ingin mengganti password</span>
                            <hr>
                            <br>
                            <br>
                        </div>
                    </div>
                    <button class="btn btn-primary data-submit mr-1">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
