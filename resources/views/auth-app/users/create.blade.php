{{-- Extends layout --}}
@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 offset-lg-2">
            <div class="card">
                {{-- Header --}}
                <div class="card-header border-bottom p-1">
                    <div class="head-label">
                        <h4 class="mb-0">Create New User</h4>
                    </div>
                    {{ BackButton($route) }}
                </div>
                {{-- Body --}}
                <div class="card-body pt-2">
                    {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'id' => 'MyForm']) !!}
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">

                            {{ Form::inputText('Name: ', 'name', null, null, ['placeholder' => 'Name', 'required']) }}
                            {{ Form::inputText('Username: ', 'username', null, null, ['placeholder' => 'Username', 'required', 'autocomplete' => 'off']) }}
                            {{ Form::inputText('Email: ', 'email', null, null, ['placeholder' => 'Email', 'required', 'autocomplete' => 'off']) }}
                            {{ Form::inputText('Unit/Bagian: ', 'unit', null, null, ['placeholder' => 'Unit/Bagian', 'required']) }}
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                            {{ Form::inputPassword('Password: ', 'password', null, '.pass', ['placeholder' => 'Password:', 'required', 'autocomplete' => 'new-password']) }}
                            {{ Form::inputPassword('Confirm Password: ', 'confirm-password', null, '.pass', ['placeholder' => 'Confirm Password', 'required', 'equalTo' => '#password']) }}
                            <div class="form-group">
                                <strong>Cabang:</strong>
                                <select class="form-control" name="branch" required>
                                    <option value="">Pilih Cabang</option>
                                    @foreach ($branches as $code => $name)
                                        <option value="{{ $code }}">{{ $code }} - {{ $name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Role:</strong>
                                {!! Form::select('roles[]', $roles, [], ['class' => 'form-control', 'multiple', 'required']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="checked"
                                    name="status" checked="">
                                <label class="form-check-label" for="inlineCheckbox1">Active</label>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                    <button class="btn btn-primary data-submit mr-1">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection --}}

@section('page-script')
    <script>
        $(document).ready(function() {
            // Set the password when the form is loaded
            var newPassword = "";
            $("#password").val(newPassword);
            $("#confirm-password").val(newPassword);
        });
    </script>
@endsection
