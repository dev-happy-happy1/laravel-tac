<nav class="header-navbar navbar navbar-expand-lg align-items-center  navbar-light navbar-shadow {{ $configData['navbarColor'] }}">
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
           <a class="navbar-brand" href="{{ route('login') }}">
              <img src="{{ asset('images/landing/Group 96210.png') }}" alt="Logo" lass="d-inline-block align-text-top" style="padding-bottom: 2px">
            </a>
        </div>
        {{-- <ul class="nav navbar-nav align-items-center ml-auto">
          <li class="nav-item d-none d-lg-block border-right"><a class="nav-link nav-link-style"><i class="ficon" data-feather="{{($configData['theme'] === 'dark') ? 'sun' : 'moon' }}"></i></a></li>
        </li> --}}
        <ul class="nav navbar-nav align-items-center ml-auto">
          <li class="nav-item d-none d-lg-block px-1"><a href="{{ route('faq') }}" class="text-dark">FAQ</a></li>
          <li class="nav-item d-none d-lg-block px-1"><a href="{{ route('contact') }}" class="text-dark">Contact</a></li>
          <li class="nav-item d-none d-lg-block px-1"><a href="{{ route('login') }}" class="btn btn-primary">Sign In</a></li>
        </li>
      </ul>
    </div>
  </nav>

