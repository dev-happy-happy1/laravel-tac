<div class="form-group">
    @php
    $cek = 'form-control ' . $class;
    if($errors->has($name)){
    $cek = 'form-control ' . $class . isValid($errors->has($name)) ;
    }
    $highlightAsterisk = strpos($label, '*') !== false ? 'text-danger' : '';
    @endphp
    <strong>
        {!! str_replace('*', '<span class="text-danger">*</span>', $label) !!}
        @if ($note)
        <small tabindex="-1" class="text-secondary note-{{ $name }}"> - {{ $note }}</small>
        @endif
    </strong>
    {{ Form::email($name, $value, array_merge(['class' => $cek, 'id' => $name], $attributes)) }}
    @if ($info)
    <small class="form-text text-danger">{{ $info }}</small>
    @endif
    <p class="invalid-feedback">{{ $errors->first($name) }}</p>
</div>