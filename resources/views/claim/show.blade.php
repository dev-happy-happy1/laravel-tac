@extends('layouts.contentLayoutMaster')

@section('title', $page_title)

@section('content')
    <div>
        <div>
            <div class="card card-custom">
                <div class="card-header border-bottom p-1">
                    <div class="head-label">
                        <h4 class="mb-0">
                            View Claim
                        </h4>
                    </div>
                    {{ BackButton($route) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="mb-1">
                            <strong>Status Claim:</strong>
                            <span style="display: block; padding-top: 5px;">
                                {!! statusClaim($claim) !!}
                            </span>
                        </div>

                        @if (!empty($claim->note))
                            <div class="mb-1"
                                style="border: 2px solid red; padding: 10px; background-color: #ffe5e5; border-radius: 5px;">
                                <strong>Alasan Reject</strong>
                                <span style="display: block; padding-top: 5px;">
                                    {{ $claim->note }}
                                </span>
                            </div>
                        @endif

                    </div>
                </div>
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="mb-1">
                            <strong>Claim Reff:</strong>
                            {{ $claim->code }}
                        </div>
                        <div class="mb-1">
                            <strong>Maker:</strong>
                            {{ $user->name }}
                        </div>
                        <div class="mb-1">
                            <strong>Bagian:</strong>
                            {{ $user->unit }}
                        </div>
                        <div class="mb-1">
                            <strong>Data Claim Request</strong>
                        </div>
                        <div class="mb-1">
                            <strong>Jenis Claim</strong>
                            <span style="display: block; padding-top: 5px;">
                                {{ $claim->type_claim }}
                            </span>
                        </div>
                        <div class="mb-1">
                            <strong>Cabang</strong>
                            <span style="display: block; padding-top: 5px;">
                                {{ $branch->code }} - {{ $branch->name }}
                            </span>
                        </div>
                        @if ($claim->type == 'Fee Transfer')
                            <div class="mb-1">
                                <strong>No CIF</strong>
                                <span style="display: block; padding-top: 5px;">
                                    {{ $claim->cif }}
                                </span>
                            </div>
                            <div class="mb-1">
                                <strong>Nama Nasabah</strong>
                                <span style="display: block; padding-top: 5px;">
                                    {{ $claim->cif_name }}
                                </span>
                            </div>
                        @else
                            <div class="mb-1">
                                <strong>Judul Event</strong>
                                <span style="display: block; padding-top: 5px;">
                                    {{ $claim->judul_event }}
                                </span>
                            </div>
                            <div class="mb-1">
                                <strong>Detail Event</strong>
                                <span style="display: block; padding-top: 5px;">
                                    {{ $claim->detail_event }}
                                </span>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="card card-custom">
                    <div class="card-header d-flex justify-content-center">
                        <h4 class="text-center">
                            Claim {{ $claim->type_claim }}
                        </h4>
                    </div>
                </div>

                @if ($claim->type == 'Fee Transfer')
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Reimburse Fee</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="fee-rows">
                                @foreach ($claim->fees as $claim_fee)
                                    <div class="fee-row flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-3">
                                            <label for="jenisFee" class="form-label">Jenis Fee</label>
                                            <input type="text" class="form-control" value="{{ $claim_fee->type_fee }}"
                                                readonly>
                                        </div>
                                        <div class="mb-1 col-md-2">
                                            <label for="currencyFee" class="form-label">Currency Fee</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_fee->currency_fee }}" readonly>
                                        </div>
                                        <div class="mb-1 col-md-2">
                                            <label for="nominalFee" class="form-label">Nominal Fee</label>
                                            <input type="text" class="form-control" value="{{ $claim_fee->amount_fee }}"
                                                readonly>
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label for="pilihAccount" class="form-label">Account</label>
                                            <input type="text" class="form-control" value="{{ $claim_fee->account }}"
                                                readonly>
                                        </div>
                                        <div class="mb-1 col-md-2">
                                            <label for="status" class="form-label">Status</label>
                                            <input type="text" class="form-control" value="{{ $claim_fee->status }}"
                                                readonly>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Attachment</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="flex-row d-flex mt-1">
                                <div class="mb-1 col-md-6">
                                    <h4 class="form-label">MD Approval</h4>
                                    <ol>
                                        @php $counter = 1 @endphp
                                        @foreach ($claim->documents as $document)
                                            @if ($document->type == 'MD Approval')
                                                <li>
                                                    <a href="{{ Storage::url('public/file/' . $document->filename) }}"
                                                        target="_blank">
                                                        {{ $document->filename }}
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <h4 class="form-label">Bukti Transaksi</h4>
                                    <ol>
                                        @php $counter = 1 @endphp
                                        @foreach ($claim->documents as $document)
                                            @if ($document->type == 'Bukti Transaksi')
                                                <a href="{{ Storage::url('public/file/' . $document->filename) }}"
                                                    target="_blank">
                                                    {{ $document->filename }}
                                                </a>
                                            @endif
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Input Profit</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="fee-rows">
                                @foreach ($claim->profits as $claim_profit)
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">
                                            <label for="pilihAccount" class="form-label">Deal Number</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_profit->deal_number }}" readonly>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <label for="status" class="form-label">Nominal Profit</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_profit->amount_profit }}" readonly>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    {{-- Summary Claim --}}
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Summary Claim</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="flex-row d-flex mt-1">
                                <div class="mb-1 col-md-6">
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">
                                            <label for="totalProfit" class="form-label">Total Profil dalam IDR</label>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $claim->sum_amount_profit }}" readonly>

                                        </div>
                                    </div>
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">

                                            <label for="totalProfit" class="form-label">Total Fee dalam IDR</label>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <input type="text" class="form-control"
                                                value="{{ $claim->sum_amount_fee }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">
                                            <label for="percentage" class="form-label">Percentage Cost To Profit</label>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <input type="text" class="form-control" value="{{ $claim->sum_percent }}"
                                                readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Reimburse Fee</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="fee-rows">
                                @foreach ($claim->fees as $claim_fee)
                                    <div class="fee-row flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-3">
                                            <label for="jenisFee" class="form-label">Jenis Fee</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_fee->type_fee }}" readonly>
                                        </div>
                                        <div class="mb-1 col-md-2">
                                            <label for="currencyFee" class="form-label">Currency Fee</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_fee->currency_fee }}" readonly>
                                        </div>
                                        <div class="mb-1 col-md-2">
                                            <label for="nominalFee" class="form-label">Nominal Fee</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_fee->amount_fee }}" readonly>
                                        </div>
                                        <div class="mb-1 col-md-3">
                                            <label for="pilihAccount" class="form-label">Account</label>
                                            <input type="text" class="form-control" value="{{ $claim_fee->account }}"
                                                readonly>
                                        </div>
                                        <div class="mb-1 col-md-2">
                                            <label for="status" class="form-label">Keterangan</label>
                                            <input type="text" class="form-control"
                                                value="{{ $claim_fee->keterangan }}" readonly>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Attachment</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="flex-row d-flex mt-1">
                                <div class="mb-1 col-md-6">
                                    <h4 class="form-label">MD Approval</h4>
                                    <ol>
                                        @php $counter = 1 @endphp
                                        @foreach ($claim->documents as $document)
                                            @if ($document->type == 'MD Approval')
                                                <li>
                                                    <a href="{{ Storage::url('public/file/' . $document->filename) }}"
                                                        target="_blank">
                                                        {{ $document->filename }}
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <h4 class="form-label">Bukti Transaksi</h4>
                                    <ol>
                                        @php $counter = 1 @endphp
                                        @foreach ($claim->documents as $document)
                                            @if ($document->type == 'Bukti Transaksi')
                                                <a href="{{ Storage::url('public/file/' . $document->filename) }}"
                                                    target="_blank">
                                                    {{ $document->filename }}
                                                </a>
                                            @endif
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Data Karyawan</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="flex-row d-flex mt-1">
                                <div class="mb-1 col-md-4">
                                    <label for="nama" class="form-label">Nama</label>
                                    <input type="text" class="form-control" value="{{ $claim->event_emp_name }}"
                                        readonly>
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label for="nip" class="form-label">NIP</label>
                                    <input type="text" class="form-control" value="{{ $claim->event_emp_nip }}"
                                        readonly>
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label for="bagian" class="form-label">Bagian</label>
                                    <input type="text" class="form-control" value="{{ $claim->event_emp_unit }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
