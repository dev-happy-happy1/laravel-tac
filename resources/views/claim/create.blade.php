@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')
    <form id="createForm" action="{{ route('claim.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">

            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="mb-1">
                            <label for="fieldName" class="form-label">Maker</label>
                            <input type="text" class="form-control" id="fieldName" name="fieldName"
                                value="{{ auth()->user()->name }}" readonly>
                        </div>
                        <div class="mb-1">
                            <label for="claimReff" class="form-label">Claim Reff</label>
                            <input type="text" class="form-control" id="claimReff" name="code" readonly>
                        </div>
                        <div class="mb-1">
                            <label for="jenisClaim" class="form-label">Jenis Claim</label>
                            <select class="form-control" id="jenisClaim" name="type_claim">
                                <option value="">Pilih Jenis Claim</option>
                                <option value="FIT">Fee Incoming Transfer</option>
                                <option value="FOT">Fee Outgoing Transfer</option>
                                <option value="FE">Fee Event</option>
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="cabang" class="form-label">Cabang</label>
                            <select class="form-control" id="cabang" name="branch">
                                <option value="">Pilih Cabang</option>
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="nocif" class="form-label">No CIF</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="nocif" name="cif">
                                <button type="button" class="btn btn-primary" id="cariNasabahButton">Cari</button>
                            </div>
                        </div>
                        <div class="mb-1">
                            <label for="namaNasabah" class="form-label">Nama Nasabah</label>
                            <input type="text" class="form-control" id="namaNasabah" name="cif_name" readonly>
                        </div>
                        <div class="mb-1">
                            <label for="judulevent" class="form-label">Judul Event</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="judulevent" name="judul_event">

                            </div>
                        </div>
                        <div class="mb-1">
                            <label for="detailevent" class="form-label">Detail Event</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="detailevent" name="detail_event">
                            </div>
                        </div>

                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <button id="addclaim" type="button" class="btn btn-primary">Add claim</button>
                    </div>
                </div>
            </div>



            <div class="col-xs-8 col-sm-8 col-md-8"> <!-- Menambahkan form di sebelah kanan -->
                {{-- Fee Transfer --}}
                <div id="feetransfer" style="display: none;">
                    <div class="card card-custom">
                        <div class="card-header d-flex justify-content-center">
                            <h4 id="judulFeeTransfer" class="text-center">
                                Form Claim
                            </h4>
                        </div>
                    </div>

                    {{-- Add Fee --}}
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Input Fee</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="fee-rows">
                                <div class="fee-row flex-row d-flex mt-1">
                                    <div class="mb-1 col-md-3">
                                        <label for="jenisFee" class="form-label">Jenis Fee</label>
                                        <select class="form-control" id="jenisFee" name="feeData[0][type_fee]">
                                            <option value="">Pilih</option>
                                            <option value="Change Amount">Change Amount</option>
                                            <option value="Provisi">Provisi</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-2">
                                        <label for="currencyFee" class="form-label">Currency Fee</label>
                                        <input type="text" class="form-control" id="currencyFee"
                                            name="feeData[0][currency_fee]" readonly>
                                    </div>
                                    <div class="mb-1 col-md-2">
                                        <label for="nominalFee" class="form-label">Nominal Fee</label>
                                        <input type="text" class="form-control" id="nominalFee"
                                            name="feeData[0][amount_fee]">
                                    </div>
                                    <div class="mb-1 col-md-3">
                                        <label for="pilihAccount" class="form-label">Pilih
                                            Account</label>
                                        <select class="form-control pilih_account_select" id="pilihAccount"
                                            name="feeData[0][account]">
                                            <option value="">Pilih</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-2">
                                        <label for="status" class="form-label">Status</label>
                                        <input type="text" class="form-control" id="status" data-id="0"
                                            name="feeData[0][status]" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end mt-2">
                                <button type="button" class="btn btn-primary" id="add-fee">+Add Fee</button>
                            </div>
                        </div>
                    </div>

                    {{-- Add File --}}
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Attachment</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="file-rows">
                                <div class="flex-row d-flex mt-1 file-row">
                                    <div class="mb-1 col-md-6">
                                        <label for="pilih-type-file" class="form-label">Type File</label>
                                        <select class="form-control" id="pilihtypeFile" name="fileData[0][type]">
                                            <option value="">Pilih</option>
                                            <option value="MD Approval">MD Approval</option>
                                            <option value="Bukti Transaksi">Bukti Transaksi</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label for="pilih-file" class="form-label">Add File</label>
                                        <input type="file" class="form-control" id="upload"
                                            name="fileData[0][file]" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="button" id="add-file" class="btn btn-primary">+Add New File</button>
                            </div>
                        </div>
                    </div>
                    {{-- Add Profit --}}
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Input Profit</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="profit-rows">
                                <div class="flex-row d-flex mt-1 profit-row">
                                    <div class="mb-1 col-md-6">
                                        <label for="dealNumber" class="form-label">Deal Number</label>
                                        <input type="text" class="form-control" id="dealNumber"
                                            name="profitData[0][deal_number]">
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label for="nominalProfit" class="form-label">Nominal Profit</label>
                                        <input type="text" class="form-control" id="amountProfit"
                                            name="profitData[0][amount_profit]">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="button" id="add-profit" class="btn btn-primary">+Add Profit</button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <button id="generateSummary" type="button" class="btn font-weight-bold btn-block mb-1"
                            style="background-color: rgb(31, 229, 31); color: white;">
                            Generate Summary
                        </button>
                    </div>

                    {{-- Summary Claim --}}
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Summary Claim</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="flex-row d-flex mt-1">
                                <div class="mb-1 col-md-6">
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">
                                            <label for="totalProfit" class="form-label">Total Profil dalam IDR</label>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <input type="text" class="form-control" id="totalProfit"
                                                name='sum_amount_profit' readonly>

                                        </div>
                                    </div>
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">

                                            <label for="totalProfit" class="form-label">Total Fee dalam IDR</label>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <input type="text" class="form-control" id="totalFee"
                                                name="sum_amount_fee" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <div class="flex-row d-flex mt-1">
                                        <div class="mb-1 col-md-6">
                                            <label for="percentage" class="form-label">Percentage Cost To Profit</label>
                                        </div>
                                        <div class="mb-1 col-md-6">
                                            <input type="text" class="form-control" id="percentage"
                                                name="sum_percent" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button id="submitBtn" type="submit"
                                        class="btn font-weight-bold btn-block btn-primary">
                                        Submit
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route($route . '.index') }}"
                                        class="btn font-weight-bold btn-block btn-danger">Cancel</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                {{-- Fee Event --}}
                <div id="feeEvent" style="display: none;">
                    <div class="card card-custom">
                        <div class="card-header d-flex justify-content-center">
                            <h4 id="feeEvent" class="text-center">
                                Form Claim Fee Event
                            </h4>
                        </div>
                    </div>
                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Input Fee</h4>
                            </div>
                        </div>

                        <div class="card-body">
                            <div id="feeEvent-rows">
                                <div class="feeEvent-row flex-row d-flex mt-1">
                                    <div class="mb-1 col-md-3">
                                        <label for="jenisFeeEvent" class="form-label">Jenis Fee</label>
                                        <select class="form-control" id="jenisFeeEvent" name="feeEventData[0][type_fee]">
                                            <option value="">Pilih</option>
                                            <option value="Entertainment">Entertainment</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-2">
                                        <label for="currencyFee" class="form-label">Currency Fee</label>
                                        <input type="text" class="form-control" id="currencyFeeEvent"
                                            name="feeEventData[0][currency_fee]" readonly>
                                    </div>
                                    <div class="mb-1 col-md-2">
                                        <label for="nominalFeeEvent" class="form-label">Nominal Fee</label>
                                        <input type="text" class="form-control" id="nominalFeeEvent"
                                            name="feeEventData[0][amount_fee]">
                                    </div>
                                    <div class="mb-1 col-md-3">
                                        <label for="pilihAccountEvent" class="form-label">Pilih
                                            Account</label>
                                        <select class="form-control pilih_account_select_event" id="pilihAccountEvent"
                                            name="feeEventData[0][account]">
                                            <option value="">Pilih</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-2">
                                        <label for="keterangan" class="form-label">Keterangan</label>
                                        <input type="text" class="form-control" id="keterangan"
                                            name="feeEventData[0][keterangan]" value="Suspen Inc Tran" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end mt-2">
                                <button type="button" class="btn btn-primary" id="add-fee-event">+Add Fee</button>
                            </div>
                        </div>
                    </div>

                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Input Data Karyawan</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="flex-row d-flex mt-1">
                                <div class="mb-1 col-md-4">
                                    <label for="nama" class="form-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="event_emp_name">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label for="nip" class="form-label">NIP</label>
                                    <input type="text" class="form-control" id="nip" name="event_emp_nip">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label for="bagian" class="form-label">Bagian</label>
                                    <input type="text" class="form-control" id="bagian" name="event_emp_unit">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card card-custom">
                        <div class="card-header border-bottom p-1">
                            <div class="head-label">
                                <h4 class="form-label">Attachment Event</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="file-rows-2">
                                <div class="flex-row d-flex mt-1 file-row-2">
                                    <div class="mb-1 col-md-6">
                                        <label for="pilih-type-file-2" class="form-label">Type File</label>
                                        <select class="form-control" id="pilihTypeFileEvent"
                                            name="fileDataEvent[0][type]">
                                            <option value="">Pilih</option>
                                            <option value="MD Approval">MD Approval</option>
                                            <option value="Bukti Transaksi">Bukti Transaksi</option>
                                        </select>
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label for="pilih-file-2" class="form-label">Add File</label>
                                        <input type="file" class="form-control" id="upload2"
                                            name="fileDataEvent[0][file]" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="button" id="add-file-2" class="btn btn-primary">+Add New File</button>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <button id="submitBtnEvent"
                                        class="btn font-weight-bold btn-block btn-primary">Submit</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route($route . '.index') }}"
                                        class="btn font-weight-bold btn-block btn-danger">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('vendor-script')
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endpush
@push('page-script')
    <!-- Script untuk toggle visibility -->

    <script>
        function showLoading() {
            swal.fire({
                icon: "info",
                title: "Loading...",
                text: "Please wait...",
                showConfirmButton: false,
                allowOutsideClick: false, // Prevent closing by clicking outside
            });
        }
    </script>



    <script>
        $(document).ready(function() {
            function toggleFieldsVisibility() {
                var jenisClaim = $('#jenisClaim').val();
                var judulEventField = $('#judulevent').closest('.mb-1');
                var detailEventField = $('#detailevent').closest('.mb-1');
                var noCIFField = $('#nocif').closest('.mb-1');
                var namaNasabahField = $('#namaNasabah').closest('.mb-1');

                if (jenisClaim === 'FE') {
                    judulEventField.show();
                    detailEventField.show();
                    noCIFField.hide();
                    namaNasabahField.hide();
                } else if (jenisClaim === 'FIT' || jenisClaim === 'FOT') {
                    judulEventField.hide();
                    detailEventField.hide();
                    noCIFField.show();
                    namaNasabahField.show();
                } else {
                    judulEventField.hide();
                    detailEventField.hide();
                    noCIFField.hide();
                    namaNasabahField.hide();
                }
            }

            // Call toggleFieldsVisibility on page load and whenever jenisClaim changes
            $('#jenisClaim').on('change', toggleFieldsVisibility);
            toggleFieldsVisibility();
        });
    </script>

    <!-- Script untuk melakukan pencarian Nasabah -->
    <script>
        $(document).ready(function() {
            $('#cariNasabahButton').on('click', function() {
                var nocif = $('#nocif').val();
                if (nocif) {

                    showLoading();
                    $.ajax({

                        url: '{{ route('dataCif') }}',
                        // url: '{{ route('dataAccount') }}',
                        method: 'GET',
                        data: {
                            cif: nocif
                        },
                        success: function(response) {
                            swal.close();
                            if (response) {
                                $('#namaNasabah').val(response.name);
                                // console.log(response);

                            } else {
                                alert('No CIF tidak ditemukan.');
                            }
                        },
                        error: function(error) {
                            console.error(error);
                            alert('Terjadi kesalahan. Silakan coba lagi.');
                        }
                    });
                } else {
                    alert('Masukkan No CIF.');
                }
            });
        });
    </script>

    <!-- Script untuk mengambil cabang -->
    <script>
        $(document).ready(function() {
            function fetchBranches() {
                $.ajax({
                    url: '{{ url('/branches') }}',
                    type: 'GET',
                    success: function(data) {
                        var branchSelect = $('#cabang');
                        branchSelect.empty();
                        branchSelect.append('<option value="">Pilih Cabang</option>');

                        data.forEach(function(branch) {
                            branchSelect.append('<option value="' + branch.code + '">' + branch
                                .code + ' - ' + branch.name + '</option>');
                        });
                    },
                    error: function(xhr, status, error) {
                        console.error('Error fetching branches:', error);
                        alert('Failed to fetch branches. Please try again.');
                    }
                });
            }

            fetchBranches();
        });
    </script>

    {{-- tombol add claim --}}
    <script>
        var select_account;
        var jenis_claim;
        var amount_fee;

        $(document).ready(function() {
            function handleClaimFormDisplay() {
                var jenisClaim = $('#jenisClaim').val();
                if (jenisClaim === 'FE') {
                    jenis_claim = jenisClaim;
                    $('#feetransfer').hide();
                    $('#feeEvent').show();
                } else if (jenisClaim === 'FIT' || jenisClaim === 'FOT') {
                    jenis_claim = jenisClaim;
                    $('#feetransfer').show();
                    $('#feeEvent').hide();
                    var transferType = jenisClaim === 'FIT' ? 'Incoming' : 'Outgoing';
                    $('#judulFeeTransfer').text(`Form Claim Fee ${transferType} Transfer`);
                } else {
                    $('#feetransfer').hide();
                    $('#feeEvent').hide();
                }
            }

            function generateClaimReff() {
                var nocif = $('#nocif').val();
                var jenisClaim = $('#jenisClaim').val();
                var cabang = $('#cabang').val();
                var date = new Date();
                var day = String(date.getDate()).padStart(2, '0');
                var month = String(date.getMonth() + 1).padStart(2, '0');
                var year = String(date.getFullYear()).substr(-2);
                var randomSeq = Math.floor(1000 + Math.random() * 9000);

                var claimReff = `${jenisClaim}${cabang}${day}${month}${year}${randomSeq}`;
                $('#claimReff').val(claimReff);
            }

            function getDataAccount() {
                var nocif = $('#nocif').val();
                showLoading();
                $.ajax({
                    url: '{{ route('dataAccount') }}',
                    method: 'GET',
                    data: {
                        code: nocif
                    },
                    success: function(response) {
                        swal.close();
                        if (response) {
                            select_account = response;
                            $('#pilihAccount').empty().append('<option value="">Pilih</option>');
                            response.forEach(function(account, index) {
                                var optionText = `${account.curr} - ${account.code}`;
                                var optionValue = account.code;
                                $('#pilihAccount').append(
                                    `<option value="${optionValue}" id="pilihAccount${index}" name="feeData[${index}][account]">${optionText}</option>`
                                );
                            });
                        } else {
                            alert('Data akun tidak ditemukan.');
                        }
                    },
                    error: function(error) {
                        console.error(error);
                        alert('Terjadi kesalahan saat mengambil data akun. Silakan coba lagi.');
                    }
                });
            }

            function getDataAccountCabang() {
                var code_cabang = $('#cabang').val();
                showLoading();
                $.ajax({
                    url: '{{ route('dataAccountCabang') }}',
                    method: 'GET',
                    data: {
                        code: code_cabang
                    },
                    success: function(response) {
                        swal.close();
                        if (response) {
                            select_account = response;
                            $('#pilihAccountEvent').empty().append('<option value="">Pilih</option>');
                            response.forEach(function(account, index) {
                                var optionText = `${account.curr} - ${account.code}`;
                                var optionValue = account.code;
                                $('#pilihAccountEvent').append(
                                    `<option value="${optionValue}" id="pilihAccountEvent${index}" name="feeEventData[${index}][account]">${optionText}</option>`
                                );
                            });
                        } else {
                            alert('Data akun tidak ditemukan.');
                        }
                    },
                    error: function(error) {
                        console.error(error);
                        alert('Terjadi kesalahan saat mengambil data akun. Silakan coba lagi.');
                    }
                });
            }



            $('#addclaim').on('click', function() {
                generateClaimReff();
                handleClaimFormDisplay();

                if (jenis_claim === 'FE') {
                    getDataAccountCabang();
                } else {
                    getDataAccount();
                }

            });
        });
    </script>


    {{-- tombol add row --}}
    <script>
        // Function to add new rows dynamically
        // Function to add new rows dynamically
        function addRow(buttonId, rowClass, containerId, retainValue = false) {
            var addButton = document.getElementById(buttonId);
            var index = document.querySelectorAll("." + rowClass).length -
                1; // Initial index based on the number of existing rows

            // Add event listener for the button click
            addButton.addEventListener("click", function() {
                // Clone the last row
                var rows = document.querySelectorAll("." + rowClass);
                var lastRow = rows[rows.length - 1]; // Get the last row
                var clonedRow = lastRow.cloneNode(true);

                // Increment the index for dynamic naming
                index++;

                // Update the name attributes with the new index
                var inputs = clonedRow.querySelectorAll("select, input");
                inputs.forEach(function(input) {
                    var name = input.getAttribute("name");
                    if (name) {
                        // Replace [0] or any existing index with the current index
                        var newName = name.replace(/\[\d+\]/, "[" + index + "]");
                        input.setAttribute("name", newName);

                        // Clear the value if retainValue is false or if the input is not the keterangan input
                        if (!retainValue || name !== "feeEventData[0][keterangan]") {
                            if (input.type === 'file') {
                                input.value = null; // Ensure file input is reset
                            } else {
                                input.value = ''; // Clear other input values
                            }
                        }
                    }
                });

                // Append the cloned row to the container
                document.getElementById(containerId).appendChild(clonedRow);
            });
        }

        // Call the function for .file-row
        addRow("add-file", "file-row", "file-rows");

        // Call the function for .file-row-2
        addRow("add-file-2", "file-row-2", "file-rows-2");

        // Call the function for .fee-row
        addRow("add-fee", "fee-row", "fee-rows");

        // Call the function for .profit-row
        addRow("add-profit", "profit-row", "profit-rows");

        // Call the function for .feeEvent-row, retainValue is set to true to keep keterangan value
        addRow("add-fee-event", "feeEvent-row", "feeEvent-rows", true);
    </script>

    {{-- Tombol Generate Summary --}}
    <script>
        function fetchConversionRate(currency) {
            return new Promise((resolve, reject) => {
                if (currency === 'IDR') {
                    resolve(1); // If currency is IDR, resolve with conversion rate of 1
                } else {
                    $.ajax({
                        url: '{{ url('api/get-kurs') }}/' + currency,
                        method: 'GET',
                        success: function(response) {
                            const rate = parseFloat(response);
                            resolve(rate);
                        },
                        error: function(xhr, status, error) {
                            console.error('Error fetching conversion rates:', error);
                            reject('Failed to fetch conversion rates. Please try again.');
                        }
                    });
                }
            });
        }

        async function calculateTotalFeeIDR() {
            let totalFeeIDR = 0;
            const feeRows = $('#fee-rows .fee-row');

            for (let i = 0; i < feeRows.length; i++) {
                const row = feeRows[i];
                const amount = parseFloat($(row).find('[name$="[amount_fee]"]').val()) || 0;
                const currency = $(row).find('[name$="[currency_fee]"]').val();

                if (currency === 'IDR') {
                    totalFeeIDR += amount;
                } else {
                    try {
                        const rate = await fetchConversionRate(currency); // Wait for conversion rate
                        console.log('Conversion rate:', rate);
                        totalFeeIDR += amount * rate;
                    } catch (error) {
                        console.error('Error calculating total fee in IDR:', error);
                        totalFeeIDR = 0; // Set totalFeeIDR to 0 or handle differently as needed
                        throw error; // Propagate the error to be caught in the calling function
                    }
                }
            }

            return totalFeeIDR;
        }

        $('#generateSummary').click(async function() {
            let totalProfit = 0;

            // Calculate total profit
            $('#profit-rows .profit-row').each(function() {
                const profit = parseFloat($(this).find('[name$="[amount_profit]"]').val()) || 0;
                totalProfit += profit;
            });

            // Show loading alert
            showLoading();

            // Calculate total fee in IDR asynchronously
            try {
                const totalFeeIDR = await calculateTotalFeeIDR();

                // Calculate percentage cost to profit
                const percentage = totalProfit !== 0 ? (totalFeeIDR / totalProfit) * 100 : 0;

                // Update the fields
                $('#totalProfit').val(totalProfit.toFixed(2));
                $('#totalFee').val(totalFeeIDR.toFixed(2));
                $('#percentage').val(percentage.toFixed(2) + '%');

                // Enable the Submit button after generating summary
                $('#submitBtn').prop('disabled', false);

                // Close the loading alert and show success alert
                swal.close();
            } catch (error) {
                console.error('Error generating summary:', error);

                // Close the loading alert and show error alert
                swal.fire({
                    icon: "error",
                    title: "Error",
                    text: "Failed to generate summary. Please try again.",
                    showConfirmButton: true,
                });
            }
        });

        // Disable Submit button by default
        $('#submitBtn').prop('disabled', true);
    </script>

    <script>
        $('body').on('change', '.pilih_account_select', function() {
            const selectedAccountCode = $(this).val();

            const selectedAccount = select_account.find(account => account.code === selectedAccountCode);

            // Find the parent fee-row
            const parentRow = $(this).closest('.fee-row');

            // Set the status and currencyFee input values based on the selected account's status
            if (selectedAccount) {
                parentRow.find('#status').val(selectedAccount.status);
                parentRow.find('#currencyFee').val(selectedAccount.curr);
            } else {
                parentRow.find('#status').val('');
                parentRow.find('#currencyFee').val('');
            }
        });
    </script>

    <script>
        $('body').on('change', '.pilih_account_select_event', function() {
            const selectedAccountCabangCode = $(this).val();

            const selectedAccount = select_account.find(account => account.code === selectedAccountCabangCode);

            // Find the parent fee-row
            const parentRow = $(this).closest('.feeEvent-row');

            // Set the status and currencyFee input values based on the selected account's status
            if (selectedAccount) {
                parentRow.find('#currencyFeeEvent').val(selectedAccount.curr);
                parentRow.find('#keterangan').val('Suspen Inc Tran');
            } else {
                parentRow.find('#currencyFeeEvent').val('');
                parentRow.find('#keterangan').val('');
            }
        });
    </script>

    <script>
        // Menangkap event klik pada tombol submit
        document.getElementById('submitBtn').addEventListener('click', function() {
            // Tampilkan SweetAlert
            Swal.fire({
                title: 'Are you sure?',
                text: 'You are about to submit this form.',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, submit it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Submit form jika user menekan tombol "Yes"
                    document.getElementById('MyForm').submit();
                }
            });
        });

        document.getElementById('submitBtnEvent').addEventListener('click', function() {
            // Tampilkan SweetAlert
            Swal.fire({
                title: 'Are you sure?',
                text: 'You are about to submit this form.',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, submit it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // Submit form jika user menekan tombol "Yes"
                    document.getElementById('MyForm').submit();
                }
            });
        });
    </script>
@endpush
