{!! Form::model($permission_edit, [
    'method' => 'PATCH',
    'route' => ['parameter-gl.update', $permission_edit->id],
    'id' => 'MyForm',
]) !!}

{{ Form::inputText('No GL: ', 'gl_no', null, null, ['placeholder' => 'No GL', 'required']) }}

{{ Form::inputText('GL Ref: ', 'gl_ref', null, null, ['placeholder' => 'GL Ref', 'required']) }}

{{ Form::inputText('Description: ', 'gl_description', null, null, ['placeholder' => 'Description', 'required']) }}

{{ Form::inputText('Currency: ', 'gl_currency', null, null, ['placeholder' => 'Currency', 'required']) }}

<div class="row">
    <div class="col-md-6">
        <button onclick="CheckValidation();" type="submit" id="btn-submit"
            class="btn font-weight-bold btn-block btn-primary">
            Update
        </button>
    </div>
    <div class="col-md-6">
        <a href="{{ route($route . '.index') }}" class="btn font-weight-bold btn-block btn-danger">Cancel</a>
    </div>
</div>

{!! Form::close() !!}
