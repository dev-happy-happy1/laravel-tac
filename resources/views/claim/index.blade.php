@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')

    <style>
        .autocomplete-suggestions {
            border: 1px solid rgba(231, 231, 231, 0.664);
            background: #FFF;
            overflow: auto;
            box-shadow: 0px 0px 30px 0px rgb(82 63 105 / 5%);
        }

        .autocomplete-suggestion {
            padding: 2px 5px;
            white-space: nowrap;
            overflow: hidden;
        }

        .autocomplete-selected {
            background: #F0F0F0;
        }

        .autocomplete-suggestions strong {
            font-weight: normal;
            color: #3399FF;
        }

        .autocomplete-group {
            padding: 2px 5px;
        }

        .autocomplete-group strong {
            display: block;
            border-bottom: 1px solid #000;
        }
    </style>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="card card-custom">
                <div class="card-body table-responsive">
                    <div class="card-header border-bottom p-1">
                        <div class="head-label">
                            <h4 class="mb-0">{{ $page_title }}</h4>
                        </div>
                        {{ CreateButton($route, $permission, 'btn-warning') }}
                    </div>
                    <h5 class="pt-1">Claim Fee Incoming/Outgoing Transfer</h5>
                    <table id='table-transfer' class="table table-bordered yajra-datatable">
                        <thead>
                            <tr class="text-center">
                                <th width='30px'>No</th>
                                <th>Tgl Request</th>
                                <th>Code</th>
                                <th>CIF Name</th>
                                <th>CIF</th>
                                <th>Cabang</th>
                                <th>Maker</th>
                                <th>Bagian</th>
                                <th>Jenis Fee</th>
                                <th>No Rekening</th>
                                <th>Currency</th>
                                <th>Nominal Fee</th>
                                <th>Status</th>
                                <th width='100px'>Action</th>
                            </tr>
                        </thead>
                    </table>

                    <h5 class="pt-1">Claim Fee Event</h5>
                    <table id='table-event' class="table table-bordered yajra-datatable">
                        <thead>
                            <tr class="text-center">
                                <th width='30px'>No</th>
                                <th>Tgl Request</th>
                                <th>Code</th>
                                <th>Nama Karyawan</th>
                                <th>NIP</th>
                                <th>Cabang</th>
                                <th>Maker</th>
                                <th>Bagian</th>
                                <th>Jenis Fee</th>
                                <th>No Rekening</th>
                                <th>Currency</th>
                                <th>Nominal Fee</th>
                                <th>Status</th>
                                <th width='100px'>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>



@endsection

@push('vendor-script')
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endpush

@push('page-script')
    <script type="text/javascript">
        $(function() {
            'use strict';

            var dt_basic_table = $('#table-transfer');
            var url = "{{ route('claim.datatableFeeTransfer') }}";

            if (dt_basic_table.length) {
                var dt_basic = dt_basic_table.DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: url,
                        type: 'GET'
                    },
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'created_at',
                            name: 'created_at',
                            render: function(data, type, row) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = String(date.getDate()).padStart(2, '0');
                                    var month = String(date.getMonth() + 1).padStart(2,
                                        '0'); // Months are zero-based
                                    var year = date.getFullYear();
                                    return day + '/' + month + '/' + year;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'code',
                            name: 'code'
                        },
                        {
                            data: 'cif_name',
                            name: 'cif_name'
                        },
                        {
                            data: 'cif',
                            name: 'cif'
                        },
                        {
                            data: 'branches',
                            name: 'branches',
                            render: function(data, type, row) {
                                return data.code + ' - ' + data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.unit;
                            }
                        },
                        {
                            data: 'fee',
                            name: 'fee'
                        },
                        {
                            data: 'account',
                            name: 'account'
                        },
                        {
                            data: 'currency',
                            name: 'currency'
                        },
                        {
                            data: 'amount',
                            name: 'amount'
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: true,
                            searchable: true
                        }
                    ],
                    columnDefs: [{
                        className: "text-center",
                        targets: [0, 13]
                    }],
                    scrollY: "500px",
                    scrollCollapse: true,
                    paging: false,
                    dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                }).on('draw', function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            }
        });
    </script>

    <script type="text/javascript">
        $(function() {
            'use strict';

            var dt_basic_table = $('#table-event');
            var url = "{{ route('claim.datatableFeeEvent') }}";

            if (dt_basic_table.length) {
                var dt_basic = dt_basic_table.DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: url,
                        type: 'GET'
                    },
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'created_at',
                            name: 'created_at',
                            render: function(data, type, row) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = String(date.getDate()).padStart(2, '0');
                                    var month = String(date.getMonth() + 1).padStart(2,
                                        '0'); // Months are zero-based
                                    var year = date.getFullYear();
                                    return day + '/' + month + '/' + year;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'code',
                            name: 'code'
                        },
                        {
                            data: 'event_emp_name',
                            name: 'event_emp_name'
                        },
                        {
                            data: 'event_emp_nip',
                            name: 'event_emp_nip'
                        },
                        {
                            data: 'branches',
                            name: 'branches',
                            render: function(data, type, row) {
                                return data.code + ' - ' + data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.name;
                            }
                        },
                        {
                            data: 'users',
                            name: 'users',
                            render: function(data, type, row) {
                                return data.unit;
                            }
                        },
                        {
                            data: 'fee',
                            name: 'fee'
                        },
                        {
                            data: 'account',
                            name: 'account'
                        },
                        {
                            data: 'currency',
                            name: 'currency'
                        },
                        {
                            data: 'amount',
                            name: 'amount'
                        },
                        {
                            data: 'status',
                            name: 'status',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: true,
                        }
                    ],
                    columnDefs: [{
                        className: "text-center",
                        targets: [0, 13]
                    }],
                    scrollY: "500px",
                    scrollCollapse: true,
                    paging: false,
                    dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                }).on('draw', function() {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            }
        });
    </script>
@endpush
