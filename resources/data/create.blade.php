{{-- Extends layout --}}
@extends('layouts/contentLayoutMaster')

@section('title', $page_title)

{{-- Content --}}
@section('content')
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 offset-lg-2">
            <div class="card">
                {{-- Header --}}
                <div class="card-header border-bottom p-1">
                    <div class="head-label">
                        <h4 class="mb-0">Create News</h4>
                    </div>
                    {{ BackButton($route) }}
                </div>
                {{-- Body --}}
                <div class="card-body pt-2">
                    {!! Form::open(['route' => $route . '.store', 'method' => 'POST', 'id' => 'MyForm', 'enctype'=>'multipart/form-data', 'onsubmit'=>"return validateForm();"]) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::inputText('Title*: ', 'title', null, null, ['placeholder' => 'title', 'required']) }}
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            {{ Form::inputText('URL: ', 'url', null, null, ['placeholder' => 'url']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            {{ Form::inputFile('Banner News: ', 'image', null, null, ['placeholder' => 'file']) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            {{ Form::inputFile('Document Attach: ', 'attach', null, null, ['placeholder' => 'file']) }}
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 pb-2">
                            <strong>
                                Content*:
                            </strong>
                           <textarea id="myeditorinstance" name='content' class="ckeditor" required></textarea>
                        </div>
                    </div>
                    <button class="btn btn-primary data-submit mr-1" type="button">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-script')
     {{-- <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script> --}}
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    {{-- <script>
        ClassicEditor
        .create( document.querySelector( '.ckeditor' ) )
        .catch( error => {
            console.error( error );
        } );

        $('.data-submit').on('click', function(params) {
            var editorContent = CKEDITOR.instances.editor.getData();
            console.log(editorContent);

            if (editorContent == '') {
            alert('Please enter some content in the editor.');
            return false;
            }
        })
        //  function validateForm() {
            
        // }
    </script> --}}
@endsection
