<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiManagementController extends Controller
{
    public $Encrypt;

    public function __construct($Encrypt = null)
    {
        $this->Encrypt = new EncryptionController();
    }
    public function GetAccessToken()
    {
        // dd('Access Token');
        $curl = curl_init();
        $username = 'tac-dev';
        $password = 'dxoDvCR3XICYOrF1VmTkyFI53pYM3F1l';

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://openapidev1.bankmega.local:15000/realms/bpsecurity/protocol/openid-connect/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                'Host: openapidev1.bankmega.local',
                'Content-Type: application/x-www-form-urlencoded',
            ),
            CURLOPT_USERPWD => "$username:$password"
        ));

        $response = curl_exec($curl);
        if ($response === false) {
            dd(curl_error($curl));
        }
        $response = json_decode($response, true);
        // dd($response);
        curl_close($curl);
        return $response['access_token'];
    }

    public function Prepare(Request $request)
    {
        $request['api_token'] = $this->GetAccessToken();
        $Encrypt = $this->Encrypt->encryptRequest($request);
        return $Encrypt;
    }

    public function PostingMisc(Request $request)
    {
        // yang perlu di isi dari detail 
        $AccountCredit = $request['item']["AccountCredit"];
        $AccountDebit = $request['item']["AccountDebit"];
        $CurrencyDebit = $request['item']["CurrencyDebit"];
        $RateCredit = $request['item']["RateCredit"];
        $RateDebit = $request['item']["RateDebit"];
        $Remark3 = $request['item']["Remark3"];
        $AmountCredit = $request['item']["AmountCredit"];
        $AmountDebit = $request['item']["AmountDebit"];
        $CurrencyCredit = $request['item']["CurrencyCredit"];
        $branch = $request['item']["branch"];
        unset($request['item']);

        $body =
            [
                "AccountCredit" => "$AccountCredit",
                "AccountDebit" => "$AccountDebit",
                "MerchantType" => "6021",
                "DE48" => [
                    "AmountCharges1" => "",
                    "AmountCharges2" => "000",
                    "CurrencyDebit" => "$CurrencyDebit",
                    "RateCredit" => "$RateCredit",
                    "AmountCharges3" => "000",
                    "AccountCharges1" => "",
                    "DealEntryTicket" => "",
                    "UserOverride" => "",
                    "CancelTransaction" => "",
                    "PassbookBalance" => "",
                    "Remark2" => "",
                    "Remark3" => "$Remark3",
                    "Remark1" => "",
                    "Profit" => "",
                    "RateDebit" => "$RateDebit",
                    "SupervisorOverride" => "",
                    "TellerOverride" => "",
                    "Loss" => "",
                    "AmountCredit" => "$AmountCredit",
                    "CheckNumber" => "",
                    "AmountDebit" => "$AmountDebit",
                    "JurnalSequence" => "",
                    "AccountCharges3" => "",
                    "AccountCharges2" => "",
                    "CurrencyCredit" => "$CurrencyCredit",
                    "RateCharges2" => "",
                    "CurrencyCharges3" => "",
                    "TransactionBranch" => "$branch",
                    "CurrencyCharges2" => "",
                    "CurrencyCharges1" => "",
                    "TellerID" => "0000",
                    "RateCharges3" => "",
                    "ProductId" => "",
                    "KodeAggregator" => "ALTERRA",
                    "AmountCharges4" => "",
                    "AccountCharges4" => "",
                    "CurrencyCharges4" => "",
                    "RateCharges4" => "",
                    "AmountCharges5" => "",
                    "AccountCharges5" => "",
                    "CurrencyCharges5" => "",
                    "RateCharges5" => "",
                    "Denda" => "",
                    "BiayaAsuransi" => "",
                    "NIK" => "",
                    "NoPolis" => "",
                    "NoRangka" => "",
                    "BiayaPengiriman" => "",
                    "TransactionId" => "",
                    "TotalTagihanPajakMurni" => ""
                ],
                "AcceptorTerminalID" => "MSML"
            ];
        $request['method'] = 'POST';
        $request['path'] = '/openapi/v1.0/misc/debit-credit';
        $request['body'] = $body;
        $prepare = $this->Prepare($request);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://openapidev1.bankmega.local:15000' . $request['path'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $prepare['encrypted_body'],
            CURLOPT_HTTPHEADER => [
                'X-SIGNATURE: ' . $prepare['signature'],
                'X-TIMESTAMP: ' . $prepare['timestamp'],
                'X-PARTNER-ID: ' . ENV('PARTNER_ID_DHL_DEV'),
                'X-EXTERNAL-ID: ' . ENV('EXTERNAL_ID_DHL_DEV'),
                'CHANNEL-ID: ' . ENV('CHANNEL_ID_DHL_DEV'),
                'Host: ' . ENV('HOST_DHL_DEV'),
                'X-CREDENTIAL-KEY: ' . $prepare['credential'],
                'Content-Type: application/json',
                'Authorization: Bearer ' . $prepare['access_token']
            ],
        ));

        $response = curl_exec($curl);
        $request['data'] = $response;
        $request['random_aes_key'] = $prepare['random_aes_key'];
        $request['random_aes_iv'] = $prepare['random_aes_iv'];
        $response = $this->Encrypt->decryptResponse($request);

        curl_close($curl);

        return json_decode($response['decrypted_response']);
    }
}
