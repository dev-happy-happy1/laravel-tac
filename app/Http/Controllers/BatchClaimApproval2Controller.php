<?php

namespace App\Http\Controllers;

use App\Models\MasterGl;
use App\Models\TransactionClaim;
use App\Models\TransactionClaimFee;
use App\Models\TrxBatch;
use App\Models\TrxBatchDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Http;

class BatchClaimApproval2Controller extends Controller
{
    private $page_title         = "Batch Claim";
    private $route              = "batch-claim-approval2";
    private $permission         = "batch-claim-approval2";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        return view('batch-claim-approval2.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'permission'    => $this->permission
        ]);
    }

    public function show($id)
    {
        // Ambil data batch_claim beserta detailnya
        $batch_claim = TrxBatch::with(['batch_detail.claim', 'batch_detail.documents'])->findOrFail($id);

        // Ambil data master_gl dan buat associative array untuk mapping
        $master_gl = MasterGl::all()->keyBy('gl_currency');

        // Ambil user yang membuat batch_claim
        $user = User::where('id', $batch_claim->created_by)->first();

        return view('batch-claim-approval2.show', [
            'pageConfigs' => $this->pageConfigs,
            'page_title' => $this->page_title,
            'route' => $this->route,
            'batch_claim' => $batch_claim,
            'user' => $user,
            'master_gl' => $master_gl
        ]);
    }


    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required'
        ]);

        $batch_claim = TrxBatch::find($request->id);

        if ($batch_claim) {
            // Update the status of the TrxBatch
            $batch_claim->status = $request->status;
            $batch_claim->note = $request->note;
            $batch_claim->save();

            // Loop through each detail and update the associated TransactionClaim
            foreach ($batch_claim->batch_detail as $detail) {
                $transactionClaim = $detail->claim;

                if ($transactionClaim) {
                    $transactionClaim->status = $request->status;
                    $transactionClaim->note = $request->note;
                    $transactionClaim->save();
                }
            }

            return response()->json(['message' => 'Claim and associated transaction claims updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Claim not found'], 404);
        }
    }

    public function getKurs($currency)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://10.14.20.15/apikurs/getkurs.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if ($response === false) {
            return null; // Handle curl execution error
        }

        $response = json_decode($response);

        // Construct the dynamic property name
        $property = $currency . '_DEVISA_BANKJUAL';

        // Access the property dynamically
        if (isset($response->$property)) {
            return $response->$property; // Return the rate value
        } else {
            return null; // Handle case where property does not exist
        }
    }

    public function postingGl(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        // Ambil semua batch_claim_detail berdasarkan trx_batch_id
        $batch_claim_details = TrxBatchDetail::where('trx_batch_id', $request->id)->get();

        // Ambil transaction_claim_fee_ids dari batch_claim_details
        $transaction_claim_fee_ids = $batch_claim_details->pluck('transaction_claim_fee_id')->toArray();

        // Ambil semua transaction_claim_fees berdasarkan transaction_claim_fee_id
        $transaction_claim_fees = TransactionClaimFee::whereIn('id', $transaction_claim_fee_ids)->get();

        // Ambil semua data dari master_gls
        $master_gls = MasterGl::get();

        // Ambil transaction_claim_ids dari batch_claim_details
        $transaction_claim_ids = $batch_claim_details->pluck('transaction_claim_id')->toArray();

        // Ambil semua branches dari tabel transaction_claims berdasarkan transaction_claim_ids
        $transaction_claims = TransactionClaim::whereIn('id', $transaction_claim_ids)->get(['id', 'branch', 'code'])->keyBy('id')->toArray();

        // Buat array $data dan isi dengan informasi yang diperlukan
        $data = [];
        foreach ($batch_claim_details as $detail) {
            $transaction_claim_id = $detail->transaction_claim_id;

            $transaction_claim_fee = $transaction_claim_fees->firstWhere('id', $detail->transaction_claim_fee_id);

            // Ambil gl_no dari master_gls yang sesuai dengan mata uang dari transaction_claim_fee
            $account_credit = $master_gls->where('gl_currency', $transaction_claim_fee->currency_fee)->pluck('gl_ref')->first();

            // Ambil RateCredit dari API menggunakan fungsi getKurs dari ApiController
            $rate_credit = $this->getKurs($transaction_claim_fee->currency_fee); // Memanggil fungsi getKurs dari ApiController

            // Isi data array dengan informasi yang sesuai
            $data[] = [
                'AccountCredit' => $account_credit,
                'AccountDebit' => $transaction_claim_fee->account,
                'CurrencyDebit' => $transaction_claim_fee->currency_fee,
                'RateCredit' => (string) ($rate_credit * 100),
                'RateDebit' => (string) ($rate_credit * 100),
                'Remark3' => $transaction_claims[$transaction_claim_id]['code'] ?? "",
                'AmountCredit' => (string) ($transaction_claim_fee->amount_fee * 100),
                'AmountDebit' => (string) ($transaction_claim_fee->amount_fee * 100),
                'CurrencyCredit' => $transaction_claim_fee->currency_fee,
                'branch' => $transaction_claims[$transaction_claim_id]['branch'] ?? "",
                'transaction_claim_fee_id' => $detail->transaction_claim_fee_id
            ];
        }

        $count_success  = 0;
        $count_error    = 0;
        $api_management = new ApiManagementController();
        foreach ($data as $item) {
            $request['item'] = $item;
            $response = $api_management->PostingMisc($request); // Kirim setiap item secara terpisah ke PostingMisc
            $response_raw = json_encode($response);

            // Ambil rc, rd dari respons PostingMisc
            $rc = @$response->ResponseCode;
            $rd = @$response->ResponseDescription;

            // Simpan rc, rd, response_raw ke transaction_claim_fees berdasarkan transaction_claim_fee_id
            $transaction_claim_fee_id = $item['transaction_claim_fee_id'];
            $transaction_claim_fee = TransactionClaimFee::find($transaction_claim_fee_id);
            if ($transaction_claim_fee) {
                $transaction_claim_fee->rc = $rc;
                $transaction_claim_fee->rd = $rd;
                $transaction_claim_fee->response_raw = $response_raw;
                $transaction_claim_fee->update();
            }

            if ($rc == '00') {
                $count_success++;
            } else {
                $count_error++;
            }

            // Debugging atau log proses update
            // dd($rc, $rd, $response_raw, $transaction_claim_fee_id);
        }

        return response()->json(['message' => 'Posting GL success', 'success' => $count_success, 'error' => $count_error], 200);
    }











    public function datatable(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $record  = TrxBatch::with(['users'])->orderBy('created_at', 'DESC')->whereIn('status', ['3.1']);
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'batch-claim-approval2', 'batch-claim-approval2');
                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }
}
