<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Treenodes;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    private $page_title         = "News Management";
    private $route              = "news";
    private $permission         = "news";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('news.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'permission'    => $this->permission,
            'route'         => $this->route,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'permission'    => $this->permission,
            'route'         => $this->route,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'      => 'required|string',
            'image'      => 'required|file|mimes:jpeg,jpg,png|max:1024',
            'attach'      => 'file|mimes:pdf|max:2048',
            'content'      => 'required',
        ]);
        $model = new News();
        // dd($request->All());
        $recordData = $request->all();
        
        if ($request->file('image')) {
            $file = $request->file('image');
            $ext  = $file->getClientOriginalExtension();
            // Check if the uploaded file is an image and of allowed formats
            if (!in_array($ext, ['jpeg', 'jpg', 'png'])) {
                return redirect()->back()->with(toaster('Oops, file upload tidak sesuai', 'error', 'error'));
            }
            // $filename = date('YmdHi') . '.' . $file->getClientOriginalName();
            // $filename = Str::slug($filename, '-') . '.' . $ext;
            $filename = 'news-' . TRIM(Auth()->user()->company) . TRIM(Auth()->user()->username) . '-' . date('YmdHis') . '.' . $ext; // You can set the desired file extension

            // $file->move(public_path('public/image'), $filename);
            // Resize the image and ensure it's within 150KB
            $img = Image::make($file)->encode('jpeg', 75); // Encode as JPEG with 75% quality
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->encode($ext, 75); // Encode the image as JPEG with 75% quality

            // Use Storage facade to store the image in the specified path
            $stg = Storage::put('public/image/' . $filename, $img->__toString());
            // // Use the 'public' disk to store the image
            // $img->storeAs('public/image', $filename);

            $recordData['image'] = $filename;

            if (!in_array($ext, ['jpeg', 'jpg', 'png'])) {
                return redirect()->back()
                    ->with(toaster('Oops, file upload tidak sesuai', 'error', 'error'));
            }
        }
        if ($request->file('attach')) {
            $file = $request->file('attach');
            $ext  = $file->getClientOriginalExtension();
            $filename = date('YmdHi') . '.' . $file->getClientOriginalName();
            $filename = Str::slug($filename, '-') . '.' . $ext;
            // $file->move(public_path('public/attach'), $filename);

            // Use the 'public' disk to store the attachment
            $file->storeAs('public/attach', $filename);

            $recordData['attach'] = $filename;

            if (!in_array($ext, ['pdf'])) {
                return redirect()->back()
                    ->with(toaster('Oops, file upload tidak sesuai', 'error', 'error'));
            }        
        }
        $recordData['slug']     = createSlug($recordData['title'], $model);
        $recordData['author']   = Auth()->user()->id;
        $recordData['published_at']   = Carbon::now();
        $recordData['ref']   = Auth()->user()->company;
        $recordData['content']   = str_replace('<ul>', '<ul style="padding-left: 18px;">', $recordData['content']);
        $recordData['content']   = str_replace('<ol>', '<ol style="padding-left: 18px;">', $recordData['content']);
        $recordData['title']    = strip_tags($recordData['title']);
        // dd($recordData);
        $save = News::create($recordData);

        return redirect()->route($this->route . '.index')
            ->with(toaster('News created successfully', 'success', 'success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record   = News::find($id);

        return view('news.edit', [
            // 'breadcrumbs' => $breadcrumbs
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'record'          => $record
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'      => 'required|string',
            'image'      => 'file|mimes:jpeg,jpg,png|max:1024',
            'attach'      => 'file|mimes:pdf|max:2048',
            'content'      => 'required',
        ]);
        try {
            $user   = News::find($id);
            $recordData = $request->all();
            if ($request->file('image')) {
                $file = $request->file('image');
                $ext  = $file->getClientOriginalExtension();
                // $filename = date('YmdHi') . '.' . $file->getClientOriginalName();
                // $filename = Str::slug($filename, '-') . '.' . $ext;
                $filename = 'news-' . TRIM(Auth()->user()->company) . TRIM(Auth()->user()->username) . '-' . date('YmdHis') . '.' . $ext; // You can set the desired file extension

                // Resize the image and ensure it's within 150KB
                $img = Image::make($file)->encode('jpeg', 75); // Encode as JPEG with 75% quality
                $img->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                // $file->move(public_path('public/image'), $filename);

                $img->encode($ext, 75); // Encode the image as JPEG with 75% quality

                // Use Storage facade to store the image in the specified path
                $stg = Storage::put('public/image/' . $filename, $img->__toString());
                // Use the 'public' disk to store the image
                // $img->storeAs('public/image', $filename);
                
                $recordData['image'] = $filename;
                // delete file 
                if (Storage::disk('public')->exists('image/' . $user->image)) {
                    // Check if the file exists
                    Storage::disk('public')->delete('image/' . $user->image); // Delete the file
                }

                if (!in_array($ext, ['jpeg', 'jpg', 'png'])) {
                    return redirect()->back()
                        ->with(toaster('Oops, file upload tidak sesuai', 'error', 'error'));
                }
            }
            if ($request->file('attach')) {
                $file = $request->file('attach');
                $ext  = $file->getClientOriginalExtension();
                $filename = date('YmdHi') . '.' . $file->getClientOriginalName();
                $filename = Str::slug($filename, '-') . '.' . $ext;
                // $file->move(public_path('public/attach'), $filename);

                $file->storeAs('public/attach', $filename);

                $recordData['attach'] = $filename;

                if (!in_array($ext, ['pdf'])) {
                    return redirect()->back()
                        ->with(toaster('Oops, file upload tidak sesuai', 'error', 'error'));
                }
            }
            $recordData['content']   = str_replace('<ul>', '<ul style="padding-left: 18px;">', $recordData['content']);
            $recordData['content']   = str_replace('<ol>', '<ol style="padding-left: 18px;">', $recordData['content']);
            $recordData['title']    = strip_tags($recordData['title']);

            $user->update($recordData);
            //code...
        } catch (\Throwable $th) {
            return redirect()->back()
                ->with(toaster('Oops!, Please check your input data.', 'error', 'error'));
        }

        return redirect()->route($this->route . '.index')
        ->with(toaster('News updated successfully', 'success', 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record   = News::find($id);
        $author   = User::find($record->author);
        $creator  = Treenodes::where('code', $record->ref)->first();

        return view('news.show', [
            // 'breadcrumbs' => $breadcrumbs
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'record'        => $record,
            'author'        => $author,
            'creator'       => $creator
        ]);
    }

    public function destroy($id)
    {
        $delete = News::findOrFail($id);
        $delete->delete() == true
            ? $return = ['code' => 'success', 'msg' => 'data deleted successfully']
            : $return = ['code' => 'error', 'msg' => 'something went wrong!'];

        return response()->json($return);
    }

    public function downloadFile($filename)
    {
        $filePath = public_path('public/attach/' . $filename);
	dd($filePath);
        // return Response::don($filePath, ['Content-Type' => 'application/pdf']);
        return Response::download($filePath);
    }

    public function DataTable(Request $request)
    {
        if ($request->ajax()) {
            $model = News::with('company');
            if (Auth()->user()->company != '0001') {
                $model = $model->where('ref', Auth()->user()->company);
            }
            $model = $model->orderBy('createdAt', 'DESC');

            return DataTables::of($model)
                ->addIndexColumn()
                ->addColumn('image', function ($data) {
                $filePath = 'public/image/' . $data->image;
                $url = Storage::url($filePath);

                    return $url;
                })
                ->addColumn('company', function ($data) {
                    return @$data->company->label != null ?  $data->company->label : '-';
                })
                ->addColumn('content', function ($data) {
                return  Str::limit($data->content, 100, '...');
                })
                ->addColumn('action', function ($data) {
                $button = ButtonSED($data, $this->route, $this->permission);
                return $button;
                })
                ->rawColumns(['ref', 'action', 'status', 'image', 'content'])
                ->make(true);
        }
    }
}
