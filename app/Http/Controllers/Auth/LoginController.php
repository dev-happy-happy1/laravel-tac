<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use ThrottlesLogins;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $maxAttempts = 3; // Change to your desired number of attempts
    protected $decayMinutes = 15; // Change to your desired lockout time in minutes

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([$this->username() => trans('auth.throttle', ['seconds' => $seconds])]);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request); // This method is provided by the AuthenticatesUsers trait

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        // cek otp 
        try {
            $record = new User();

            // if (filter_var(
            //     $request['email'],
            //     FILTER_VALIDATE_EMAIL
            // )) {
            //     $record = $record->where('email', $request['email'])->where('status', 1);
            // } else {
            $record = $record->where(
                'username',
                $request['email']
            )->where('status', 1);
            // }
            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }
        } catch (\Throwable $th) {
            return redirect()->back()
                ->with(toaster(
                    'Oops, something went wrong!',
                    'error',
                    'error'
                ));
        }
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function validateUser(Request $request)
    {
        try {
            $pageConfigs = [
                'bodyClass' => "bg-full-screen-image",
                'blankPage' => true
            ];

            $user = $request->email;
            $pass = $request->password;

            $record = new User();

            // if (filter_var(
            //     $user,
            //     FILTER_VALIDATE_EMAIL
            // )) {
            //     $record = $record->where(
            //         'email',
            //         $request['email']
            //     )->where('status', 1);
            // } else {
                $record = $record->where(
                        'username',
                        $request['email']
                    )->where('status', 1);
            // }

            $record = $record->first();
            $validCredentials = Hash::check($request['password'], $record->getAuthPassword());

            if ($validCredentials) {
                $lastOTPGeneratedAt = $record->otp_generated_at;

                // Calculate time difference in minutes
                $minutesDiff = now()->diffInMinutes($lastOTPGeneratedAt ?? now()->subMinutes(5));

                // If OTP was not generated for the same email in the last 5 minutes, generate a new OTP
                if ($minutesDiff >= 5 || $record->last_otp_email != $record->email) {
                    $otp = $this->generateOTP();
                    $api = new ApiController();

                    $api->sendOTP(
                        $record->name . ' [' . $record->username . ']',
                        $record->email,
                        $otp
                    );

                    // otp save 
                    $record->otp = $otp;
                    $record->otp_generated_at = now(); // Save the timestamp of OTP generation
                    $record->last_otp_email = $record->email;
                    $record->update();

                    $email = $record->email;
                    $maskedEmail = $this->maskEmail($email);

                    $request->session()->flash('toaster', toaster(
                        'OTP sudah dikirimkan ke email: ' . $maskedEmail . '. OTP yang sama berlaku 5 menit',
                        'info',
                        'info'
                    ));

                    return view('auth.otp', [
                        'pageConfigs' => $pageConfigs,
                        'username' => $request['email'],
                        'password' => $request['password']
                    ]);
                } else {
                    $request->session()->flash('toaster', toaster(
                        'OTP telah dikirimkan, silakan cek email Anda. OTP yang sama berlaku 5 menit',
                        'info',
                        'info'
                    ));

                    return view('auth.otp', [
                        'pageConfigs' => $pageConfigs,
                        'username' => $request['email'],
                        'password' => $request['password']
                    ]);
                }
            } else {
                return redirect()->back()
                ->with(toaster('Oops, something went wrong!, check your password and username', 'error', 'error'));
            }
        } catch (\Throwable $th) {
            return redirect()->back()
                ->with(toaster(
                    'Oops, something went wrong! : ' . $th->getMessage(),
                    'error',
                    'error'
                ));
        }
    }


    function maskEmail($email)
    {
        $parts = explode('@', $email);
        $username = $parts[0];
        $domain = $parts[1];
        $maskedUsername = substr(
            $username,
            0,
            3
        ) . str_repeat(
            '*',
            strlen($username) - 3
        );
        return $maskedUsername . '@' . $domain;
    }

    protected function attemptLogin(Request $request)
    {
        $user = $request->email;
        $pass = $request->password;

        // if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
        //     return auth()->attempt(['email' => $user, 'password' => $pass, 'status' => 1]);
        // } else {
            return auth()->attempt(['username' => $user, 'password' => $pass, 'status' => 1]);
        // }

        return false;
    }

    function generateOTP()
    {
        // Generate a random 6-digit OTP
        $otp = rand(100000, 999999);
        return $otp;
    }

    // Login
    public function showLoginForm()
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/auth/login', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}
