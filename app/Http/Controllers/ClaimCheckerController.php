<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\MasterGl;
use App\Models\TransactionClaim;
use App\Models\TransactionClaimFee;
use App\Models\TransactionClaimProfit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ClaimCheckerController extends Controller
{
    private $page_title         = "Claim";
    private $route              = "claim-checker";
    private $permission         = "claim-checker";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        return view('claim-checker.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'permission'    => $this->permission
        ]);
    }

    public function show($id)
    {
        $claim = TransactionClaim::with(['documents', 'branches', 'fees', 'profits'])->findOrFail($id);

        $branch = MasterBranch::where('code', $claim->branch)->first();

        $user = User::where('id', $claim->created_by)->first();

        return view('claim-checker.show', [
            'pageConfigs' => $this->pageConfigs,
            'page_title' => $this->page_title,
            'route' => $this->route,
            'claim' => $claim,
            'branch' => $branch,
            'user' => $user
        ]);
    }


    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required',
        ]);
        $claim = TransactionClaim::find($request->id);
        if ($claim) {
            $claim->status = $request->status;
            $claim->note = $request->note;
            $claim->save();
            return response()->json(['message' => 'Claim updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Claim not found'], 404);
        }
    }


    public function datatableFeeTransfer(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $branch = auth()->user()->branch;

            $record = TransactionClaim::with(['branches', 'fees', 'users'])
                ->orderBy('created_at', 'DESC')
                ->where('type', 'Fee Transfer')
                ->where('status', '0');

            if ($branch !== null) {
                $record->where('branch', $branch);
            }

            $record = $record->get();


            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('type', function ($data) {
                    $render = $data->type;
                    $render = explode('.', $render);
                    return $render[0];
                })
                ->addColumn('fee', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->type_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('currency', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->currency_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('account', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->account . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('amount', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->amount_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'claim-checker', 'claim-checker');
                    return $button;
                })
                ->rawColumns(['action', 'fee', 'currency', 'account', 'amount', 'status'])
                ->make(true);
        }
    }

    public function datatableFeeEvent(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $branch = auth()->user()->branch;

            $record = TransactionClaim::with(['branches', 'fees', 'users'])
                ->orderBy('created_at', 'DESC')
                ->where('type', 'Fee Event')
                ->where('status', '0');

            if ($branch !== null) {
                $record->where('branch', $branch);
            }

            $record = $record->get();
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('type', function ($data) {
                    $render = $data->type;
                    $render = explode('.', $render);
                    return $render[0];
                })
                ->addColumn('fee', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->type_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('currency', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->currency_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('account', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->account . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('amount', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->amount_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'claim-checker', 'claim-checker');
                    return $button;
                })
                ->rawColumns(['action', 'fee', 'currency', 'account', 'amount', 'status'])
                ->make(true);
        }
    }
}
