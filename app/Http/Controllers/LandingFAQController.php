<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingFAQController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('landing.faq.index', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}
