<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class UserController extends Controller
{
    private $page_title         = "User Management";
    private $route              = "users";
    private $permission         = "user";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = User::with(['branches'])->get();
            return DataTables::of($model)
                ->addIndexColumn()
                ->addColumn('branch', function ($row) {
                    return $row->branches ? $row->branches->code . ' - ' . $row->branches->name : '';
                })
                ->addColumn('roles', function ($data) {
                    $label = '';
                    if (!empty($data->getRoleNames())) {
                        foreach ($data->getRoleNames() as $v) {
                            $label .=  " <label class='badge badge-success'> " . $v . " </label> ";
                        }
                    }
                    return $label;
                })
                ->addColumn('status', function ($data) {
                    if ($data->status == 1) {
                        $label =  " <label class='badge badge-success'> Active </label> ";
                    } else {
                        $label =  " <label class='badge badge-warning'> Non-Active </label> ";
                    }
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'users', 'user');
                    if ($data->status == 0) {
                        $button .= ' <button class="btn btn-icon btn-sm btn-activated btn-success" data-remote="' . route($this->route . '.updateStatus', $data->id) .
                            '" data-toggle="tooltip" title="Activate User">
                            ' . SVGI('bi-check') . '
                            </button>';
                    }
                    return $button;
                })
                ->rawColumns(['roles', 'action', 'status'])
                ->make(true);
        }

        return view('auth-app.users.index', [
            // 'breadcrumbs' => $breadcrumbs
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'permission'    => $this->permission,
            'route'         => $this->route,
        ]);
    }

    public function create()
    {
        $roles = Role::where('name', '!=', 'super-admin')->pluck('name', 'name');
        $branches = MasterBranch::pluck('name', 'code');

        return view('auth-app.users.create', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'roles'         => $roles,
            'branches'      => $branches
        ]);
    }



    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required|email',
            // 'password'  => 'required|same:confirm-password|min:8',
            'password' => [
                'required',
                'same:confirm-password',
                'string',
                'min:8',             // Minimum length of 8 characters
                'regex:/[A-Z]/',      // At least one uppercase letter
                'regex:/[a-z]/',      // At least one lowercase letter
                'regex:/[0-9]/',      // At least one number
                'regex:/[@$!%*#?&]/', // At least one special character
            ],
            'roles'     => 'required',
            'branch'   => 'required',
            'unit'    => 'required',
        ]);

        // dd($request->All());

        $request['password'] = Hash::make($request['password']);
        // $username           = Str::slug($request['name'], '');
        // $request['username']  = $username;
        // $request['status']  = '0';
        if (isset($request['status'])) {
            $request['status'] = true;
        } else {
            $request['status'] = false;
        }


        $user               = User::create($request->All());
        $user->assignRole($request->input('roles'));

        return redirect()->route($this->route . '.index')
            ->with(toaster('User created successfully', 'success', 'success'));
    }

    public function edit($id)
    {
        $user       = User::find($id);
        $roles      = Role::where('name', '!=', 'super-admin')->pluck('name', 'name')->all();
        $userRole   = $user->roles->pluck('name', 'name')->all();
        $branches = MasterBranch::pluck('name', 'code');
        $userBranch = $user->branch;

        // $companies = Treenodes::where('level', 3)->pluck('label', 'code');

        return view('auth-app.users.edit', [
            // 'breadcrumbs' => $breadcrumbs
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'roles'         => $roles,
            'userRole'      => $userRole,
            'user'          => $user,
            'branches'      => $branches,
            'userBranch'    => $userBranch,
        ]);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'roles' => 'required',
            'branch' => '',
        ];

        // Check if a new password is provided (the 'password' field is not empty)
        if (!empty($request->input('password'))) {
            $rules['password'] = [
                'string',
                'min:8',             // Minimum length of 8 characters
                'regex:/[A-Z]/',      // At least one uppercase letter
                'regex:/[a-z]/',      // At least one lowercase letter
                'regex:/[0-9]/',      // At least one number
                'regex:/[@$!%*#?&]/', // At least one special character
            ];
            $rules['confirm-password'] = 'required|string|same:password';
        }

        $this->validate($request, $rules);

        // $this->validate($request, [
        //     'name'      => 'required',
        //     'email'     => 'required|email,' . $id,
        //     // 'password'  => 'same:confirm-password|min:8',
        //     'password' => [
        //         'same:confirm-password',
        //         'string',
        //         'min:8',             // Minimum length of 8 characters
        //         'regex:/[A-Z]/',      // At least one uppercase letter
        //         'regex:/[a-z]/',      // At least one lowercase letter
        //         'regex:/[0-9]/',      // At least one number
        //         'regex:/[@$!%*#?&]/', // At least one special character
        //     ],
        //     'roles'     => 'required'
        // ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            unset($input['password']);
        }
        // dd($request->All());

        if (isset($input['status'])) {
            $input['status'] = true;
        } else {
            $input['status'] = false;
        }
        unset($input['username']);

        $roles = array_filter($request->input('roles'), function ($role) {
            return $role !== "super-admin";
        });

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($roles);

        return redirect()->route('users.index')
            ->with(toaster('User updated successfully', 'success', 'Success'));
    }

    public function show($id)
    {
        $user   = User::find($id);

        return view('auth-app.users.show', [
            // 'breadcrumbs' => $breadcrumbs
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'user'          => $user
        ]);
    }

    public function destroy($id)
    {
        $delete = User::findOrFail($id);
        $delete->delete() == true
            ? $return = ['code' => 'success', 'msg' => 'data deleted successfully']
            : $return = ['code' => 'error', 'msg' => 'something went wrong!'];

        return response()->json($return);
    }

    public function updateStatus($id)
    {
        $user   = User::find($id);
        $user->update(['status' => 1]) == true
            ? $return = ['code' => 'success', 'msg' => 'User updated successfully']
            : $return = ['code' => 'error', 'msg' => 'something went wrong!'];

        return response()->json($return);
    }

    public function ChangePassword($id)
    {
        $user       = User::find($id);

        return view('auth-app.users.change-password', [
            // 'breadcrumbs' => $breadcrumbs
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'user' => $user
        ]);
    }

    public function updatePassword(Request $request, $id)
    {
        $rules = [];
        // Check if a new password is provided (the 'password' field is not empty)
        if (!empty($request->input('password'))) {
            $rules['password'] = [
                'string',
                'min:8',             // Minimum length of 8 characters
                'regex:/[A-Z]/',      // At least one uppercase letter
                'regex:/[a-z]/',      // At least one lowercase letter
                'regex:/[0-9]/',      // At least one number
                'regex:/[@$!%*#?&]/', // At least one special character
            ];
            $rules['confirm-password'] = 'required|string|same:password';
        }

        $this->validate($request, $rules);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
            $msg = 'Change password successfully';
        } else {
            unset($input['password']);
            $msg = 'No Update password';
        }

        unset($input['name']);
        unset($input['username']);
        unset($input['email']);

        $user = User::find($id);
        $user->update($input);

        return redirect()->back()
            ->with(toaster($msg, 'success', 'Success'));
    }
}
