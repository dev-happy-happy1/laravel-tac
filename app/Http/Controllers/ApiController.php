<?php

namespace App\Http\Controllers;

use App\Models\Employees;
use App\Models\Province;
use App\Models\TrxJobs;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;


class ApiController extends Controller
{
    public function getKurs($currency)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://10.14.20.15/apikurs/getkurs.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        // Construct the dynamic property name
        $property = $currency . '_DEVISA_BANKJUAL';

        // Access the property dynamically
        if (isset($response->$property)) {
            $value = $response->$property;
        } else {
            echo "Property {$property} does not exist.";
        }
        curl_close($curl);
        return $value;
    }

    // public function dataCif(Request $req)
    // {
    //     $cif = DB::connection('dwh2')->select("
    //         SELECT
    //             CURRENT_CFMAST.CFCIF# CIF,
    //             CURRENT_CFMAST.CFSNME,
    //             CURRENT_CFMAST.CFYBIP,
    //             CURRENT_CFMAST.CFNA1,
    //             CURRENT_CFAIDN.CFSSNO CFSSNO
    //         FROM
    //             CURRENT_CFMAST
    //         LEFT JOIN
    //             CURRENT_CFAIDN ON CURRENT_CFAIDN.CFCIF# = CURRENT_CFMAST.CFCIF# AND CURRENT_CFAIDN.CFSSCD = 'NP'
    //         WHERE
    //             CURRENT_CFMAST.CFCIF# LIKE '%$req->cif%'
    //         ORDER BY
    //             CURRENT_CFMAST.CFCIF# ASC
    //     ");

    //     // $cif = DB::table('master_cifmast')
    //     //     ->where('CIFNO', 'LIKE', '%' . $req->name .'%')
    //     //     ->orWhere('CFSNME', 'LIKE', '%' . $req->name .'%')
    //     //     ->orderBy('CIFNO', 'ASC')
    //     //     ->get();
    //     // dd($cif);
    //     $data = [];
    //     foreach ($cif as $key => $val) {
    //         $data[$key]['code'] = $val->CIF;
    //         $data[$key]['name'] = $val->CFNA1;
    //         // $data[$key]['npwp'] = '000000000000000';
    //         $data[$key]['npwp'] = $val->CFSSNO;

    //         // $data[$key]['address'] = $val->CFNA2 . ' '. $val->CFNA3 . ' ' . $val->CFNA4 . ' ' . $val->CFNA5;
    //         $data[$key]['address']  = $val->CFYBIP;
    //         $data[$key]['address']  = substr($data[$key]['address'], 0, 30);
    //         // $data[$key]['telp']     = $val->CFTLPN;
    //         // $data[$key]['api']      = $val->CFAPI;
    //         // $data[$key]['npwp']     = $val->CFNPWP;
    //         // $data[$key]['memo']     = $val->MEMO;
    //     }

    //     return response()->json($data[0]);
    // }

    public function dataCif(Request $req)
    {
        $cif = DB::connection('dwh2')->select("
            SELECT
                CURRENT_DDMAST.CIFNO,
                CURRENT_DDMAST.SNAME
            FROM
                CURRENT_DDMAST
            WHERE
             CURRENT_DDMAST.CIFNO = '$req->cif'
            ORDER BY
                CURRENT_DDMAST.CIFNO ASC
        ");
        $data = [];
        foreach ($cif as $key => $val) {
            $data[$key]['code'] = $val->CIFNO;
            $data[$key]['name'] = $val->SNAME;
        }

        return response()->json($data[0]);
    }

    public function dataAccount(Request $req)
    {
        $id = $req->code;
        $cif = DB::connection('dwh2')->select("SELECT * FROM CURRENT_DDMAST WHERE CIFNO = ? AND STATUS_DESC IN ('ACTIVE', 'DORMANT')", [$id]);

        $data = [];
        foreach ($cif as $key => $val) {
            $data[$key]['code'] = $val->ACCTNO;
            $data[$key]['curr'] = str_replace(' ', '', $val->DDCTYP);
            $data[$key]['status'] = $val->STATUS_DESC;
        }

        return response()->json($data);
    }

    public function dataAccountCabang(Request $req)
    {
        $id     = $req->code;
        $cif = DB::connection('dwh2')->select("SELECT * FROM CURRENT_RMESCRPF WHERE REBRN = '" . $id . "' AND status = 1");

        $data = [];
        foreach ($cif as $key => $val) {
            $data[$key]['code'] = $val->REACC1;
            $data[$key]['codeCab'] = $val->REBRN;
            $data[$key]['curr'] = str_replace(' ', '', $val->RECCY);
            $data[$key]['desc'] = $val->REDESC;
        }

        return response()->json($data);
    }

    public function PostingGltoGl($posting)
    {
        // // data posting
        // $TransactionBranch  = $posting['TransactionBranch'];
        // $AmountDebit        = $posting['AmountDebit'];
        // $CurrencyDebit      = $posting['CurrencyDebit'];
        // $RateDebit          = $posting['RateDebit'];
        // $AmountCredit       = $posting['AmountCredit'];
        // $CurrencyCredit     = $posting['CurrencyCredit'];
        // $RateCredit         = '100';
        // $AccDebit           = $posting['AccDebit'];
        // $AccCredit          = $posting['AccCredit'];
        // $Remark             = $posting['Remark'];
        // $KodeTransaksi      = "GLTOGL RECONARTAJASA";
        // $CardAcceptorTerminalID    = "TFEXPORT";

        $rateCredit = '100';
        $remark = $posting['remark'];
        $accCredit = $posting['c_reff_number'];
        $accDebit = $posting['d_reff_number'];
        $amount = $posting['amount'];
        $transactionBranch = $posting['d_branch'];
        $kodeTransaksi = "GLTOGL TFEXPORT";
        $cardAcceptorTerminalID = "TFEXPORT";
        // dd($posting, 'asd');

        $request = '{
        "Data" : {
            "AccCredit" : "' . $accCredit . '",
            "ReferenceNumber" : "940912",
            "AccDebit" : "' . $accDebit . '",
            "Time": "' . date('His') . '",
            "MerchType" : "6020",
            "DE48" : {
                "AmountCharges1" : "",
                "AmountCharges2" : "",
                "CurrencyDebit" : "IDR",
                "KodeTransaksi" : "' . $kodeTransaksi . '",
                "RateCredit" : "' . $rateCredit . '",
                "AmountCharges3" : "",
                "AccountCharges1" : "",
                "DealEntryTicket" : "",
                "UserOverride" : "",
                "CancelTransaction" : "",
                "PassbookBalance" : "",
                "Remark2" : "",
                "Remark3" : "",
                "Remark1" : "' . $remark . '",
                "Profit" : "",
                "RateDebit" : "100",
                "SupervisorOverride" : "",
                "TellerOverride" : "",
                "Loss" : "",
                "AmountCredit" : "' . $amount . '",
                "CheckNumber" : "",
                "AmountDebit" : "' . $amount . '",
                "JurnalSequence" : "",
                "AccountCharges3" : "",
                "AccountCharges2" : "",
                "CurrencyCredit" : "IDR",
                "RateCharges2" : "",
                "CurrencyCharges3" : "",
                "TransactionBranch" : "' . $transactionBranch . '",
                "CurrencyCharges2" : "",
                "CurrencyCharges1" : "",
                "TellerID" : "",
                "RateCharges3" : ""
            },
            "TransmissionDateTime": "' . date('mdHis') . '",
            "DatePlus": "' . date('md') . '",
            "ProcessingCode" : "490051",
            "Date": "' . date('md') . '",
            "CardAcceptorTerminalID" : "' . $cardAcceptorTerminalID . '",
            "TraceID" : "trx_trace_umc"
        }
        }';
        $log_request = $request;
        $request = encrypt_decrypt('E', $request);
        // dd($request);

        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => ENV('UMC_GL_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
            "channel_id": "' . ENV('UMC_GL_CHANEL_ID') . '",
            "service_id": "' . ENV('UMC_GL_SERVICE_ID') . '",
            "key_id": "' . ENV('UMC_GL_KEY_ID') . '",
            "data" : {
                "msg":
                "' . $request . '"
                }
            }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            )
        );

        $response = curl_exec($curl);

        $response = json_decode($response);

        curl_close($curl);
        return $response;
    }
}
