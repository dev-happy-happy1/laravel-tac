<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\MasterGl;
use App\Models\TransactionClaim;
use App\Models\TrxBatch;
use App\Models\TrxBatchDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class HistoryBatchClaimController extends Controller
{
    private $page_title         = "History Batch Claim";
    private $route              = "history-batch-claim";
    private $permission         = "history-batch-claim";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        return view('history-batch-claim.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'permission'    => $this->permission
        ]);
    }


    public function show($id)
    {
        $batch_claim = TrxBatch::with(['batch_detail.claim', 'batch_detail.documents'])->findOrFail($id);
        // Ambil data master_gl dan buat associative array untuk mapping
        $master_gl = MasterGl::all()->keyBy('gl_currency');


        $user = User::where('id', $batch_claim->created_by)->first();
        return view('history-batch-claim.show', [
            'pageConfigs' => $this->pageConfigs,
            'page_title' => $this->page_title,
            'route' => $this->route,
            'batch_claim' => $batch_claim,
            'user' => $user,
            'master_gl' => $master_gl
        ]);
    }


    public function datatable(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $record  = TrxBatch::with(['users'])->orderBy('created_at', 'DESC');
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'history-batch-claim', 'history-batch-claim');
                    return $button;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }
}
