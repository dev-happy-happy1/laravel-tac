<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class EncryptionController extends Controller
{
    public function encryptRequest(Request $request)
    {
        date_default_timezone_set('UTC');

        // Calculate X-TIMESTAMP
        $dt = new \DateTime();
        $timestamp = $this->toIsoString($dt);
        Log::channel('api_management')->info("Timestamp: $timestamp");
        // dump($timestamp);

        // Generate Random AES Key
        $salt = random_bytes(16);
        $randomAESKeyBytes = hash_pbkdf2('sha256', 'itec-cds-is-the-beast', $salt, 1000, 32, true);
        $randomAESKey = base64_encode($randomAESKeyBytes);
        Log::channel('api_management')->info("Random AES Key: $randomAESKey");
        // dump($randomAESKey);

        // Generate Random IV
        $ivBytes = random_bytes(16);
        $randomAESIv = base64_encode($ivBytes);
        Log::channel('api_management')->info("Random AES IV: $randomAESIv");
        // dump($randomAESIv);

        // Calculate X-CREDENTIAL-KEY Header
        $public_key_base64 = env('PUBLIC_KEY_DHL_DEV');
        $public_key = "-----BEGIN PUBLIC KEY-----\n" . wordwrap($public_key_base64, 64, "\n", true) . "\n-----END PUBLIC KEY-----";

        // Log the public key for debugging purposes
        Log::channel('api_management')->info("Public Key: $public_key");

        $publicKeyResource = openssl_pkey_get_public($public_key);

        if ($publicKeyResource === false) {
            $error = openssl_error_string();
            Log::channel('api_management')->error("Invalid public key: $error");
            return response()->json(['error' => 'Invalid public key'], 500);
        }

        $body = json_encode($request['body']);
        // dd($body);
        Log::channel('api_management')->info("Body: $body");
        // dump($body);

        $combined = $randomAESKey . ":" . $randomAESIv;
        openssl_public_encrypt($combined, $encrypted, $publicKeyResource);
        $xcred = base64_encode($encrypted);
        Log::channel('api_management')->info("X-Credential Key: $xcred");
        // dump($xcred);

        // Encrypt Request Body
        $encryptedBody = openssl_encrypt($body, 'AES-256-CBC', $randomAESKeyBytes, OPENSSL_RAW_DATA, $ivBytes);
        $encryptedBodyBase64 = base64_encode($encryptedBody);
        Log::channel('api_management')->info("Encrypted Body: $encryptedBodyBase64");
        // dump($encryptedBodyBase64);

        // Calculate X-SIGNATURE Header
        $method = $request['method'];
        $path = $request['path'];
        $query = $request->getQueryString();
        // dd($method, $path, $query);
        $queries = explode("&", $query);
        sort($queries);
        $query = implode("&", $queries);
        if (strlen($query) > 0) {
            $path = $path . '?' . $query;
        }
        $access_token = $request['api_token']; // Assuming you store access token in user table
        $mdHex = hash('sha256', $body);
        $stringToSign = $method . ":" . $path . ":" . $access_token . ":" . $mdHex . ":" . $timestamp;
        // dd($stringToSign);
        Log::channel('api_management')->info("String to Sign: $stringToSign");
        // dump($stringToSign);
        // die();

        $clientSecret = env('SECRET_KEY_DHL_DEV');
        $hash = hash_hmac('sha512', $stringToSign, $clientSecret, true);
        $signature = base64_encode($hash);
        Log::channel('api_management')->info("Signature: $signature");
        // dump($signature);

        return [
            'timestamp' => $timestamp,
            'random_aes_key' => $randomAESKey,
            'random_aes_iv' => $randomAESIv,
            'credential' => $xcred,
            'encrypted_body' => $encryptedBodyBase64,
            'signature' => $signature,
            'access_token' => $access_token
        ];
    }

    public function toIsoString($date)
    {
        $tzo = -$date->getOffset() / 60;
        $dif = $tzo >= 0 ? '+' : '-';
        $pad = function ($num) {
            return str_pad($num, 2, '0', STR_PAD_LEFT);
        };

        return $date->format('Y') .
            '-' . $pad($date->format('m')) .
            '-' . $pad($date->format('d')) .
            'T' . $pad($date->format('H')) .
            ':' . $pad($date->format('i')) .
            ':' . $pad($date->format('s')) .
            $dif . $pad(floor(abs($tzo) / 60)) .
            ':' . $pad(abs($tzo) % 60);
    }

    public function decryptResponse(Request $request)
    {
        // Retrieve the encrypted response body from the request
        $encryptedResponseBodyBase64 = $request['data'];
        Log::channel('api_management')->info("Encrypted Response: $encryptedResponseBodyBase64");

        // Retrieve the AES key and IV from the environment
        $randomAESKey = base64_decode($request['random_aes_key']);
        $randomAESIV = base64_decode($request['random_aes_iv']);

        // Decrypt the response body
        $decryptedResponseBody = openssl_decrypt(
            base64_decode($encryptedResponseBodyBase64),
            'AES-256-CBC',
            $randomAESKey,
            OPENSSL_RAW_DATA,
            $randomAESIV
        );
        Log::channel('api_management')->info("Decrypted Response: $decryptedResponseBody");

        // Optionally, you can return the decrypted response
        return [
            'decrypted_response' => $decryptedResponseBody
        ];
    }
}
