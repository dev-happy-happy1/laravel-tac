<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingContactController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('landing.contact.index', [
            'pageConfigs' => $pageConfigs
        ]);
    }
}
