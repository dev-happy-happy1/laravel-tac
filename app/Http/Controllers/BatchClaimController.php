<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\TransactionClaim;
use App\Models\TrxBatch;
use App\Models\TrxBatchDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Stringable;
use Yajra\DataTables\Facades\DataTables;

class BatchClaimController extends Controller
{
    private $page_title         = "Batch Claim";
    private $route              = "batch-claim";
    private $permission         = "batch-claim";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        return view('batch-claim.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'permission'    => $this->permission
        ]);
    }

    public function show($id)
    {
        $claim = TransactionClaim::with(['documents', 'branches', 'fees', 'profits'])->findOrFail($id);

        $branch = MasterBranch::where('code', $claim->branch)->first();

        $user = User::where('id', $claim->created_by)->first();

        return view('batch-claim.show', [
            'pageConfigs' => $this->pageConfigs,
            'page_title' => $this->page_title,
            'route' => $this->route,
            'claim' => $claim,
            'branch' => $branch,
            'user' => $user
        ]);
    }


    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required'
        ]);
        $claim = TransactionClaim::find($request->id);
        if ($claim) {
            $claim->status = $request->status;
            $claim->note = $request->note;
            $claim->save();
            return response()->json(['message' => 'Claim updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Claim not found'], 404);
        }
    }

    // app/Http/Controllers/BatchClaimController.php
    public function createBatch(Request $request)
    {
        // Debug the request data
        $claim = TransactionClaim::with('fees')->whereIn('id', $request->claimId)->get();

        for ($i = 0; $i < count($claim); $i++) {
            $claim[$i]->status = '2.3';
            $claim[$i]->save();
        }

        //save to batch
        $batch = new TrxBatch();
        $batch->name = 'Batch By: ' . auth()->user()->usename . '-' . auth()->user()->name . ' ' . now();
        $batch->note = '';
        $batch->status = '2.3';
        $batch->save();
        //save to detail
        $save = [];
        foreach ($claim as $key => $val) {
            foreach ($val->fees as $key => $fee) {
                $save[] = [
                    'id' => (string) \Illuminate\Support\Str::uuid(),
                    'trx_batch_id'    => $batch->id,
                    'transaction_claim_id' => $val->id,
                    'transaction_claim_fee_id' => $fee->id,
                    'branch_code' => $val->branch,
                    'branch_name' => $val->branches->name,
                    'currency_fee' => $fee->currency_fee,
                    'amount_fee' => $fee->amount_fee,
                    'created_at' => now(),
                    'created_by' => auth()->user()->id,
                ];
            }
        }

        // //buat fungsi save dari array ke detail
        foreach (array_chunk($save, 1000) as $chunk) {
            TrxBatchDetail::insert($chunk);
        }

        return response()->json(['message' => 'Batch created successfully'], 200);
    }



    public function datatableFeeTransfer(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $record  = TransactionClaim::with(['branches', 'fees', 'users'])->orderBy('created_at', 'DESC')->where('type', 'Fee Transfer')->whereIn('status', ['1.1', '2.1']);
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('id', function ($data) {
                    if ($data->status == '2.1') {
                        return '<input type="checkbox" class="row-checkbox" value="' . $data->id . '">';
                    }
                    return '';
                })
                ->addColumn('type', function ($data) {
                    $render = $data->type;
                    $render = explode('.', $render);
                    return $render[0];
                })
                ->addColumn('fee', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->type_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('currency', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->currency_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('account', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->account . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('amount', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->amount_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'batch-claim', 'batch-claim');
                    return $button;
                })
                ->rawColumns(['action', 'fee', 'currency', 'account', 'amount', 'status', 'id'])
                ->make(true);
        }
    }

    public function datatableFeeEvent(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $record  = TransactionClaim::with(['branches', 'fees', 'users'])->orderBy('created_at', 'DESC')->where('type', 'Fee Event')->whereIn('status', ['1.1', '2.1']);
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('id', function ($data) {
                    if ($data->status == '2.1') {
                        return '<input type="checkbox" class="row-checkbox" value="' . $data->id . '">';
                    }
                    return '';
                })
                ->addColumn('type', function ($data) {
                    $render = $data->type;
                    $render = explode('.', $render);
                    return $render[0];
                })
                ->addColumn('fee', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->type_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('currency', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->currency_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('account', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->account . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('amount', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->amount_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'batch-claim', 'batch-claim');
                    return $button;
                })
                ->rawColumns(['action', 'fee', 'currency', 'account', 'amount', 'status', 'id'])
                ->make(true);
        }
    }
}
