<?php

namespace App\Http\Controllers;

use App\Models\MasterGl;
use App\Models\TransactionClaim;
use App\Models\TrxBatch;
use App\Models\TrxBatchDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BatchClaimApproval1Controller extends Controller
{
    private $page_title         = "Batch Claim";
    private $route              = "batch-claim-approval1";
    private $permission         = "batch-claim-approval1";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        return view('batch-claim-approval1.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'permission'    => $this->permission
        ]);
    }

    public function show($id)
    {
        // Ambil data batch_claim beserta detailnya
        $batch_claim = TrxBatch::with(['batch_detail.claim', 'batch_detail.documents'])->findOrFail($id);

        // Ambil data master_gl dan buat associative array untuk mapping
        $master_gl = MasterGl::all()->keyBy('gl_currency');

        // Ambil user yang membuat batch_claim
        $user = User::where('id', $batch_claim->created_by)->first();

        return view('batch-claim-approval1.show', [
            'pageConfigs' => $this->pageConfigs,
            'page_title' => $this->page_title,
            'route' => $this->route,
            'batch_claim' => $batch_claim,
            'user' => $user,
            'master_gl' => $master_gl
        ]);
    }

    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required'
        ]);

        $batch_claim = TrxBatch::find($request->id);

        if ($batch_claim) {
            // Update the status of the TrxBatch
            $batch_claim->status = $request->status;
            $batch_claim->note = $request->note;
            $batch_claim->save();

            // Loop through each detail and update the associated TransactionClaim
            foreach ($batch_claim->batch_detail as $detail) {
                $transactionClaim = $detail->claim;

                if ($transactionClaim) {
                    $transactionClaim->status = $request->status;
                    $transactionClaim->note = $request->note;
                    $transactionClaim->save();
                }
            }

            return response()->json(['message' => 'Claim and associated transaction claims updated successfully'], 200);
        } else {
            return response()->json(['message' => 'Claim not found'], 404);
        }
    }



    public function datatable(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $record  = TrxBatch::with(['users'])->orderBy('created_at', 'DESC')->whereIn('status', ['2.3']);
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'batch-claim-approval1', 'batch-claim-approval1');
                    return $button;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }
}
