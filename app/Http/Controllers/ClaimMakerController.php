<?php

namespace App\Http\Controllers;

use App\Models\MasterBranch;
use App\Models\MasterGl;
use App\Models\TransactionClaim;
use App\Models\TransactionClaimDocument;
use App\Models\TransactionClaimFee;
use App\Models\TransactionClaimProfit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class ClaimMakerController extends Controller
{
    private $page_title         = "Claim";
    private $route              = "claim";
    private $permission         = "claim";
    private $pageConfigs        = ['pageHeader' => false];

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:' . $this->permission . '.index|' . $this->permission . '.create|' . $this->permission . '.edit|' . $this->permission . '.delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:' . $this->permission . '.create', ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $this->permission . '.edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $this->permission . '.delete', ['only' => ['destroy']]);
        $this->middleware('permission:' . $this->permission . '.show', ['only' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        return view('claim.index', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'route'         => $this->route,
            'permission'    => $this->permission
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('claim.create', [
            'pageConfigs'   => $this->pageConfigs,
            'page_title'    => $this->page_title,
            'permission'    => $this->permission,
            'route'         => $this->route,
        ]);
    }


    public function store(Request $request)
    {
        // Validasi request jika diperlukan
        $this->validate($request, []);

        // Debugging data yang diterima
        // dd($request->all()); // Cek data yang diterima

        // Membuat objek klaim baru
        $claim = new TransactionClaim();
        $claim->id = (string) \Illuminate\Support\Str::uuid(); // Assign UUID manually if necessary
        $claim->type = ($request->type_claim == 'FIT' || $request->type_claim == 'FOT') ? 'Fee Transfer' : 'Fee Event';
        $claim->branch = $request->branch;
        $claim->cif = $request->cif;
        $claim->cif_name = $request->cif_name;
        $claim->type_claim = ($request->type_claim == 'FIT') ? 'Fee Incoming Transfer' : (($request->type_claim == 'FOT') ? 'Fee Outgoing Transfer' : 'Fee Event');
        $claim->code = $request->code;
        $claim->judul_event = $request->judul_event;
        $claim->detail_event = $request->detail_event;
        $claim->status = "0";
        $claim->sum_amount_profit = $request->sum_amount_profit;
        $claim->sum_amount_fee = $request->sum_amount_fee;
        $claim->sum_percent = $request->sum_percent;
        $claim->event_emp_name = $request->event_emp_name;
        $claim->event_emp_nip = $request->event_emp_nip;
        $claim->event_emp_unit = $request->event_emp_unit;
        $claim->save();

        // Save feeData or feeEventData to TransactionClaimFee based on the claim type
        if ($request->type_claim == 'FIT' || $request->type_claim == 'FOT') {
            foreach ($request->feeData as $fee) {
                $feeModel = new TransactionClaimFee();
                $feeModel->type_fee = $fee['type_fee'];
                $feeModel->currency_fee = $fee['currency_fee'];
                $feeModel->amount_fee = $fee['amount_fee'];
                $feeModel->account = $fee['account'];
                $feeModel->status = $fee['status'];
                $feeModel->transaction_claim_id = $claim->id; // Set foreign key
                $feeModel->save();
            }
        } elseif ($request->type_claim == 'FE') {
            foreach ($request->feeEventData as $fee) {
                $feeModel = new TransactionClaimFee();
                $feeModel->type_fee = $fee['type_fee'];
                $feeModel->currency_fee = $fee['currency_fee'];
                $feeModel->amount_fee = $fee['amount_fee'];
                $feeModel->account = $fee['account'];
                $feeModel->keterangan = $fee['keterangan'];
                $feeModel->transaction_claim_id = $claim->id; // Set foreign key
                $feeModel->save();
            }
        }

        if ($request->type_claim == 'FIT' || $request->type_claim == 'FOT') {
            // Save profitData to TransactionClaimProfit
            foreach ($request->profitData as $profit) {
                $profitModel = new TransactionClaimProfit();
                $profitModel->deal_number = $profit['deal_number'];
                $profitModel->amount_profit = $profit['amount_profit'];
                $profitModel->transaction_claim_id = $claim->id; // Set foreign key
                $profitModel->save();
            }
        }

        // Menyimpan file dari fileData
        if ($request->has('fileData')) {
            foreach ($request->fileData as $file) {
                if (isset($file['file']) && $file['file'] instanceof \Illuminate\Http\UploadedFile) {
                    $fileModel = new TransactionClaimDocument();
                    $files = $file['file'];
                    $ext = $files->getClientOriginalExtension();
                    $filename = date('YmdHi') . '.' . $files->getClientOriginalName();
                    $filename = Str::slug($filename, '-') . '.' . $ext;

                    // Gunakan disk 'public' untuk menyimpan lampiran
                    $files->storeAs('public/file', $filename);
                    $fileModel->type = $file['type'];
                    $fileModel->filename = $filename;
                    $fileModel->transaction_claim_id = $claim->id; // Set foreign key
                    $fileModel->save();
                }
            }
        }

        // Menyimpan file dari fileDataEvent
        if ($request->has('fileDataEvent')) {
            foreach ($request->fileDataEvent as $file) {
                if (isset($file['file']) && $file['file'] instanceof \Illuminate\Http\UploadedFile) {
                    $fileModel = new TransactionClaimDocument();
                    $files = $file['file'];
                    $ext = $files->getClientOriginalExtension();
                    $filename = date('YmdHi') . '.' . $files->getClientOriginalName();
                    $filename = Str::slug($filename, '-') . '.' . $ext;

                    // Gunakan disk 'public' untuk menyimpan lampiran
                    $files->storeAs('public/file', $filename);
                    $fileModel->type = $file['type'];
                    $fileModel->filename = $filename;
                    $fileModel->transaction_claim_id = $claim->id; // Set foreign key
                    $fileModel->save();
                }
            }
        }

        return redirect()->route('claim.index')
            ->with('message', 'Claim created successfully');
    }



    public function show($id)
    {
        $claim = TransactionClaim::with(['documents', 'branches', 'fees', 'profits'])->findOrFail($id);

        $branch = MasterBranch::where('code', $claim->branch)->first();

        $user = User::where('id', $claim->created_by)->first();

        return view('claim.show', [
            'pageConfigs' => $this->pageConfigs,
            'page_title' => $this->page_title,
            'route' => $this->route,
            'claim' => $claim,
            'branch' => $branch,
            'user' => $user
        ]);
    }

    public function datatableFeeTransfer(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $branch = auth()->user()->branch;

            $record  = TransactionClaim::with(['branches', 'fees', 'users'])->orderBy('created_at', 'DESC')->where('type', 'Fee Transfer');
            if ($branch !== null) {
                $record->where('branch', $branch);
            }
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('type', function ($data) {
                    $render = $data->type;
                    $render = explode('.', $render);
                    return $render[0];
                })
                ->addColumn('fee', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->type_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('currency', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->currency_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('account', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->account . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('amount', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->amount_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'claim', 'claim');
                    return $button;
                })
                ->rawColumns(['action', 'fee', 'currency', 'account', 'amount', 'status'])
                ->make(true);
        }
    }

    public function datatableFeeEvent(Request $req)
    {
        if ($req->ajax()) {
            $this->type = $req['type'];
            $branch = auth()->user()->branch;
            $record  = TransactionClaim::with(['branches', 'fees', 'users'])->orderBy('created_at', 'DESC')->where('type', 'Fee Event');

            if ($branch !== null) {
                $record->where('branch', $branch);
            }
            return DataTables::of($record)
                ->addIndexColumn()
                ->addColumn('type', function ($data) {
                    $render = $data->type;
                    $render = explode('.', $render);
                    return $render[0];
                })
                ->addColumn('fee', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->type_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('currency', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->currency_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('account', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->account . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('amount', function ($data) {
                    $return = '';
                    foreach ($data->fees as $key => $val) {
                        $return .= $val->amount_fee . '<hr>';
                    }
                    return $return;
                })
                ->addColumn('status', function ($data) {
                    $label = statusClaim($data);
                    return $label;
                })
                ->addColumn('action', function ($data) {
                    $button = ButtonSED($data, 'claim', 'claim');
                    return $button;
                })
                ->rawColumns(['action', 'fee', 'currency', 'account', 'amount', 'status'])
                ->make(true);
        }
    }




    public function searchNasabah(Request $request)
    {
        $nocif = $request->input('nocif');

        // Mencari nasabah berdasarkan nomor CIF
        $nasabah = User::where('username', $nocif)->first();

        if ($nasabah) {
            // Jika nasabah ditemukan, kembalikan respons JSON dengan nama nasabah
            return response()->json(['nama_nasabah' => $nasabah->name]);
        } else {
            // Jika nasabah tidak ditemukan, kembalikan null
            return response()->json(['nama_nasabah' => null]);
        }
    }

    public function getBranches()
    {
        $branches = MasterBranch::all(['code', 'name']); // Adjust the fields as per your database structure
        return response()->json($branches);
    }
}
