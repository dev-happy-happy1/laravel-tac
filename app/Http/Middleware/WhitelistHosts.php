<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WhitelistHosts
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $allowedHosts = [
            '110.239.71.134', // Ganti dengan host yang diizinkan
            '110.239.70.145', // Ganti dengan host yang diizinkanPlo
            '10.14.21.139', // Tambahkan host lain jika diperlukan
	    '127.0.0.1', //
	    'ctconnect.ctcorpora.com',
            'ctconnect.ctcorp.com',
            'https://ctconnect.ctcorpora.com/',
            'https://ctconnect.ctcorpora.com',
        ];

        $requestHost = $request->getHost();
	//dd($requestHost);
         if (!in_array($requestHost, $allowedHosts)) {
              abort(403, 'Forbidden'); // Atau Anda bisa meredirect ke halaman error yang sesuai
         }

        return $next($request);
    }
}
