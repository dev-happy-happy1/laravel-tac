<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterGl extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $fillable = [
        'gl_no',
        'gl_ref',
        'gl_description',
        'gl_currency',
        'created_by',
        'updated_by'
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
}
