<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionClaim extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $fillable = [
        'type',
        'branch',
        'cif',
        'cif_name',
        'type_claim',
        'code',
    ];



    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function fees()
    {
        return $this->hasMany(TransactionClaimFee::class, 'transaction_claim_id');
    }

    public function profits()
    {
        return $this->hasMany(TransactionClaimProfit::class, 'transaction_claim_id');
    }

    public function documents()
    {
        return $this->hasMany(TransactionClaimDocument::class, 'transaction_claim_id');
    }

    public function branches()
    {
        return $this->hasOne(MasterBranch::class, 'code', 'branch');
    }

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
