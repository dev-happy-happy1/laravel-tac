<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $connection = 'pgsql_all';
    protected $table = 'news';
    protected $primaryKey = 'id';
    public $incrementing = false;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $fillable = [
        'url',
        'title',
        'slug',
        'content',
        'image',
        'published_at',
        'created_by',
        'updated_by',
        'author',
        'attach',
        'ref',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    // public function Author()
    // {
    //     return $this->hasOne(User::class, 'id', 'author');
    // }

    public function company()
    {
        return $this->belongsTo(Treenodes::class, 'ref', 'code');
    }
}
