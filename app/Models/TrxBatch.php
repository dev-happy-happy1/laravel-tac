<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxBatch extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function batch_detail()
    {
        return $this->hasMany(TrxBatchDetail::class, 'trx_batch_id');
    }

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
