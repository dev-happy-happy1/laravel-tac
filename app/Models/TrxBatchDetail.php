<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrxBatchDetail extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function fees()
    {
        return $this->hasMany(TransactionClaimFee::class, 'transaction_claim_id');
    }

    public function claim()
    {
        return $this->hasOne(TransactionClaim::class, 'id', 'transaction_claim_id');
    }

    public function documents()
    {
        return $this->hasMany(TransactionClaimDocument::class, 'transaction_claim_id', 'transaction_claim_id');
    }

    public function branches()
    {
        return $this->hasOne(MasterBranch::class, 'code', 'branch_code');
    }
}
