<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionClaimFee extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'type_fee',
        'currency_fee',
        'amount_fee',
        'account'
    ];

    public function claim()
    {
        return $this->belongsTo(TransactionClaim::class);
    }
}
