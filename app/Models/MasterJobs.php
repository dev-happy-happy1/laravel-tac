<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Model;

class MasterJobs extends Model
{
    use RecordSignatureUUID;

    protected $connection = 'pgsql_all';
    protected $table = 'jobs';

    protected $primaryKey = 'id';
    public $incrementing = false;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $fillable = [
        'company_code',
        'title',
        'location',
        'type',
        'email',
        'job_about',
        'job_description',
        'job_requirement',
        'other_description',
        'status_email',
        'post_date',
        'expiry_date',
        'created_by',
        'updated_by',
        'author'
    ];

    public function company()
    {
        return $this->belongsTo(Treenodes::class, 'company_code', 'code');
    }
}
