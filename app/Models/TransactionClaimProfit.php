<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionClaimProfit extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'deal_number',
        'amount_profit'
    ];

    public function claim()
    {
        return $this->belongsTo(TransactionClaim::class);
    }
}
