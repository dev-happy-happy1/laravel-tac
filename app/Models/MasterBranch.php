<?php

namespace App\Models;

use App\Traits\RecordSignatureUUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBranch extends Model
{
    use HasFactory, RecordSignatureUUID;

    protected $fillable = [
        'code',
        'name',
        'description',
    ];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function claim()
    {
        return $this->belongsTo(TransactionClaim::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
