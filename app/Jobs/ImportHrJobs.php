<?php

namespace App\Jobs;

set_time_limit(600);

use App\Imports\DatabaseUsersImport;
use App\Imports\EmployeesHrImport;
use App\Models\DatabaseUsers;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;

class ImportHrJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $filepath;
    public $company;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filepath, $company)
    {
        $this->filepath = $filepath;
        $this->company = $company;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Excel::import(new EmployeesHrImport($this->filepath, $this->company), $this->filepath);
    }
}
