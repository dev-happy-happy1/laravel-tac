<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTAC extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('branch')->after('status')->nullable(); //cabang si user
            $table->string('unit')->after('branch')->nullable(); //bagian
        });

        Schema::table('master_gls', function (Blueprint $table) {
            $table->string('type')->after('gl_currency')->nullable(); //event atau fee
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
