<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionClaimFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_claim_fees', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('transaction_claim_id');
            $table->string('type_fee')->nullable();
            $table->string('currency_fee')->nullable();
            $table->string('amount_fee')->nullable();
            $table->string('account')->nullable();
            $table->string('status')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_claim_fees');
    }
}
