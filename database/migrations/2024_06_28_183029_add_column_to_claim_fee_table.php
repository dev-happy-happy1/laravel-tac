<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToClaimFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_claim_fees', function (Blueprint $table) {
            $table->string('rc')->after('keterangan')->nullable();
            $table->string('rd')->after('keterangan')->nullable();
            $table->string('response_raw')->after('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_claim_fees', function (Blueprint $table) {
            //
        });
    }
}
