<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_claims', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('type')->nullable();
            $table->string('branch')->nullable();
            $table->string('cif')->nullable();
            $table->string('cif_name')->nullable();
            $table->string('type_claim')->nullable();
            $table->string('code')->nullable();
            $table->string('judul_event')->nullable();
            $table->string('detail_event')->nullable();
            $table->string('status');
            $table->string('sum_amount_profit')->nullable();
            $table->string('sum_amount_fee')->nullable();
            $table->string('sum_percent')->nullable();
            $table->string('event_emp_name')->nullable();
            $table->string('event_emp_nip')->nullable();
            $table->string('event_emp_unit')->nullable();
            $table->timestamps();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_claims');
    }
}
