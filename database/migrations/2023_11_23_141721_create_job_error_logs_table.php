<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobErrorLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_error_logs', function (Blueprint $table) {
            $table->id();
            $table->string('nip')->nullable();
            $table->string('name')->nullable();
            $table->string('structure_code')->nullable();
            $table->string('filename')->nullable();
            $table->string('company')->nullable();
            $table->text('message')->nullable();
            $table->integer('row')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_error_logs');
    }
}
