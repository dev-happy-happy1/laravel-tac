<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrxBatchDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_batch_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('trx_batch_id');
            $table->uuid('transaction_claim_id');
            $table->uuid('transaction_claim_fee_id');
            $table->string('branch_code');
            $table->string('branch_name');
            $table->string('currency_fee');
            $table->string('amount_fee');
            $table->timestamps();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_batch_details');
    }
}
