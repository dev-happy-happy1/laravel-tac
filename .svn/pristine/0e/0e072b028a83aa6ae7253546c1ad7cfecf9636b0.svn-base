<?php

namespace App\Console\Commands;

use App\Models\TransactionAtCost;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetDataFromDWH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:dataDwh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get daily data from DWH';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = DB::connection('sqlsrv_dwh')->select("
        SELECT
            RFVDAT AS value_date,
            RFREFF AS noreff,
            RFOCD1 AS customer_name,
            RFACCT AS customer_account,
            RFCHKA AS transfer_amount,
            RFCURR AS currency,
            RFDTCH AS type,
            RFDISG AS trx_date 
        FROM
            CURRENT_RMVLSM 
        WHERE
            RFDTCH LIKE '%3%' 
            AND RFCURR IN ( 'AUD', 'GBP', 'HKD' )
            AND RFDISG = '290121'");

        if ($users) {
            foreach ($users as $key => $val) {
                DB::beginTransaction();
                try {
                    $trx = TransactionAtCost::updateOrCreate(
                        [
                            'h_noreff'          => trim($val->noreff)
                        ],
                        [
                            'h_value_date'      => cekDateDwh(trim($val->value_date)),
                            'h_noreff'          => trim($val->noreff),
                            'h_customer_name'   => trim($val->customer_name),
                            'h_customer_account' => trim($val->customer_account),
                            'h_transfer_amount' => trim($val->transfer_amount),
                            'h_currency'        => trim($val->currency),
                            'h_type'            => trim($val->type),
                            'h_trx_date'        => cekDateDwh(trim($val->trx_date)),
                            'status_posting'    => 4,
                            'updated_at'        => date('Y-m-d H:i:s')
                        ]
                    );
                    DB::commit();
                    Log::build([
                        'driver'    => 'single',
                        'path'      => storage_path('logs/getDwh_succcess/Log, ' . date('Ymd') . '.log'),
                    ])->info('Success! : ' . json_encode($trx));
                } catch (\Exception  $th) {
                    //throw $th;
                    Log::build([
                        'driver'    => 'single',
                        'path'      => storage_path('logs/getDwh_error/Log Error, ' . date('Ymd') . '.log'),
                    ])->error($th->getMessage());
                    DB::rollBack();
                }
            }
        }
        // AND RFDISG = '" . date('j', strtotime(date('Y-m-d' . '-1 days'))) . "'");
        dd($users);
    }
}
