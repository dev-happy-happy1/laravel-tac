<?php

namespace App\Console\Commands;

use App\Http\Controllers\ApiController;
use App\Models\OriginalDwh;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PostToHost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $api            = new ApiController;
        $master         = OriginalDwh::with(['original_mt', 'master_gl', 'transaction' => function ($q) {
            $q->where('posting_status', 0); //status masih generated
        }])->get();
        // dd($master);
        // remap data 
        $data   = [];
        $no     = 0;
        foreach ($master as $key => $value) {
            $data[$no]['original_dwh_id']    = $value['id'];
            $data[$no]['noreff']             = $value['h_noreff'];
            // customer data 
            $data[$no]['customer_name']              = $value['h_customer_name'];
            $data[$no]['customer_rekening_debit']    = $value['h_customer_account'];
            $data[$no]['customer_rekening_ccy']      = $value['h_currency'];
            // gl data 
            $data[$no]['gl_name']            = $value['master_gl']['name'];
            $data[$no]['gl_ccy']             = $value['master_gl']['currency'];
            $data[$no]['gl_rekening_credit'] = $value['master_gl']['code'];
            $data[$no]['gl_reff_credit']     = $value['master_gl']['reff_number'];
            // jurnal posting data 
            $data[$no]['matching_status']    = false;
            $data[$no]['posting_type']       = '-';
            $data[$no]['posting_status']     = 0; //generated
            // mengecek data swift mt 
            if ($value['original_mt']) {
                $data[$no]['original_mt_id']     = $value['original_mt']['id'];
                $data[$no]['matching_status']    = true;
                $data[$no]['posting_type']       = 'system';
                //atcost data 
                $data[$no]['atcost_amount']      = $value['original_mt']['amount'];
                $data[$no]['atcost_ccy']         = $value['original_mt']['currency'];
                $data[$no]['submit_note']        = 'auto posting';
                // mengecek apakah currency rekening dan at cost sama 
                if ($value['h_currency'] == $value['original_mt']['currency']) {
                    $data[$no]['submit_kurs_sell']   = 1;
                    $data[$no]['submit_kurs_buy']    = 1;
                    $data[$no]['atcost_equivalen']   = $value['original_mt']['amount'];
                } else {
                    // proses kalkulasi 
                    $data[$no]['submit_kurs_sell']   = $api->getRate($value['original_mt']['currency'], 'kurs1')['rate'];
                    $data[$no]['submit_kurs_buy']    = $api->getRate($value['h_currency'], 'kurs2')['rate'];
                    // if ($value['h_currency'] == 'IDR') {
                    //     $data[$no]['submit_kurs_sell']   = $api->getRate($value['h_currency'], 'kurs1')['rate'];
                    //     $data[$no]['submit_kurs_buy']    = $api->getRate($value['original_mt']['currency'], 'kurs2')['rate'];
                    // }
                    // kalkulasi 
                    $data[$no]['atcost_equivalen']   = ($value['original_mt']['amount'] * $data[$no]['submit_kurs_sell']) / $data[$no]['submit_kurs_buy'];
                }
                // jika bukan HKD, maka di proses oleh system 
                if ($value['h_currency'] != 'HKD') {
                    $data[$no]['process_type']       =  'A';
                } else {
                    $data[$no]['posting_type']       = 'user';
                    $data[$no]['process_type']       =  'B';
                }
            }
            $no++;
            // save to database 
        }

        foreach ($data as $key => $value) {
            $cek        = Transaction::where('noreff', @$value['noreff'])->where('posting_status', 0)->first();
            $kondisi    = Transaction::where('noreff', @$value['noreff'])->first();
            // dd($kondisi);
            if ($cek || empty($kondisi)) {
                Transaction::updateOrCreate([
                    'noreff'            => @$value['noreff'],
                ], [
                    'original_dwh_id'   => @$value['original_dwh_id'],
                    'original_mt_id'    => @$value['original_mt_id'],
                    'noreff'            => @$value['noreff'],
                    //atcost data 
                    'atcost_amount'     => @$value['atcost_amount'],
                    'atcost_ccy'        => @$value['atcost_ccy'],
                    'atcost_equivalen'  => @$value['atcost_equivalen'],
                    // customer data 
                    'customer_name'             => @$value['customer_name'],
                    'customer_rekening_debit'   => @$value['customer_rekening_debit'],
                    'customer_rekening_ccy'     => @$value['customer_rekening_ccy'],
                    // gl data 
                    'gl_name'               => @$value['gl_name'],
                    'gl_ccy'                => @$value['gl_ccy'],
                    'gl_rekening_credit'    => @$value['gl_rekening_credit'],
                    'gl_reff_credit'        => @$value['gl_reff_credit'],
                    // jurnal posting data 
                    'matching_status'   => @$value['matching_status'],
                    'posting_status'    => @$value['posting_status'],
                    'posting_type'      => @$value['posting_type'],
                    'posting_response'  => @$value['posting_response'],
                    'process_type'      => @$value['process_type'],
                    'posting_status'    => @$value['posting_status'],
                    // submitter data 
                    'submit_note'       => @$value['submit_note'],
                    'submit_kurs_sell'  => @$value['submit_kurs_sell'],
                    'submit_kurs_buy'   => @$value['submit_kurs_buy'],
                    'updated_at'        => date('Y-m-d H:i:s')
                ]);
            }
        }

        // PROSES PEN JURNALAN 
        $transaction = Transaction::where('posting_status', 0)->whereNotNull('original_mt_id')->get();
        foreach ($transaction as $key => $record) {
            $posting = '';
            DB::beginTransaction();
            try {
                $data_api['TransactionBranch']  = '888';
                $data_api['AmountDebit']        = explode('.', (convertNumber($record['atcost_equivalen']) * 100))[0];
                $data_api['CurrencyDebit']      = $record['customer_rekening_ccy'];
                $data_api['RateDebit']          = explode('.', (convertNumber($record['submit_kurs_sell']) * 100))[0];
                $data_api['AmountCredit']       = explode('.', (convertNumber($record['atcost_amount']) * 100))[0];
                $data_api['CurrencyCredit']     = $record['gl_ccy'];
                $data_api['RateCredit']         = explode('.', (convertNumber($record['submit_kurs_buy']) * 100))[0];
                $data_api['AccDebit']           = $record['customer_rekening_debit'];
                $data_api['AccCredit']          = $record['gl_reff_credit'];

                // GET SESSION & IV SECRET 
                // $getSession = $api->geSession();
                // $sk_sc      = $getSession['sk_sc'];
                // $iv_session = $getSession['iv_session'];

                // $data_api['sk_sc']          = $sk_sc;
                // $data_api['iv_session']     = $iv_session;
                // PROSES POSTING 
                $posting    = $api->postingMISC($data_api);
                if ($posting->Data->ResponseCode == '00') {
                    $record->update([
                        'posting_status' => 1, //posting success
                        'posting_response' => json_encode($posting)
                    ]);
                } else {
                    $record->update([
                        'posting_status' => 2, //posting failed
                        'posting_response' => json_encode($posting)
                    ]);
                }
                Log::build([
                    'driver'    => 'single',
                    'path'      => storage_path('logs/POST/SUCCESS/Log, ' . date('Y-m-d') . '.log'),
                ])->info('Success! : ' . json_encode($record));
                DB::commit();
            } catch (\Exception $th) {
                //throw $th;
                Log::build([
                    'driver'    => 'single',
                    'path'      => storage_path('logs/POST/ERROR/Log, ' . date('Y-m-d') . '.log'),
                ])->error($th->getMessage());
                DB::rollBack();
            }
        }
        // dd($data);
        // PROSES PENJURNALAN 
        // $data['posting_response']   = $value;
    }
}
