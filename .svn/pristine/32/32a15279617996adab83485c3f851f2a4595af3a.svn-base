<?php

namespace App\Http\Controllers;

use App\Models\MasterRate;
use App\Models\OriginalRate;
use App\Repository\Task\EloquentTaskRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ApiController extends Controller
{

    public function getRate($curr, $type, $lawan = null)
    {
        $rate_type = '';
        if ($type == 'kurs1') {
            $rate_type = 'DEVISA_BANKJUAL';
        }
        if ($type == 'kurs2') {
            $rate_type = 'DEVISA_BANKBELI';
        }
        if ($type == 'tengah') {
            $rate_type = 'TENGAH';
        }

        $record = OriginalRate::where('currency', $curr)->where('rate_type', $rate_type)->orderBy('date', 'DESC')->first();
        // dump($record);
        $rate   = @$record->rate;
        if ($curr == 'IDR' && $type == 'kurs2') {
            $rate = 1;
        }

        $data   = [
            'id'    => @$record->id,
            'rate'  => @$rate,
        ];
        return $data;
    }

    public function getRekening(Request $req)
    {
        $api = $this->getRekeningAPI($req['rekening_number']);
        if (in_array($api['record']->AccountStatus, [1, 4, 5])) {
            if ($api['status'] != 'success') {
                $json = [
                    'status'    => $api['status'],
                    'msg'       => $api['msg']
                ];
                return response()->json($json);
            }
            $data['kurs1']              = 1;
            $data['kurs2']              = 1;
            $data['nominal_equivalen']  = $req['amount'];
            if (trim($api['record']->AccountCurrency) != trim($req['currency'])) {
                $kurs1 = $this->getRate($req['currency'], 'kurs1');
                $kurs2 = $this->getRate(trim($api['record']->AccountCurrency), 'kurs2');
                // if (trim($api['record']->AccountCurrency) == 'IDR') {
                //     $kurs1 = $this->getRate(trim($api['record']->AccountCurrency), 'kurs1');
                //     $kurs2 = $this->getRate($req['currency'], 'kurs2');
                // }

                $data['kurs1']      = $kurs1['rate'];
                $data['kurs2']      = $kurs2['rate'];

                $data['nominal_equivalen']  = ($req['amount'] * $data['kurs1']) / $data['kurs2'];
            }
            // parsing data ke json 
            $json = [
                'status'            => $api['status'],
                'msg'               => $api['msg'],
                'account_name'      => trim($api['record']->AccountName),
                'account_number'    => trim($api['record']->AccountNumber),
                'account_currency'  => trim($api['record']->AccountCurrency),
                'kurs1'             => $data['kurs1'],
                'kurs2'             => $data['kurs2'],
                'nominal_equivalen' => $data['nominal_equivalen'],
            ];
        } else {
            $json = [
                'status' =>  'error',
                'msg'    => 'Account Not Found or Non-Active!'
            ];
        }
        // dd($json);
        return response()->json($json);
    }

    public function getRekeningAPI($account)
    {
        $curl   = curl_init();
        $request = [
            "Data" => [
                "ProcessingCode" => "285026",
                "TransmissionDateTime" => "0912042842",
                "TraceID" => "trx_trace_umc",
                "Time" => "042842",
                "Date" => "0912",
                "DateSettlement" => "0912",
                "TerminalID" => "940912",
                "DE48" => [
                    "AccountNumber" => $account
                ]
            ]
        ];
        $request = json_encode($request);
        // dd($request);
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('API_INQ_CASA_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "channel_id": "' . ENV('API_INQ_CASA_CHANEL_ID') . '",
                "timestamp" : "1582901215",
                "service_id": "' . ENV('API_INQ_CASA_SERVICE_ID') . '",
                "key_id": "' . ENV('API_INQ_CASA_KEY_ID') . '",
                "data": {
                "msg": "' . encrypt_decrypt('encrypt', $request) . '"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        try {
            // dd($response);
            $response = encrypt_decrypt('decrypt', $response->data);
            $response = json_decode($response);
            // dd($response);
            $json = [
                'status' => $response->Data->Response == '00' ? 'success' : 'error',
                'msg'    => @$response->Data->Response == '00' ? 'success' : $response->Data->ResponseDesc,
                'record' => $response->Data->DE48
            ];

            //code...
        } catch (\Exception $th) {
            //throw $th;
            $json = [
                'status' =>  'error',
                'msg'    => $th->getMessage()
            ];
        }
        curl_close($curl);
        return $json;
    }

    // TRANSAKSI 
    public function geSession()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ENV('API_MISC_SESSION_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "channel_id": "' . ENV('API_MISC_SESSION_CHANEL_ID') . '",
                "key_id": "' . ENV('API_MISC_SESSION_KEY_ID') . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        $response = json_decode($response);
        dd($response);
        if ($response->rc == '00') {
            $json = [
                'status'        => 'success',
                'sk_sc'         => $response->sk_sc,
                'iv_session'    => $response->iv_session,
            ];
        } else {
            $json = [
                'status'        => 'error',
                'response'      => $response,
            ];
        }
        curl_close($curl);
        return $json;
        // echo $response;
    }

    public function postingMISC($data)
    {
        // data posting 
        $TransactionBranch  = $data['TransactionBranch'];
        $AmountDebit        = $data['AmountDebit'];
        $CurrencyDebit      = $data['CurrencyDebit'];
        $RateDebit          = $data['RateDebit'];
        $AmountCredit       = $data['AmountCredit'];
        $CurrencyCredit     = $data['CurrencyCredit'];
        $RateCredit         = $data['RateCredit'];
        $AccDebit           = $data['AccDebit'];
        $AccCredit          = $data['AccCredit'];

        // signature 
        // $sk_sc              = $data['sk_sc'];
        // $iv_session         = $data['iv_session'];
        // dd(date('Y-m-d H:i:s', '1582901215'));
        $productId          = '0000000000000000000';

        // data request 
        $request = '{"Data": {
                    "ProcessingCode": "490029",
                    "TransmissionDateTime": "' . date('mdHis') . '",
                    "TraceID": "trx_trace_umc",
                    "Time": "' . date('His') . '",
                    "Date": "' . date('md') . '",
                    "DatePlus": "' . date('md') . '",
                    "MerchType": "6017",
                    "ReferenceNumber": "940912",
                    "CardAcceptorTerminalID": "MSMILE",
                    "DE48": {
                        "TransactionBranch": "' . $TransactionBranch . '",
                        "AmountDebit": "' . $AmountDebit . '",
                        "CurrencyDebit": "' . $CurrencyDebit . '",
                        "RateDebit": "' . $RateDebit . '",
                        "AmountCredit": "' . $AmountCredit . '",
                        "CurrencyCredit": "' . $CurrencyCredit . '",
                        "RateCredit": "' . $RateCredit . '",
                        "AccountCharges1": "",
                        "CurrencyCharges1": "",
                        "AmountCharges1": "",
                        "Remark1": "",
                        "Remark2": "",
                        "Remark3": "",
                        "PassbookBalance": "",
                        "TellerID": "0000",
                        "JurnalSequence": "",
                        "CheckNumber": "",
                        "UserOverride": "",
                        "SupervisorOverride": "",
                        "TellerOverride": "",
                        "CancelTransaction": "",
                        "DealEntryTicket": "",
                        "Profit": "",
                        "Loss": "",
                        "AmountCharges2": "",
                        "AccountCharges2": "",
                        "CurrencyCharges2": "",
                        "RateCharges2": "",
                        "AmountCharges3": "",
                        "AccountCharges3": "",
                        "CurrencyCharges3": "",
                        "RateCharges3": "",
                        "ProdukID": "' . $productId . '"
                    },
                    "AccDebit": "' . $AccDebit . '",
                    "AccCredit": "' . $AccCredit . '"
                }}';
        // encrypt 
        $request = encrypt_decrypt('encrypt', $request);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => '10.14.20.154:30001/securitybp',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "channel_id": "000011",
            "timestamp": "' . strtotime(date('Y-m-d H:i:s')) . '",
            "service_id": "UMC_UAT079",
            "key_id": "AXCVSFDJKAGDKAGDKAFG",
            "data": {
                "msg": "' . $request . '"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response);
        // dd($response);
        $response = encrypt_decrypt('decrypt', $response->data);
        $response = json_decode($response);
        curl_close($curl);
        return $response;
    }

    public function test()
    {
        return 'ok';
    }
}
